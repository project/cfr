<?php

namespace Drupal\cfrapi\CfrGroupSchema;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * Base class for a group schema decorator.
 *
 * Each decorator layer can add additional options, and then use these to
 * process the value.
 */
abstract class CfrGroupSchema_DecoratorBase implements GeneratingGroupSchemaInterface {

  /**
   * @var \Drupal\cfrapi\CfrGroupSchema\CfrGroupSchemaInterface
   */
  private $decorated;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\CfrGroupSchema\CfrGroupSchemaInterface $decorated
   *   Decorated group schema.
   */
  public function __construct(CfrGroupSchemaInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurators() {
    return $this->decorated->getConfigurators();
  }

  /**
   * {@inheritdoc}
   */
  public function getLabels() {
    return $this->decorated->getLabels();
  }

  /**
   * {@inheritdoc}
   */
  public function valuesGetValue(array $values) {
    $value = $this->decorated->valuesGetValue($values);
    return static::decorateValue($value, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function valuesPhpGetPhp(array $values_php_parts, CfrCodegenHelperInterface $helper) {

    if (!$this->decorated instanceof GeneratingGroupSchemaInterface) {
      return $helper->notSupported($this->decorated, NULL, 'This group schema does not support code generation.');
    }

    $value_php = $this->decorated->valuesPhpGetPhp($values_php_parts, $helper);

    $rm = new \ReflectionMethod(static::class, 'decorateValue');

    if (self::class === $rm->getDeclaringClass()) {
      // No need to call ->decorateValue().
      return $value_php;
    }

    if (!$values_php_parts) {
      $values_php = '[]';
    }
    else {
      $values_php = '';
      foreach ($values_php_parts as $key => $php_statement) {
        $values_php .= "\n  " . var_export($key, TRUE) . ' => ' . $php_statement . ',';
      }
      $values_php = "[$values_php\n]";
    }

    if ($values_php !== '') {
      $values_php .= "\n";
    }

    return '\\' . static::class . '::decorateValue('
      . $value_php . ','
      . $values_php . ')';
  }

  /**
   * Decorates the value.
   *
   * This static method can be used both in ->confGetValue() and in
   * ->confGetPhp(). This way, subclasses only need to override a single method.
   *
   * The default implementation just returns the value unchanged.
   *
   * @param mixed $value
   *   Combined value from parent group schema.
   * @param mixed[] $child_values
   *   Values returned from group configurators.
   *
   * @return mixed
   *   New combined value.
   */
  public static function decorateValue($value, array $child_values) {
    return $value;
  }

}
