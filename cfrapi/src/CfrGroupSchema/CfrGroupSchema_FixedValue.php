<?php

namespace Drupal\cfrapi\CfrGroupSchema;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * Implementation with a fixed value, with no item configurators.
 *
 * This can be useful as the bottom of a decorator stack.
 */
class CfrGroupSchema_FixedValue implements GeneratingGroupSchemaInterface {

  /**
   * @var mixed
   */
  private $value;

  /**
   * @var string|null
   */
  private $php;

  /**
   * Constructor.
   *
   * @param mixed $value
   *   Fixed value.
   * @param string|null $php
   *   Value-generating PHP expression, or NULL to attempt to export $value.
   */
  public function __construct($value, $php = NULL) {
    $this->value = $value;
    $this->php = $php;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurators() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabels() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function valuesGetValue(array $values) {
    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function valuesPhpGetPhp(array $valuesPhp, CfrCodegenHelperInterface $helper) {
    if (NULL !== $this->php) {
      return $this->php;
    }
    return $helper->export($this->value);
  }

}
