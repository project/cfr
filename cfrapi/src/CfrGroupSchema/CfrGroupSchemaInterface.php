<?php

namespace Drupal\cfrapi\CfrGroupSchema;

/**
 * Schema object for a group configurator.
 *
 * The object expresses all the distinguishing characteristics of a group
 * configurator, while letting the configurator do the hard work such as form
 * building.
 *
 * When this interface was originally created, one method was forgotten.
 * Classes should not implement this directly, instead they should implement
 * GeneratingGroupSchemaInterface to support code generation.
 *
 * @see \Drupal\cfrapi\Configurator\Group\Configurator_CfrGroupSchema
 * @see \Drupal\cfrapi\CfrGroupSchema\GeneratingGroupSchemaInterface
 */
interface CfrGroupSchemaInterface {

  /**
   * Gets the configurator objects for each group options.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface[]
   *   Configurator for each group option, by string key.
   */
  public function getConfigurators();

  /**
   * Gets the labels for the group options.
   *
   * @return string[]
   *   Label for each group option, by string key.
   */
  public function getLabels();

  /**
   * Generates a combined value from the group values.
   *
   * The combined value could be just an array containing all the group values,
   * but it could also be the return value from a function call, where the group
   * values were passed as parameters.
   *
   * @param mixed[] $values
   *   Values returned from group configurators.
   *
   * @return mixed
   *   Combined value.
   */
  public function valuesGetValue(array $values);

}
