<?php

namespace Drupal\cfrapi\CfrGroupSchema;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * More complete version of CfrGroupSchemaInterface.
 *
 * The method defined here should have been added on the parent interface, but
 * doing this now would be a BC break. So instead this new interface was
 * introduced.
 */
interface GeneratingGroupSchemaInterface extends CfrGroupSchemaInterface {

  /**
   * Combines multiple PHP expressions into a single one.
   *
   * @param string[] $valuesPhp
   *   PHP expression for each value.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *   Helper for building PHP.
   *
   * @return string
   *   PHP expression for the final value.
   */
  public function valuesPhpGetPhp(array $valuesPhp, CfrCodegenHelperInterface $helper);

}
