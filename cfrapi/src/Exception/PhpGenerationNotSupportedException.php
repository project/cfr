<?php

namespace Drupal\cfrapi\Exception;

/**
 * Can be thrown in generated PHP code.
 */
class PhpGenerationNotSupportedException extends ConfToValueException {

}
