<?php

namespace Drupal\cfrapi\Exception;

/**
 * Exception: Producing a value from given configuration has failed.
 *
 * The most common explanation is that the "expected schema" has changed since
 * the configuration has been saved, and the configuration is now outdated.
 */
class ConfToValueException extends \Exception {

}
