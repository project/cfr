<?php

namespace Drupal\cfrapi\Exception;

/**
 * Failure to create a configurator, e.g. from a plugin definition.
 *
 * This usually indicates a programming error.
 */
class ConfiguratorFactoryException extends \Exception {

}
