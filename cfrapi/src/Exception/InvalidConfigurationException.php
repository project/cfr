<?php

namespace Drupal\cfrapi\Exception;

/**
 * Obsolete exception class.
 *
 * @deprecated
 *   Use ConfToValueException instead.
 */
class InvalidConfigurationException extends ConfToValueException {

}
