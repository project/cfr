<?php

namespace Drupal\cfrapi\ValueProvider;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;

/**
 * Implementation that uses a configurator with fixed configuration.
 *
 * @todo This seems unused. Consider to remove. See #3165296.
 */
class ValueProvider_FromCfrConf implements ValueProviderInterface {

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private $configurator;

  /**
   * @var mixed
   */
  private $conf;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator.
   * @param mixed $conf
   *   Fixed configuration to pass to the configurator.
   */
  public function __construct(ConfiguratorInterface $configurator, $conf) {
    $this->configurator = $configurator;
    $this->conf = $conf;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    return $this->configurator->confGetValue($this->conf);
  }

  /**
   * {@inheritdoc}
   */
  public function getPhp(CfrCodegenHelperInterface $helper) {
    return $this->configurator->confGetPhp($this->conf, $helper);
  }

}
