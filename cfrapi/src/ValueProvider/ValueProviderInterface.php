<?php

namespace Drupal\cfrapi\ValueProvider;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * Object which can produce a value and a PHP exression to produce such value.
 *
 * @todo This seems to be unused. Consider to remove.
 */
interface ValueProviderInterface {

  /**
   * Gets the value.
   *
   * The value can be anything.
   *
   * @return mixed
   *   The value.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   */
  public function getValue();

  /**
   * Gets PHP code to produce the value.
   *
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *   Helper for code generation.
   *
   * @return string
   *   PHP expression which, if executed, produces the value or throws an
   *   exception.
   */
  public function getPhp(CfrCodegenHelperInterface $helper);

}
