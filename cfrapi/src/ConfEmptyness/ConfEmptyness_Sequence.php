<?php

namespace Drupal\cfrapi\ConfEmptyness;

/**
 * Implementation for the old sequence configurator.
 *
 * A sequence configuration is empty if it has zero items, or if each item
 * is determined to be "empty" by an inner emptyness object.
 *
 * @deprecated
 *   All ConfEmptyness* classes and the interface will be removed in 7.x-3.x.
 *   The entire concept is obsolete.
 *   See https://www.drupal.org/project/cfr/issues/3165150.
 *   Even before that, this class was already obsolete, because the sequence
 *   configurator no longer works as described here.
 *   The new sequence configurator uses ConfEmptyness_Array.
 *   See https://www.drupal.org/project/cfr/issues/3168185.
 *
 * @see \Drupal\cfrapi\Configurator\Sequence\Configurator_Sequence
 * @see \Drupal\cfrapi\ConfEmptyness\ConfEmptyness_Array
 */
class ConfEmptyness_Sequence implements ConfEmptynessInterface {

  /**
   * @var \Drupal\cfrapi\ConfEmptyness\ConfEmptynessInterface
   */
  private $emptyness;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\ConfEmptyness\ConfEmptynessInterface $emptyness
   *   Object to detect empty item configurations.
   */
  public function __construct(ConfEmptynessInterface $emptyness) {
    $this->emptyness = $emptyness;
  }

  /**
   * {@inheritdoc}
   */
  public function confIsEmpty($conf) {
    if (NULL === $conf || [] === $conf) {
      return TRUE;
    }
    if (!\is_array($conf)) {
      // Invalid configuration.
      return FALSE;
    }
    foreach ($conf as $delta => $deltaConf) {
      if ($delta[0] === '#') {
        // Invalid delta.
        return FALSE;
      }
      if (!$this->emptyness->confIsEmpty($deltaConf)) {
        // Non-empty configuration for delta.
        return FALSE;
      }
    }
    // All items are empty.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyConf() {
    return [];
  }

}
