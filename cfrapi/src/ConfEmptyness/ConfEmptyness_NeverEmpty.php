<?php

namespace Drupal\cfrapi\ConfEmptyness;

/**
 * Implementation where no configuration counts as empty.
 *
 * @deprecated
 *   All ConfEmptyness* classes and the interface will be removed in 7.x-3.x.
 *   The entire concept is obsolete.
 *   See https://www.drupal.org/project/cfr/issues/3165150.
 */
class ConfEmptyness_NeverEmpty implements ConfEmptynessInterface {

  /**
   * {@inheritdoc}
   */
  public function confIsEmpty($conf) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyConf() {
    throw new \Exception('Never empty.');
  }

}
