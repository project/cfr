<?php

namespace Drupal\cfrapi\ConfEmptyness;

/**
 * Implementation using a list of configuration values that count as empty.
 *
 * @deprecated
 *   All ConfEmptyness* classes and the interface will be removed in 7.x-3.x.
 *   The entire concept is obsolete.
 *   See https://www.drupal.org/project/cfr/issues/3165150.
 */
class ConfEmptyness_Whitelist implements ConfEmptynessInterface {

  /**
   * @var mixed
   */
  private $defaultEmptyConf;

  /**
   * @var mixed[]
   */
  private $otherEmptyConfigurations;

  /**
   * Constructor.
   *
   * @param mixed $defaultEmptyConf
   *   Default empty configuration.
   * @param mixed[] $otherEmptyConfigurations
   *   Other configuration values that count as empty.
   */
  public function __construct($defaultEmptyConf, array $otherEmptyConfigurations = []) {
    $this->defaultEmptyConf = $defaultEmptyConf;
    $this->otherEmptyConfigurations = $otherEmptyConfigurations;
  }

  /**
   * {@inheritdoc}
   */
  public function confIsEmpty($conf) {
    if ($conf === $this->defaultEmptyConf) {
      return TRUE;
    }
    // in_array() does not work here because it uses == instead of ===.
    foreach ($this->otherEmptyConfigurations as $emptyConf) {
      if ($conf === $emptyConf) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyConf() {
    return $this->defaultEmptyConf;
  }

}
