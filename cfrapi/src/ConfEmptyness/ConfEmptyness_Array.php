<?php

namespace Drupal\cfrapi\ConfEmptyness;

/**
 * Implementation for "modern" sequence configurators.
 *
 * Configuration counts as empty if it has zero items, or if it is NULL.
 *
 * @deprecated
 *   All ConfEmptyness* classes and the interface will be removed in 7.x-3.x.
 *   The entire concept is obsolete.
 *   See https://www.drupal.org/project/cfr/issues/3165150.
 *
 * @see \Drupal\cfrapi\Configurator\Sequence\Configurator_SequenceTabledrag
 * @see \Drupal\cfrapi\ConfEmptyness\ConfEmptyness_Sequence
 */
class ConfEmptyness_Array implements ConfEmptynessInterface {

  /**
   * {@inheritdoc}
   */
  public function confIsEmpty($conf) {
    return NULL === $conf || [] === $conf;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyConf() {
    return [];
  }

}
