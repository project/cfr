<?php

namespace Drupal\cfrapi\ConfEmptyness;

/**
 * Implementation for configurators where one array key determines emptiness.
 *
 * It is used for drilldown configurators:
 * Drilldown configuration, if valid, has the format [$idKey => $id], or
 * [$idKey => $id, $optionsKey => $optionsConf], if options are present.
 * It counts as "empty" if the id is NULL or empty string.
 *
 * @deprecated
 *   All ConfEmptyness* classes and the interface will be removed in 7.x-3.x.
 *   The entire concept is obsolete.
 *   See https://www.drupal.org/project/cfr/issues/3165150.
 */
class ConfEmptyness_Key implements ConfEmptynessInterface {

  /**
   * @var string
   */
  private $key;

  /**
   * Constructor.
   *
   * @param string $key
   *   Array key to check if the configuration is an array.
   */
  public function __construct($key) {
    $this->key = $key;
  }

  /**
   * {@inheritdoc}
   */
  public function confIsEmpty($conf) {
    return !isset($conf[$this->key]) || '' === $conf[$this->key];
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyConf() {
    return NULL;
  }

}
