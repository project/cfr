<?php

namespace Drupal\cfrapi\ConfEmptyness;

/**
 * Implementation where every configuration counts as empty.
 *
 * @deprecated
 *   All ConfEmptyness* classes and the interface will be removed in 7.x-3.x.
 *   The entire concept is obsolete.
 *   See https://www.drupal.org/project/cfr/issues/3165150.
 */
class ConfEmptyness_AlwaysEmpty implements ConfEmptynessInterface {

  /**
   * @var mixed|null
   */
  private $emptyConf;

  /**
   * Constructor.
   *
   * @param mixed $emptyConf
   *   Example empty configuration.
   */
  public function __construct($emptyConf = NULL) {
    $this->emptyConf = $emptyConf;
  }

  /**
   * {@inheritdoc}
   */
  public function confIsEmpty($conf) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyConf() {
    return $this->emptyConf;
  }

}
