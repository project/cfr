<?php

namespace Drupal\cfrapi\ConfEmptyness;

/**
 * Implementation for select configurators.
 *
 * Configuration is empty if it is an empty string or NULL.
 *
 * @deprecated
 *   All ConfEmptyness* classes and the interface will be removed in 7.x-3.x.
 *   The entire concept is obsolete.
 *   See https://www.drupal.org/project/cfr/issues/3165150.
 *
 * @see \Drupal\cfrapi\Configurator\Id\Configurator_LegendSelect
 */
class ConfEmptyness_Enum implements ConfEmptynessInterface {

  /**
   * {@inheritdoc}
   */
  public function confIsEmpty($conf) {
    return NULL === $conf || '' === $conf;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyConf() {
    return NULL;
  }

}
