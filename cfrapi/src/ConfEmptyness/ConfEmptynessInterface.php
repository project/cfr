<?php

namespace Drupal\cfrapi\ConfEmptyness;

/**
 * Object to detect and create 'empty' configuration values.
 *
 * This is needed for the now obsolete concept of 'optional' configurators,
 * where some configuration values are considered to be 'empty'.
 *
 * Each optional configurator may have its own understanding what is considered
 * an 'empty' configuration.
 *
 * Currently the concept of 'optional' configurators and 'empty' configuration
 * is only used in an outdated version of the sequence configurator, so it is
 * not really useful.
 * See https://www.drupal.org/project/cfr/issues/3165152
 *
 * @deprecated
 *   All ConfEmptyness* classes and the interface will be removed in 7.x-3.x.
 *   The entire concept is obsolete.
 *   See https://www.drupal.org/project/cfr/issues/3165150.
 *
 * @see \Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface
 * @see \Drupal\cfrapi\Configurator\Sequence\Configurator_Sequence
 */
interface ConfEmptynessInterface {

  /**
   * Determines if given configuration is empty.
   *
   * @param mixed $conf
   *   Configuration to check.
   *
   * @return bool
   *   TRUE, if $conf is both valid and empty.
   */
  public function confIsEmpty($conf);

  /**
   * Gets a valid configuration where $this->confIsEmpty($conf) returns TRUE.
   *
   * @return mixed|null
   *   Configuration that is considered 'empty'.
   *
   * @throws \Exception
   *   If no empty configuration exists.
   */
  public function getEmptyConf();

}
