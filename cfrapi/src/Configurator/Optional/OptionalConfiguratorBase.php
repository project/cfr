<?php

/** @noinspection PhpDeprecationInspection */

namespace Drupal\cfrapi\Configurator\Optional;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\ConfEmptyness\ConfEmptynessInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Base class for some "optional" configurators.
 *
 * @deprecated
 *   Will be removed in 7.x-3.x.
 *   With the planned removal of OptionalConfiguratorInterface, the benefit of
 *   this base class is diminished.
 *   It is now recommended to implement this functionality in the class itself,
 *   instead of using this base class.
 *   See https://www.drupal.org/project/cfr/issues/3165150.
 */
abstract class OptionalConfiguratorBase implements OptionalConfiguratorInterface, ConfEmptynessInterface {

  /**
   * @var bool
   */
  private $required;

  /**
   * Constructor.
   *
   * @param bool $required
   *   TRUE, if an "empty" value should not be allowed.
   */
  public function __construct($required = TRUE) {
    $this->required = $required;
  }

  /**
   * Tells if the configurator is required.
   *
   * @return bool
   *   TRUE, if an "empty" value should not be allowed.
   */
  protected function isRequired() {
    return $this->required;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {

    if ($this->confIsEmpty($conf)) {
      if ($this->required) {
        return '- ' . t('Missing') . ' -';
      }

      return '- ' . t('None') . ' -';
    }

    return $this->nonEmptyConfGetSummary($conf, $summaryBuilder);
  }

  /**
   * Gets a summary for a non-empty configuration.
   *
   * (This method will not be called with empty configuration)
   *
   * @param mixed $conf
   *   Non-empty configuration.
   * @param \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface $summaryBuilder
   *   Helper object for summary-building.
   *
   * @return mixed
   *   The summary, usually a string of html.
   */
  abstract protected function nonEmptyConfGetSummary($conf, SummaryBuilderInterface $summaryBuilder);

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {

    if ($this->confIsEmpty($conf)) {
      if ($this->required) {
        throw new ConfToValueException("Required, but empty.");
      }

      return NULL;
    }

    return $this->nonEmptyConfGetValue($conf);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    if ($this->confIsEmpty($conf)) {
      if ($this->required) {
        return $helper->incompatibleConfiguration($conf, "Required, but empty.");
      }

      return 'NULL';
    }

    return $this->nonEmptyConfGetPhp($conf, $helper);
  }

  /**
   * Gets a value for non-empty configuration.
   *
   * (This method will not be called with empty configuration)
   *
   * @param mixed $conf
   *   Non-empty configuration.
   *
   * @return mixed
   *   Value for the given configuration.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   *   If no value can be produced for the given configuration.
   */
  abstract protected function nonEmptyConfGetValue($conf);

  /**
   * Generates PHP code for non-empty configuration.
   *
   * (This method will not be called with empty configuration)
   *
   * @param mixed $conf
   *   Non-empty configuration.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *   Helper object to export values and to manage failure.
   *
   * @return string
   *   PHP expression which, when executed, shall either return a value as
   *   expected for the given configuration, OR throw an exception.
   */
  protected function nonEmptyConfGetPhp($conf, CfrCodegenHelperInterface $helper) {
    return $helper->notSupported($this, $conf, "nonEmptyConfGetPhp() not supported.");
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated
   *   The method will be removed in 7.x-3.x.
   *   See https://www.drupal.org/project/cfr/issues/3165150.
   */
  public function getEmptyness() {
    return $this->required ? NULL : $this;
  }

  /**
   * {@inheritdoc}
   */
  public function confIsEmpty($conf) {
    return NULL === $conf || '' === $conf || [] === $conf;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyConf() {
    return NULL;
  }

}
