<?php

namespace Drupal\cfrapi\Configurator\Optional;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;

/**
 * Configurator where some configuration values are considered to be 'empty'.
 *
 * This concept was needed for an outdated version of the sequence configurator,
 * where "non-empty" items cause the sequence to grow, and "empty" items were
 * removed, but this is no longer the case with the new sequence configurator.
 * See https://www.drupal.org/project/cfr/issues/3168185.
 *
 * @deprecated
 *   Use the base interface instead. Will be removed in 7.x-3.x.
 *   See https://www.drupal.org/project/cfr/issues/3165150.
 */
interface OptionalConfiguratorInterface extends ConfiguratorInterface {

  /**
   * Gets an object to detect and create 'empty' configuration values.
   *
   * @return \Drupal\cfrapi\ConfEmptyness\ConfEmptynessInterface|null
   *   An emptyness object, or
   *   NULL, if the configurator is in fact required and thus no valid conf
   *   counts as empty.
   *
   * @deprecated
   *   The interface will be removed in 7.x-3.x.
   *   Classes that currently implement this interface will no longer do so in
   *   the future, and no longer have this method.
   *   Known implementations of this method have their own deprecation tag, to
   *   make the intended removal absolutely clear.
   *   See https://www.drupal.org/project/cfr/issues/3165150.
   */
  public function getEmptyness();

}
