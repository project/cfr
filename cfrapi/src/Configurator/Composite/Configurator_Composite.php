<?php

namespace Drupal\cfrapi\Configurator\Composite;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\ConfToForm\ConfToFormInterface;
use Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface;
use Drupal\cfrapi\ConfToValue\ConfToValueInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Configurator which uses separate objects for form, value and summary.
 */
class Configurator_Composite implements ConfiguratorInterface {

  /**
   * @var \Drupal\cfrapi\ConfToForm\ConfToFormInterface
   */
  private $confToForm;

  /**
   * @var \Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface
   */
  private $confToSummary;

  /**
   * @var \Drupal\cfrapi\ConfToValue\ConfToValueInterface
   */
  private $confToValue;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\ConfToForm\ConfToFormInterface $confToForm
   *   Partial object for ->confGetForm().
   * @param \Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface $confToSummary
   *   Partial object for ->confGetSummary().
   * @param \Drupal\cfrapi\ConfToValue\ConfToValueInterface $confToValue
   *   Partial object for ->confGetValue() and ->confGetPhp().
   */
  public function __construct(ConfToFormInterface $confToForm, ConfToSummaryInterface $confToSummary, ConfToValueInterface $confToValue) {
    $this->confToForm = $confToForm;
    $this->confToSummary = $confToSummary;
    $this->confToValue = $confToValue;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    return $this->confToForm->confGetForm($conf, $label);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return $this->confToSummary->confGetSummary($conf, $summaryBuilder);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    return $this->confToValue->confGetValue($conf);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    return $this->confToValue->confGetPhp($conf, $helper);
  }

}
