<?php

namespace Drupal\cfrapi\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\Optionable\Configurator_TextfieldBase;

/**
 * Textfield configurator.
 */
class Configurator_Textfield extends Configurator_TextfieldBase {

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    return \is_string($conf) ? $conf : '';
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    return \is_string($conf) ? var_export($conf, TRUE) : '';
  }

}
