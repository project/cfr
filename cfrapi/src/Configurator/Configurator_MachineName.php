<?php

/** @noinspection PhpDeprecationInspection */

namespace Drupal\cfrapi\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\ConfEmptyness\ConfEmptyness_Enum;
use Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Configurator for a string suitable as machine name.
 */
class Configurator_MachineName implements OptionalConfiguratorInterface {

  /**
   * @var bool
   */
  private $required;

  /**
   * Constructor.
   *
   * @param bool $required
   *   TRUE, to make this required.
   */
  public function __construct($required = TRUE) {
    $this->required = $required;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {

    if (!\is_string($conf)) {
      $conf = NULL;
    }

    return [
      /* @see form_process_machine_name() */
      '#type' => 'machine_name',
      '#title' => $label,
      '#default_value' => $conf,
      '#required' => $this->required,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {

    if (NULL === $conf || '' === $conf || !\is_string($conf)) {
      if ($this->required) {
        return t('Missing value');
      }

      return "''";
    }

    if (\strlen($conf) > 30) {
      $conf = substr($conf, 0, 27) . '[..]';
    }

    return check_plain(var_export($conf, TRUE));
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated
   *   The method will be removed in 7.x-3.x.
   *   See https://www.drupal.org/project/cfr/issues/3165150.
   */
  public function getEmptyness() {
    return new ConfEmptyness_Enum();
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {

    if (!\is_string($conf)) {
      return '';
    }

    if (NULL !== $msg = $this->machineNameGetError($conf)) {
      throw new ConfToValueException($msg);
    }

    return $conf;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    if (!\is_string($conf)) {
      return "''";
    }

    if (NULL !== $msg = $this->machineNameGetError($conf)) {
      return $helper->incompatibleConfiguration($conf, $msg);
    }

    return var_export($conf, TRUE);
  }

  /**
   * Validates the machine name, and returns a string error if not valid.
   *
   * This is only used when producing the value, not in form validation.
   *
   * @param string $value
   *   Machine name candidate to validate.
   *
   * @return null|string
   *   Failure message to use in an exception, or NULL if all is good.
   *   The message is not translated, because it is only used in the exception.
   *
   * @see form_process_machine_name()
   */
  private function machineNameGetError($value) {

    // Verify that the machine name not only consists of replacement tokens.
    if (preg_match('@^_+$@', $value)) {
      return 'The machine-readable name must contain unique characters.';
    }

    // Verify that the machine name contains no disallowed characters.
    if (preg_match('@[^a-z0-9_]+@', $value)) {
      // Since a hyphen is the most common alternative replacement character,
      // a corresponding validation error message is supported here.
      return 'The machine-readable name must contain only lowercase letters, numbers, and underscores.';
    }

    return NULL;
  }

}
