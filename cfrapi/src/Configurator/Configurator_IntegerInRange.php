<?php

namespace Drupal\cfrapi\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;
use Drupal\themekit\T;

/**
 * Configurator where the value is an integer.
 */
class Configurator_IntegerInRange implements ConfiguratorInterface {

  /**
   * @var int|null
   */
  private $min;

  /**
   * @var int|null
   */
  private $max;

  /**
   * @var bool
   */
  private $required = TRUE;

  /**
   * Constructor.
   *
   * @param int|null $min
   *   Minimum value, or NULL for no limit.
   * @param int|null $max
   *   Maximum value, or NULL for no limit.
   */
  public function __construct($min = NULL, $max = NULL) {
    $this->min = $min;
    $this->max = $max;
  }

  /**
   * Immutable setter, making the object optional.
   *
   * @return static
   *   Modified clone.
   */
  public function optional() {
    $clone = clone $this;
    $clone->required = FALSE;
    return $clone;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {

    $element = [
      '#title' => $label,
      '#type' => 'textfield',
      '#element_validate' => [
        T::c('element_validate_integer'),
      ],
      '#description' => $this->buildDescription(),
    ];

    if ($this->required) {
      $element['#required'] = TRUE;
    }

    if (NULL !== $this->min) {
      $element['#min'] = $this->min;
      $element['#element_validate'][] = T::c([self::class, 'elementValidateMin']);
    }

    if (NULL !== $this->max) {
      $element['#max'] = $this->max;
      $element['#element_validate'][] = T::c([self::class, 'elementValidateMax']);
    }

    if (\is_int($conf) || \is_string($conf)) {
      $element['#default_value'] = $conf;
    }

    return $element;
  }

  /**
   * Builds a description for the textfield element.
   *
   * @return string|null
   *   Description text, or NULL for no description.
   */
  private function buildDescription() {

    if (NULL === $this->max) {
      if (NULL === $this->min) {
        return NULL;
      }

      if (0 === $this->min) {
        return t('Non-negative integer.');
      }

      if (1 === $this->min) {
        return t('Positive integer.');
      }

      return t('Integer greater or equal to @min', ['@min' => $this->min]);
    }

    if (NULL === $this->min) {
      return t('Integer up to @max', ['@max' => $this->max]);
    }

    return t('Integer in range [@min...@max]', ['@min' => $this->min, '@max' => $this->max]);
  }

  /**
   * Callback for '#element_validate'. Validates the minimum value.
   *
   * @param array $element
   *   Textfield element.
   */
  public static function elementValidateMin(array $element /* , array &$form_state */) {

    $v = $element['#value'];

    $min = $element['#min'];
    if ($v < $min) {
      if (0 === $min) {
        form_error($element, t('%name must be non-negative.', ['%name' => $element['#title']]));
      }
      elseif (1 === $min) {
        form_error($element, t('%name must be positive.', ['%name' => $element['#title']]));
      }
      else {
        form_error($element, t('%name must be greater than or equal to @min.', ['%name' => $element['#title'], '@min' => $min]));
      }
    }
  }

  /**
   * Callback for '#element_validate'. Validates the maximum value.
   *
   * @param array $element
   *   The textfield element.
   */
  public static function elementValidateMax(array $element /* , array &$form_state */) {

    $v = $element['#value'];

    $max = $element['#max'];
    if ($v > $max) {
      form_error($element, t('%name must be no greater than @max.', ['%name' => $element['#title'], '@max' => $max]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return check_plain(var_export($conf, TRUE));
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {

    if (\is_string($conf)) {
      if ((string) (int) $conf !== $conf) {
        throw new ConfToValueException("Value must be an integer.");
      }
      $conf = (int) $conf;
    }
    elseif (!\is_int($conf)) {
      throw new ConfToValueException("Value must be an integer.");
    }

    if (NULL !== $this->min && $conf < $this->min) {
      throw new ConfToValueException("Value must be greater than or equal to $this->min.");
    }

    if (NULL !== $this->max && $conf > $this->max) {
      throw new ConfToValueException("Value must be no greater than $this->max.");
    }

    return $conf;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    if (\is_string($conf)) {
      if ((string) (int) $conf !== $conf) {
        return $helper->incompatibleConfiguration($conf, "Value must be an integer.");
      }
      $conf = (int) $conf;
    }
    elseif (!\is_int($conf)) {
      return $helper->incompatibleConfiguration($conf, "Value must be an integer.");
    }

    if (NULL !== $this->min && $conf < $this->min) {
      return $helper->incompatibleConfiguration($conf, "Value must be greater than or equal to $this->min.");
    }

    if (NULL !== $this->max && $conf > $this->max) {
      return $helper->incompatibleConfiguration($conf, "Value must be no greater than $this->max.");
    }

    return var_export($conf, TRUE);
  }

}
