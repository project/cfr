<?php

namespace Drupal\cfrapi\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * Decorator where the raw configuration is returned as a value.
 */
class Configurator_Passthru extends Configurator_DecoratorBase {

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    return $conf;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    return var_export($conf, TRUE);
  }

}
