<?php

namespace Drupal\cfrapi\Configurator\Group;

/**
 * Group configurator where config and value are split into 'options'.
 */
class Configurator_Group extends Configurator_GroupBase implements GroupConfiguratorInterface {

}
