<?php

namespace Drupal\cfrapi\Configurator\Group;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;

/**
 * Configurator where config and value are split into 'member' values.
 *
 * Not all 'group' configurators implement this interface.
 * A regular 'group' configurator does expect an associative array as
 * configuration, but the value can be anything. E.g. it could be the return
 * value of a method call where the values from the 'member' configurators were
 * passed as parameters.
 * By implementing this interface, the configurator promises to always return
 * the unaltered values from the member configurators. This allows other classes
 * to do any follow-up processing.
 *
 * @todo Rethink this, see #3165159.
 */
interface GroupConfiguratorInterface extends ConfiguratorInterface {

  /**
   * Produces a value from configuration.
   *
   * Unlike the base method, this method shall always return an associative
   * array with the same keys as expected for the configuration.
   *
   * @param mixed $conf
   *   Configuration data.
   *   A 'compatible' configuration is an associative array with one entry for
   *   each 'member' key.
   *
   * @return mixed[]
   *   Associative array with one value per 'member' key.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   *   In case the configuration is not accepted.
   */
  public function confGetValue($conf);

  /**
   * Gets PHP expressions for each group 'member'.
   *
   * @param mixed $conf
   *   Configuration data.
   *   The expected configuration is an associative array with one entry for
   *   each 'member' key.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *   Helper object for code generation.
   *
   * @return string[]
   *   PHP expressions to generate the values, one for each 'member' key, OR
   *   an exception-throwing statement, if the configuration is incompatible.
   *
   * @todo The correct term would be 'expression', not 'statement'.
   */
  public function confGetPhpStatements($conf, CfrCodegenHelperInterface $helper);

}
