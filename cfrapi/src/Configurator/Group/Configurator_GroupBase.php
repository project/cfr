<?php

namespace Drupal\cfrapi\Configurator\Group;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;

/**
 * Base class where group options are provided as properties.
 */
abstract class Configurator_GroupBase extends Configurator_GroupGrandBase {

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface[]
   */
  private $configurators = [];

  /**
   * @var string[]
   */
  private $labels = [];

  /**
   * Static factory.
   *
   * @param array $paramConfigurators
   *   Configurator for each group option.
   * @param string[] $labels
   *   Label for each group option.
   *
   * @return static
   *   Created instance.
   */
  public static function createFromConfigurators(array $paramConfigurators, array $labels) {
    $groupConfigurator = new static();
    foreach ($paramConfigurators as $k => $paramConfigurator) {
      $paramLabel = isset($labels[$k]) ? $labels[$k] : $k;
      $groupConfigurator->keySetConfigurator($k, $paramConfigurator, $paramLabel);
    }
    return $groupConfigurator;
  }

  /**
   * Fluent setter. Adds or replaces one of the group configurators.
   *
   * @param string $key
   *   String key for this option.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator for this option.
   * @param string $label
   *   Label for this option.
   *
   * @return $this
   */
  public function keySetConfigurator($key, ConfiguratorInterface $configurator, $label) {
    if ($key !== '' && is_string($key) && $key[0] === '#') {
      throw new \InvalidArgumentException("Key '$key' must not begin with '#'.");
    }
    $this->configurators[$key] = $configurator;
    $this->labels[$key] = $label;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigurators() {
    return $this->configurators;
  }

  /**
   * {@inheritdoc}
   */
  protected function getLabels() {
    return $this->labels;
  }

}
