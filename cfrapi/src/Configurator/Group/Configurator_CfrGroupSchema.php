<?php

namespace Drupal\cfrapi\Configurator\Group;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\CfrGroupSchema\CfrGroupSchemaInterface;
use Drupal\cfrapi\CfrGroupSchema\GeneratingGroupSchemaInterface;

/**
 * Group configurator using a group schema.
 */
class Configurator_CfrGroupSchema extends Configurator_GroupBase {

  /**
   * @var \Drupal\cfrapi\CfrGroupSchema\CfrGroupSchemaInterface
   */
  private $groupSchema;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\CfrGroupSchema\CfrGroupSchemaInterface $groupSchema
   *   The group schema to provide the option configurators.
   */
  public function __construct(CfrGroupSchemaInterface $groupSchema) {
    $this->groupSchema = $groupSchema;
    $labels = $groupSchema->getLabels();
    foreach ($groupSchema->getConfigurators() as $k => $configurator) {
      $label = isset($labels[$k]) ? $labels[$k] : $k;
      $this->keySetConfigurator($k, $configurator, $label);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    $value = parent::confGetValue($conf);
    if (!\is_array($value)) {
      return $value;
    }
    return $this->groupSchema->valuesGetValue($value);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    if (!$this->groupSchema instanceof GeneratingGroupSchemaInterface) {
      return $helper->notSupported($this->groupSchema, NULL, 'This group schema does not support code generation.');
    }

    $valuesPhp = $this->confGetPhpStatements($conf, $helper);

    return $this->groupSchema->valuesPhpGetPhp($valuesPhp, $helper);
  }

}
