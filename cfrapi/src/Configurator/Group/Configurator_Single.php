<?php

namespace Drupal\cfrapi\Configurator\Group;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Group configurator with a single option for the key '0'.
 *
 * @todo This is not used anywhere. Consider to remove. See #3165296.
 */
class Configurator_Single implements GroupConfiguratorInterface {

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private $configurator;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator for the value at [0].
   */
  public function __construct(ConfiguratorInterface $configurator) {
    $this->configurator = $configurator;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    $value = $this->configurator->confGetValue($conf);
    return [$value];
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return $this->configurator->confGetSummary($conf, $summaryBuilder);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    return $this->configurator->confGetForm($conf, $label);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhpStatements($conf, CfrCodegenHelperInterface $helper) {
    $php = $this->configurator->confGetPhp($conf, $helper);
    return [$php];
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    $php = $this->configurator->confGetPhp($conf, $helper);
    return "[$php]";
  }

}
