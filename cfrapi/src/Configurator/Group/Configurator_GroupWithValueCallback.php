<?php

namespace Drupal\cfrapi\Configurator\Group;

use Donquixote\CallbackReflection\Util\CallbackUtil;
use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * Group configurator with an additional value callback.
 */
class Configurator_GroupWithValueCallback extends Configurator_GroupBase {

  /**
   * @var callable
   */
  private $valueCallback;

  /**
   * Constructor.
   *
   * @param callable $valueCallback
   *   Callback to produce a combined value from group values.
   *   If this is an anonymous function, ->confGetPhp() won't work.
   */
  public function __construct($valueCallback) {
    if (!\is_callable($valueCallback)) {
      throw new \InvalidArgumentException("Argument must be callable.");
    }
    $this->valueCallback = $valueCallback;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    $value = parent::confGetValue($conf);
    if (!\is_array($value)) {
      return $value;
    }
    return \call_user_func($this->valueCallback, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    $php = parent::confGetPhp($conf, $helper);

    $callbackReflection = CallbackUtil::callableGetCallback($this->valueCallback);

    if (NULL === $callbackReflection) {
      return $helper->notSupported($this, $conf, "Callback is not valid");
    }

    return $callbackReflection->argsPhpGetPhp([$php], $helper);
  }

}
