<?php

namespace Drupal\cfrapi\Configurator\Group;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Base class for group configurators.
 *
 * The actual group options are coming from abstract methods to be implemented
 * in a sub-class.
 */
abstract class Configurator_GroupGrandBase implements ConfiguratorInterface {

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    if (!\is_array($conf)) {
      $conf = [];
    }
    $form = [];
    if (NULL !== $label && '' !== $label) {
      $form['#title'] = $label;
    }

    $labels = $this->getLabels();

    foreach ($this->getConfigurators() as $key => $configurator) {
      $keyConf = isset($conf[$key]) ? $conf[$key] : NULL;
      $form[$key] = $configurator->confGetForm($keyConf, $labels[$key]);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {

    if (!\is_array($conf)) {
      $conf = [];
    }

    $labels = $this->getLabels();

    $group = $summaryBuilder->startGroup();
    foreach ($this->getConfigurators() as $key => $configurator) {
      $keyConf = array_key_exists($key, $conf) ? $conf[$key] : NULL;
      $group->addSetting($labels[$key], $configurator, $keyConf);
    }

    return $group->buildSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {

    if (!\is_array($conf)) {
      // If all values are optional, this might still work.
      $conf = [];
    }

    $values = [];
    foreach ($this->getConfigurators() as $key => $configurator) {
      if (array_key_exists($key, $conf)) {
        $value = $configurator->confGetValue($conf[$key]);
      }
      else {
        $value = $configurator->confGetValue(NULL);
      }
      $values[$key] = $value;
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    if ([] === $this->getConfigurators()) {
      return '[]';
    }

    $php = '';
    foreach ($this->confGetPhpStatements($conf, $helper) as $key => $php_statement) {
      $php .= "\n  " . var_export($key, TRUE) . ' => ' . $php_statement . ',';
    }

    return "[$php\n]";
  }

  /**
   * Gets PHP expressions for the group values.
   *
   * Allows this to be subclassed implementing GroupConfiguratorInterface.
   *
   * @param mixed $conf
   *   Original configuration.
   *   If valid, this is an array, with one entry for each group option.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *   Helper for code generation.
   *
   * @return string[]
   *   PHP expressions to generate the values.
   *
   * @todo The correct term would be 'expression', not 'statement'.
   *
   * @see GroupConfiguratorInterface::confGetPhpStatements()
   */
  public function confGetPhpStatements($conf, CfrCodegenHelperInterface $helper) {

    if (!\is_array($conf)) {
      // If all values are optional, this might still work.
      $conf = [];
    }

    $php_statements = [];
    foreach ($this->getConfigurators() as $key => $configurator) {

      $key_conf = array_key_exists($key, $conf)
        ? $conf[$key]
        : NULL;

      $php_statements[$key] = $configurator->confGetPhp($key_conf, $helper);
    }

    return $php_statements;
  }

  /**
   * Gets the group configurators.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface[]
   *   Configurator for each member key.
   */
  abstract protected function getConfigurators();

  /**
   * Gets the labels.
   *
   * @return string[]
   *   Label or each member key.
   */
  abstract protected function getLabels();

}
