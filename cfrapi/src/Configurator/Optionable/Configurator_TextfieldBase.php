<?php

/** @noinspection PhpDeprecationInspection */

namespace Drupal\cfrapi\Configurator\Optionable;

use Drupal\cfrapi\ConfEmptyness\ConfEmptyness_Enum;
use Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;
use Drupal\themekit\T;

/**
 * Base class for textfield configurators.
 */
abstract class Configurator_TextfieldBase implements OptionalConfiguratorInterface {

  /**
   * @var bool
   */
  private $required;

  /**
   * Constructor.
   *
   * @param bool $required
   *   TRUE to make the textfield required.
   */
  public function __construct($required = TRUE) {
    $this->required = $required;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {

    if (!\is_string($conf)) {
      $conf = NULL;
    }

    return [
      '#type' => T::th('theme_textfield'),
      '#title' => $label,
      '#default_value' => $conf,
      '#required' => $this->required,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {

    if (NULL === $conf || '' === $conf || !\is_string($conf)) {
      if ($this->required) {
        return t('Missing value');
      }

      return "''";
    }

    if (\strlen($conf) > 30) {
      $conf = substr($conf, 0, 27) . '[..]';
    }

    return check_plain(var_export($conf, TRUE));
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated
   *   The method will be removed in 7.x-3.x.
   *   See https://www.drupal.org/project/cfr/issues/3165150.
   */
  public function getEmptyness() {
    return new ConfEmptyness_Enum();
  }

}
