<?php

namespace Drupal\cfrapi\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Checkbox configurator.
 */
class Configurator_Checkbox implements ConfiguratorInterface {

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    return !empty($conf);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return !empty($conf)
      ? t('Yes')
      : t('No');
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    $element = [
      '#title' => $label,
      '#type' => 'checkbox',
      '#default_value' => !empty($conf),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    return !empty($conf) ? 'true' : 'false';
  }

}
