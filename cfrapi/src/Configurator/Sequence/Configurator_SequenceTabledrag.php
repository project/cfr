<?php

/** @noinspection PhpDeprecationInspection */

namespace Drupal\cfrapi\Configurator\Sequence;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\ConfEmptyness\ConfEmptyness_Array;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;
use Drupal\cfrapi\Util\ArrayMode;
use Drupal\cfrapi\Util\FormUtil;
use Drupal\themekit\T;

/**
 * Configurator for arrays of values of the same type.
 *
 * The form element is a tabledrag with the option to remove items and add new
 * items. An inner configurator builds the form element for each row.
 *
 * A parameter controls whether array should be serial, associative, or mixed.
 * In case of an associative array, the user can define the array key when
 * adding new items. In case of mixed keys, the custom key is optional.
 */
class Configurator_SequenceTabledrag implements OptionalConfiguratorInterface {

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private $configurator;

  /**
   * @var bool
   */
  private $skipBrokenItems;

  /**
   * @var callable|null
   */
  private $itemLabelCallback;

  /**
   * @var int
   */
  private $arrayMode;

  /**
   * Static factory. Uses self::MODE_ASSOC.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator to use for each sequence item.
   * @param bool $skipBrokenItems
   *   TRUE, to skip items with incompatible configuration.
   *   FALSE, to have an exception thrown for broken items.
   * @param bool $mixed
   *   TRUE for a mix of serial and associative keys.
   *   FALSE to treat all keys associative.
   *
   * @return self
   *   Created instance.
   */
  public static function createAssoc(ConfiguratorInterface $configurator, $skipBrokenItems = FALSE, $mixed = FALSE) {
    return new self(
      $configurator,
      $skipBrokenItems,
      $mixed ? ArrayMode::MIXED : ArrayMode::ASSOC);
  }

  /**
   * Static factory. Uses self::MODE_MIXED.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator to use for each sequence item.
   * @param bool $skipBrokenItems
   *   TRUE, to skip items with incompatible configuration.
   *   FALSE, to have an exception thrown for broken items.
   *
   * @return self
   *   Created instance.
   */
  public static function createMixed(ConfiguratorInterface $configurator, $skipBrokenItems = FALSE) {
    return new self(
      $configurator,
      $skipBrokenItems,
      ArrayMode::MIXED);
  }

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator to use for each sequence item.
   * @param bool $skipBrokenItems
   *   TRUE, to skip items with incompatible configuration.
   *   FALSE, to have an exception thrown for broken items.
   * @param int|null $array_mode
   *   Determines how array keys are handled.
   *   One of the ArrayMode::* constants.
   */
  public function __construct(ConfiguratorInterface $configurator, $skipBrokenItems = FALSE, $array_mode = ArrayMode::SERIAL) {
    // Validate the array mode.
    switch ($array_mode) {
      case ArrayMode::ASSOC:
      case ArrayMode::SERIAL:
      case ArrayMode::MIXED:
        break;

      default:
        throw new \InvalidArgumentException('Invalid array mode.');
    }
    $this->configurator = $configurator;
    $this->skipBrokenItems = $skipBrokenItems;
    $this->arrayMode = $array_mode;
  }

  /**
   * Immutable setter. Sets array mode to assoc.
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withAssocKeys() {
    $clone = clone $this;
    $clone->arrayMode = ArrayMode::ASSOC;
    return $clone;
  }

  /**
   * Immutable setter. Sets array mode to serial.
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withSerialKeys() {
    $clone = clone $this;
    $clone->arrayMode = ArrayMode::SERIAL;
    return $clone;
  }

  /**
   * Immutable setter. Sets array mode to mixed.
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withMixedKeys() {
    $clone = clone $this;
    $clone->arrayMode = ArrayMode::MIXED;
    return $clone;
  }

  /**
   * Immutable setter. Sets array mode to the value provided.
   *
   * @param int|null $array_mode
   *   Determines how array keys are handled.
   *   One of the ArrayMode::* constants.
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withArrayMode($array_mode) {
    // Validate the array mode.
    switch ($array_mode) {
      case ArrayMode::ASSOC:
      case ArrayMode::SERIAL:
      case ArrayMode::MIXED:
        break;

      default:
        throw new \InvalidArgumentException('Invalid array mode.');
    }
    $clone = clone $this;
    $clone->arrayMode = $array_mode;
    return $clone;
  }

  /**
   * Immutable setter, sets item labels as t() strings.
   *
   * @param string $newItemLabel
   *   Translated label for the "Add another item" button.
   * @param string $nthItemLabel
   *   Translated label for the nth item, with a placeholder for the delta.
   *   The placeholder replacement is happening outside of the t() function,
   *   with a direct call to format_string().
   * @param string $placeholder
   *   Name of the placeholder for the item key.
   *
   * @return static
   *   Modified clone of the object.
   */
  public function withItemLabel($newItemLabel, $nthItemLabel, $placeholder = '@delta') {
    $clone = clone $this;
    $clone->itemLabelCallback = static::buildItemLabelCallback(
      // phpcs:ignore
      t($newItemLabel),
      // phpcs:ignore
      t($nthItemLabel),
      $placeholder);
    return $clone;
  }

  /**
   * Creates a callback to create an item label.
   *
   * @param string $newItemLabel
   *   Translated label for the "Add another item" button.
   * @param string $nthItemLabel
   *   Translated label for the nth item, with a placeholder for the delta.
   * @param string $placeholder
   *   Name of the placeholder for the item key.
   *
   * @return \Closure
   *   Item label callback.
   *   Signature: ($delta) -> $label.
   */
  private static function buildItemLabelCallback($newItemLabel, $nthItemLabel, $placeholder = '@delta') {
    return static function ($delta = NULL) use ($newItemLabel, $nthItemLabel, $placeholder) {
      if (NULL === $delta) {
        return $newItemLabel;
      }
      $delta = is_numeric($delta)
        ? '#' . $delta
        : var_export($delta, TRUE);
      if ('!' === $placeholder[0]) {
        $delta = check_plain($delta);
      }
      return format_string($nthItemLabel, [$placeholder => $delta]);
    };
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated
   *   The method will be removed in 7.x-3.x.
   *   See https://www.drupal.org/project/cfr/issues/3165150.
   */
  public function getEmptyness() {
    return new ConfEmptyness_Array();
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {

    if (NULL === $conf) {
      return [];
    }
    if (!\is_array($conf)) {
      throw new ConfToValueException('Configuration must be an array or NULL.');
    }

    $values = [];
    foreach ($conf as $delta => $deltaConf) {

      if (!$this->arrayMode) {
        if ((string) (int) $delta !== (string) $delta || $delta < 0) {
          // Fail on non-numeric and negative keys.
          throw new ConfToValueException("Deltas must be non-negative integers.");
        }
      }
      else {
        if (NULL !== $error = $this->deltaGetValidationError($delta)) {
          // Fail for illegal array keys.
          throw new ConfToValueException($error);
        }
      }

      if (!$this->skipBrokenItems) {
        $deltaValue = $this->configurator->confGetValue($deltaConf);
      }
      else {
        try {
          $deltaValue = $this->configurator->confGetValue($deltaConf);
        }
        catch (ConfToValueException $e) {
          continue;
        }
      }

      $values[$delta] = $deltaValue;
    }

    if (!$this->arrayMode) {
      // Re-key the array with serial numeric keys.
      $values = array_values($values);
    }
    elseif ($this->arrayMode === ArrayMode::MIXED) {
      // Re-key items with numeric keys, but preserve string keys.
      $values = array_merge($values, []);
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    if (!\is_array($conf)) {
      $conf = [];
    }
    return $summaryBuilder->buildSequence($this->configurator, $conf);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {

    if (!\is_array($conf)) {
      // Always start with one stub item, if this is a serial sequence.
      $conf = $this->arrayMode ? [] : [NULL];
    }
    elseif (!$this->arrayMode) {
      $conf = array_values($conf);
    }
    else {
      $conf = $this->filterAssocValues($conf);
    }

    $form = [
      '#input' => TRUE,
      '#default_value' => $conf,
      '#type' => 'themekit_container',
      '#attributes' => ['class' => ['cfrapi-sequence']],
      '#process' => [function (array $element, array &$form_state, array $form) {
        return $this->elementProcess($element, $form_state, $form);
      }],
      '#after_build' => [function (array $element, array &$form_state) {
        return $this->elementAfterBuild($element, $form_state);
      }],
      '#value_callback' => T::c('_cfrapi_generic_value_callback'),
      '#cfrapi_value_callback' => function (array $element, $input, array &$form_state) {
        return $this->elementValue($element, $input, $form_state);
      },
      '#pre_render' => [T::c('_cfrapi_generic_pre_render')],
      '#cfrapi_pre_render' => [function (array $element) {
        return $this->elementPreRender($element);
      }],
    ];

    if (NULL !== $label && '' !== $label && 0 !== $label) {
      // Prepend a label, like in a form element.
      /* @see \theme_form_element() */
      $form['label'] = [
        '#theme' => T::th('theme_form_element_label'),
        '#title' => $label,
        '#title_display' => 'above',
      ];
    }

    return $form;
  }

  /**
   * Called from a '#value_callback' callback.
   *
   * @param array $element
   *   The form element for this configurator.
   * @param array|mixed|false $input
   *   Raw value from form submission, or FALSE to use #default_value.
   * @param array $form_state
   *   The form state.
   *
   * @return array|bool|mixed
   *   Value to store in $form_state['values'] for this element.
   */
  private function elementValue(array $element, $input, array &$form_state) {

    if (empty($form_state['input'])) {
      // Return NULL to use default value.
      return NULL;
    }

    if (!\is_array($input)) {
      $input = !$this->arrayMode ? [NULL] : [];
    }
    elseif (!$this->arrayMode) {
      $input = array_values($input);
    }
    else {
      $input = $this->filterAssocValues($input);
    }

    // Set altered input for nested form elements.
    drupal_array_set_nested_value($form_state['input'], $element['#parents'], $input);

    // Create a reduced value for $form_state['values'] and $element['#value'].
    // More detail will be filled in by child elements.
    $value = array_fill_keys(array_keys($input), NULL);

    return $value;
  }

  /**
   * Called from a '#process' callback.
   *
   * @param array $element
   *   The form element for this configurator.
   * @param array $form_state
   *   The form state.
   * @param array $form
   *   The complete form.
   *
   * @return array
   *   Processed element.
   */
  private function elementProcess(array $element, array &$form_state, array $form) {
    $control_name = 'cfr-sequence-' . sha1(serialize($element['#parents']));

    $value = $element['#value'];

    $element['#control_name'] = $control_name;

    $form_build_id = $form['form_build_id']['#value'];
    $uniqid = sha1($form_build_id . serialize($element['#parents']));
    $element['#cfr_sequence_ajax_wrapper_id'] = $uniqid;

    $module_path = drupal_get_path('module', 'cfrapi');

    $element['#attached']['css'][] = $module_path . '/css/cfrapi.tabledrag.css';
    $element['#attached']['css'][] = $module_path . '/css/cfrapi.sequence-tabledrag.css';
    $element['#attached']['library'][] = ['system', 'jquery.cookie'];
    $element['#attached']['js'][] = 'misc/tabledrag.js';
    $element['#attached']['js'][] = $module_path . '/js/cfrapi.tabledrag.js';

    $element['items'] = [];
    $element['items']['#parents'] = $element['#parents'];

    /** @see _cfrapi_generic_pre_render() */
    $element['#prefix'] = $prefix = '<div id="' . $uniqid . '" class="cfr-sequence-ajax-wrapper">';
    $element['#suffix'] = '</div>';

    $parents = $element['#parents'];

    $itemLabelCallback = $this->itemLabelCallback
      ?: static::buildItemLabelCallback(
        t('Add another item'),
        t('Item @delta'));

    foreach ($value as $delta => $itemValue) {

      $element['items'][$delta] = [
        '#attributes' => ['class' => ['cfrapi-sequence-item']],
      ];

      $element['items'][$delta]['handle'] = [
        '#markup' => '<!-- -->',
      ];

      $element['items'][$delta]['item'] = [
        '#theme_wrappers' => [T::th('theme_container')],
        '#attributes' => [
          'class' => ['cfrapi-sequence-item-form'],
        ],
      ];

      $itemLabel = $itemLabelCallback($delta);

      if (1
        && isset($element['#title'])
        && '' !== ($sequenceLabel = $element['#title'])
      ) {
        $itemLabel = $sequenceLabel . ': ' . $itemLabel;
      }

      $itemForm = $this->configurator->confGetForm($itemValue, $itemLabel);

      $element['items'][$delta]['item']['form']['conf'] = $itemForm;

      $element['items'][$delta]['item']['form']['conf']['#parents'] = array_merge(
        $element['#parents'],
        [$delta]);

      $element['items'][$delta]['delete'] = [
        '#attributes' => ['class' => ['cfrapi-sequence-delete-container']],
      ];

      $element['items'][$delta]['delete']['button'] = [
        '#type' => 'submit',
        '#parents' => [$control_name, '.delete', $delta],
        // The button name must be explicitly set, otherwise it will be 'op'.
        '#name' => $control_name . '[.delete][' . $delta . ']',
        '#value' => t('Remove'),
        '#attributes' => ['class' => ['cfrapi-sequence-delete']],
        '#submit' => [
          function (array $form, array &$form_state) use ($parents, $delta) {
            $input = drupal_array_get_nested_value($form_state['input'], $parents);
            if (!\is_array($input)) {
              $input = [];
            }
            unset($input[$delta]);
            if (!$this->arrayMode) {
              $input = array_values($input);
            }
            elseif ($this->arrayMode === ArrayMode::MIXED) {
              // Re-key items with numeric keys, but preserve string keys.
              $input = array_merge($input, []);
            }
            drupal_array_set_nested_value($form_state['input'], $parents, $input);
            $form_state['rebuild'] = TRUE;
          },
        ],
        '#ajax' => [
          'callback' => FormUtil::f_findNestedElement('#prefix', $prefix),
          'wrapper' => $uniqid,
          'method' => 'replace',
        ],
        '#limit_validation_errors' => [],
      ];
    }

    if (empty($element['items'])) {
      $element['items']['#markup'] = '<!-- -->';
    }

    if (!$this->arrayMode) {
      $element['add_more_seq'] = [
        '#type' => 'submit',
        '#parents' => [$control_name, 'add_more_button'],
        // The button name must be explicitly set, otherwise it will be 'op'.
        '#name' => $control_name . '[add_more_button]',
        '#value' => $itemLabelCallback(NULL),
        '#attributes' => ['class' => ['cfrapi-sequence-add-more']],
        '#submit' => [
          static function (array $form, array &$form_state) use ($parents) {
            $input = drupal_array_get_nested_value($form_state['input'], $parents);
            if (!\is_array($input)) {
              $input = [];
            }
            // Add an empty item.
            $input[] = NULL;
            $input = array_values($input);
            drupal_array_set_nested_value($form_state['input'], $parents, $input);
            $form_state['rebuild'] = TRUE;
          },
        ],
        '#ajax' => [
          'callback' => FormUtil::f_findNestedElement('#prefix', $prefix),
          'wrapper' => $uniqid,
          'method' => 'replace',
        ],
        '#limit_validation_errors' => [],
      ];
    }
    else {
      $element['add_more_assoc'] = [
        '#type' => 'themekit_container',
        '#attributes' => [
          'class' => ['cfrapi-assoc_add_more'],
        ],
      ];

      $element['add_more_assoc']['handle'] = [
        '#markup' => '',
      ];

      $element['add_more_assoc']['form']['key'] = [
        '#parents' => [$control_name, 'add_more_key'],
        /* @see \theme_textfield() */
        '#type' => 'textfield',
        '#title' => t('String key for new item'),
        '#default_value' => '',
        '#required' => FALSE,
        '#size' => 36,
      ];

      // Only validate the textfield if the 'Add more' button is clicked.
      if (!empty($form_state['input']['_triggering_element_name'])) {
        $triggering_name = $form_state['input']['_triggering_element_name'];
        if ($triggering_name === $control_name . '[add_more_button]') {
          $element['add_more_assoc']['form']['key']['#element_validate'] = [
            function (array $element, array &$form_state) use ($value) {
              $add_more_key = $element['#value'];
              if ('' === $add_more_key) {
                if ($this->arrayMode !== ArrayMode::MIXED) {
                  form_error(
                    $element,
                    t('No key specified.'));
                }
              }
              elseif (NULL !== $error = $this->deltaGetValidationError($add_more_key)) {
                form_error($element, $error);
              }
              elseif (array_key_exists($add_more_key, $value)) {
                form_error(
                  $element,
                  t(
                    'Key @key already exists.',
                    [
                      '@key' => var_export($add_more_key, TRUE),
                    ]));
              }
            },
          ];
        }
      }

      $element['add_more_assoc']['add'] = [
        '#type' => 'themekit_container',
      ];

      $element['add_more_assoc']['add']['button'] = [
        '#type' => 'submit',
        '#parents' => [$control_name, 'add_more_button'],
        // The button name must be explicitly set, otherwise it will be 'op'.
        '#name' => $control_name . '[add_more_button]',
        '#value' => $itemLabelCallback(NULL),
        '#attributes' => ['class' => ['cfrapi-sequence-add-more']],
        '#submit' => [
          function (array $form, array &$form_state) use ($parents, $control_name) {
            $input = drupal_array_get_nested_value($form_state['input'], $parents);
            if (!\is_array($input)) {
              $input = [];
            }
            $add_more_key = $form_state['values'][$control_name]['add_more_key'];
            if ($add_more_key === '') {
              $input[] = NULL;
            }
            else {
              $input[$add_more_key] = NULL;
            }
            if ($this->arrayMode === ArrayMode::MIXED) {
              // Re-key items with numeric keys, but preserve string keys.
              $input = array_merge($input, []);
            }
            // Reset the add-more key for the next insertion.
            $form_state['input'][$control_name]['add_more_key'] = '';
            drupal_array_set_nested_value($form_state['input'], $parents, $input);
            $form_state['rebuild'] = TRUE;
          },
        ],
        '#ajax' => [
          'callback' => FormUtil::f_findNestedElement('#prefix', $prefix),
          'wrapper' => $uniqid,
          'method' => 'replace',
        ],
        '#limit_validation_errors' => [[$control_name]],
      ];
    }

    return $element;
  }

  /**
   * Called from the '#after_build' callback.
   *
   * @param array $element
   *   Original form element that was already processed.
   * @param array $form_state
   *   Processed form element.
   */
  private function elementAfterBuild(array $element, array &$form_state) {
    // Normalize the form value.
    $value = drupal_array_get_nested_value($form_state['values'], $element['#parents']);
    if ($value) {
      // All good.
      return $element;
    }
    // Set an empty array.
    drupal_array_set_nested_value($form_state['values'], $element['#parents'], []);
    return $element;
  }

  /**
   * Called from the '#pre_render' callback.
   *
   * @param array $element
   *   Original render element for this configurator form.
   *
   * @return array
   *   Processed render element.
   */
  private function elementPreRender(array $element) {

    $rows = [];
    foreach (element_children($element['items']) as $delta) {

      $item_element = $element['items'][$delta];
      $cells = [];
      foreach (element_children($item_element) as $colname) {
        $cell_element = $item_element[$colname];
        $cell = ['data' => $cell_element];
        if (!empty($cell_element['#attributes'])) {
          $cell += $cell_element['#attributes'];
        }
        $cells[] = $cell;
      }

      $row = ['data' => $cells];
      if (!empty($item_element['#attributes'])) {
        $row += $item_element['#attributes'];
      }
      $row['class'][] = 'draggable';
      $rows[] = $row;
      unset($element['items'][$delta]);
    }

    if ($this->arrayMode) {
      // Insert the 'Add another item' as a table row.
      if (isset($element['add_more_assoc'])) {
        $add_more_element = $element['add_more_assoc'];
        $cells = [];
        foreach (element_children($add_more_element) as $colname) {
          $cell_element = $add_more_element[$colname];
          $cell = ['data' => $cell_element];
          if (!empty($cell_element['#attributes'])) {
            $cell += $cell_element['#attributes'];
          }
          $cells[] = $cell;
        }

        $row = ['data' => $cells];
        if (!empty($add_more_element['#attributes'])) {
          $row += $add_more_element['#attributes'];
        }
        $rows[] = $row;
        unset($element['add_more_assoc']);
      }
    }

    if ([] === $rows) {
      $element['items'] = [
        '#type' => T::th('theme_themekit_container'),
        '#children' => '(' . t('no items') . ')',
      ];
      return $element;
    }

    $table_element = [
      '#theme' => T::th('theme_table'),
      // Disable disruptive behavior in bootstrap theme.
      '#context' => [
        'responsive' => FALSE,
        'hover' => FALSE,
      ],
      '#rows' => $rows,
      '#attributes' => $element['items']['#attributes'],
    ];

    $table_element['#attributes']['class'][] = 'cfrapi-tabledrag';

    $element['items'] = $table_element;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    if (NULL === $conf || [] === $conf) {
      return '[]';
    }

    if (!\is_array($conf)) {
      return $helper->incompatibleConfiguration($conf, "Configuration must be an array or NULL.");
    }

    $phpStatements = [];
    foreach ($conf as $delta => $deltaConf) {
      if (!$this->arrayMode) {
        if ((string) (int) $delta !== (string) $delta || $delta < 0) {
          // Fail on non-numeric and negative keys.
          return $helper->incompatibleConfiguration($conf, "Sequence array keys must be non-negative integers.");
        }
      }
      else {
        if (NULL !== $error = $this->deltaGetValidationError($delta)) {
          // Fail for illegal array keys.
          return $helper->incompatibleConfiguration($conf, $error);
        }
      }
      $phpStatements[$delta] = $this->configurator->confGetPhp($deltaConf, $helper);
    }

    if ($this->skipBrokenItems) {
      return $this->formatPhpStatements_skipBrokenItems($phpStatements);
    }

    $phpParts = [];
    foreach ($phpStatements as $delta => $deltaValuePhp) {
      if (!$this->arrayMode
        || ($this->arrayMode === ArrayMode::MIXED && is_int($delta))
      ) {
        $phpParts[] = ''
          . "\n// Sequence item #$delta"
          . "\n  $deltaValuePhp,";
      }
      else {
        $deltaPhp = var_export($delta, TRUE);
        $phpParts[] = ''
          . "\n  $deltaPhp => $deltaValuePhp,";
      }
    }

    $php = implode("\n", $phpParts);

    return "[$php\n]";
  }

  /**
   * Wraps php expressions into try/catch to skip broken items.
   *
   * @param string[] $phpStatements
   *   PHP expressions for sequence items, which may or may not throw an
   *   exception.
   *
   * @return string
   *   Combined php expression which won't throw an exception.
   */
  private function formatPhpStatements_skipBrokenItems(array $phpStatements) {

    $php = '';
    foreach ($phpStatements as $delta => $deltaValuePhp) {
      if (!$this->arrayMode
        || ($this->arrayMode === ArrayMode::MIXED && is_int($delta))
      ) {
        $deltaPhp = '';
      }
      else {
        $deltaPhp = var_export($delta, TRUE);
      }

      $php .= <<<EOT

// Sequence item #$delta
try {
  \$items[$deltaPhp] = $deltaValuePhp;
}
catch (\Exception \$e) {
  // Skip the item.
}
EOT;
    }

    return <<<EOT
call_user_func(
static function() {
  \$items = [];
  $php

  return \$items;
})
EOT;
  }

  /**
   * Filters an associative array of values, by removing illegal keys.
   *
   * @param mixed[] $values
   *   Unfiltered values.
   *
   * @return mixed[]
   *   Filtered values.
   */
  private function filterAssocValues(array $values) {
    foreach ($values as $delta => $value) {
      if ($this->deltaGetValidationError($delta)) {
        unset($values[$delta]);
      }
    }
    if ($this->arrayMode === ArrayMode::MIXED) {
      // Re-key items with numeric keys, but preserve string keys.
      $values = array_merge($values, []);
    }
    return $values;
  }

  /**
   * Checks if an item key is valid.
   *
   * This only applies to the 'assoc' variant of this class.
   *
   * @param string $delta
   *   String key to validate.
   *
   * @return string|null
   *   NULL if ok, or a message if validation fails.
   */
  protected function deltaGetValidationError($delta) {

    if ('' === $delta) {
      return t("Empty string key '' encountered.");
    }

    // Verify that the string key contains no disallowed characters.
    if (preg_match('@\W@', $delta, $m)) {
      return t(
        'String key @key with invalid character @char encountered.',
        [
          '@key' => var_export($delta, TRUE),
          '@char' => var_export($m[0], TRUE),
        ]);
    }

    return NULL;
  }

}
