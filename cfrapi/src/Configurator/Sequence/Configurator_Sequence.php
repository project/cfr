<?php

namespace Drupal\cfrapi\Configurator\Sequence;

/**
 * Legacy alias of Configurator_SequenceTabledrag.
 *
 * @deprecated
 *   Use the base class instead.
 *   See https://www.drupal.org/project/cfr/issues/3168185.
 */
class Configurator_Sequence extends Configurator_SequenceTabledrag {

}
