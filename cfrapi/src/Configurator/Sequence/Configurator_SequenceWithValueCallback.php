<?php

namespace Drupal\cfrapi\Configurator\Sequence;

use Donquixote\CallbackReflection\Util\CallbackUtil;
use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;

/**
 * Sequence configurator where the value is passed through a callback.
 *
 * @deprecated
 *   Use Configurator_SequenceTabledrag + Configurator_CallbackMono instead.
 *   See https://www.drupal.org/project/cfr/issues/3168183.
 *
 * @see \Drupal\cfrreflection\Configurator\Configurator_CallbackMono
 */
class Configurator_SequenceWithValueCallback extends Configurator_SequenceTabledrag {

  /**
   * @var callable
   */
  private $valueCallback;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $itemConfigurator
   *   Configurator for each item.
   * @param callable $valueCallback
   *   Callback to process the final value.
   *   If this is an anonymous function, PHP generation won't work.
   */
  public function __construct(ConfiguratorInterface $itemConfigurator, $valueCallback) {
    if (!\is_callable($valueCallback)) {
      throw new \InvalidArgumentException("Argument must be callable.");
    }
    $this->valueCallback = $valueCallback;
    parent::__construct($itemConfigurator);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    $value = parent::confGetValue($conf);
    if (!\is_array($value)) {
      return $value;
    }
    return \call_user_func($this->valueCallback, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    $callback_reflection = CallbackUtil::callableGetCallback(
      $this->valueCallback);
    if ($callback_reflection === NULL) {
      return $helper->notSupported($this, $conf, 'Failed to create CallbackReflection object.');
    }
    $sequence_php = parent::confGetPhp($conf, $helper);
    return $callback_reflection->argsPhpGetPhp([$sequence_php], $helper);
  }

}
