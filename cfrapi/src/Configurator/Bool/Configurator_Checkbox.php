<?php

declare(strict_types = 1);

// phpcs:ignore
namespace Drupal\cfrapi\Configurator\Bool;

/**
 * @deprecated
 *   The namespace 'Bool' can cause problems in some edge cases, because 'bool'
 *   is a reserved word in PHP.
 *   Therefore the class was moved to the parent namespace, and an alias left
 *   for BC - see the parent class.
 */
class Configurator_Checkbox extends \Drupal\cfrapi\Configurator\Configurator_Checkbox {

}
