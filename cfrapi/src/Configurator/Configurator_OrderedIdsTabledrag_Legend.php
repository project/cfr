<?php

namespace Drupal\cfrapi\Configurator;

use Drupal\cfrapi\Legend\LegendInterface;

/**
 * Configurator to choose from a list of available ids in custom order.
 *
 * In this implementation, ids are provided by a legend object.
 *
 * @internal
 * This component does not properly support copy/paste functionality.
 * Until it does, it is marked as "internal".
 */
class Configurator_OrderedIdsTabledrag_Legend extends Configurator_OrderedIdsTabledragBase {

  /**
   * @var \Drupal\cfrapi\Legend\LegendInterface
   */
  private $legend;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Legend\LegendInterface $legend
   *   Legend that holds the available ids.
   */
  public function __construct(LegendInterface $legend) {
    $this->legend = $legend;
  }

  /**
   * {@inheritdoc}
   */
  protected function getOptions() {
    $options = [];
    foreach ($this->legend->getSelectOptions() as $keyOrGroupLabel => $groupOrLabel) {
      if (!\is_array($groupOrLabel)) {
        $options[$keyOrGroupLabel] = $groupOrLabel;
      }
      else {
        $options += $groupOrLabel;
      }
    }
    return $options;
  }

}
