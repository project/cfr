<?php

namespace Drupal\cfrapi\Configurator;

use Drupal\cfrapi\ConfToValue\ConfToValueInterface;
use Drupal\cfrapi\RawConfigurator\RawConfiguratorInterface;

/**
 * Object which provides a UI to 'configure' a value or object.
 *
 * With the methods of the base interfaces, this can:
 * - create a value from given configuration data.
 * - generate PHP code to produce that same value.
 * - produce a Drupal form element to configure a value which can later be sent
 *   to one of the other methods e.g. to produce a value.
 * - produce a html/text summary from given configuration data.
 */
interface ConfiguratorInterface extends RawConfiguratorInterface, ConfToValueInterface {

}
