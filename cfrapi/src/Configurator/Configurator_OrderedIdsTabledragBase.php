<?php

/** @noinspection PhpDeprecationInspection */

namespace Drupal\cfrapi\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\ConfEmptyness\ConfEmptyness_Array;
use Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;
use Drupal\themekit\T;

/**
 * Base configurator to choose from a list of available ids in custom order.
 *
 * Configuration format: $[] = $id.
 * Value format: $[$id] = $id.
 * The ids can be string or integer.
 *
 * @internal
 * This component does not properly support copy/paste functionality.
 * Until it does, it is marked as "internal".
 */
abstract class Configurator_OrderedIdsTabledragBase implements OptionalConfiguratorInterface {

  /**
   * {@inheritdoc}
   *
   * @deprecated
   *   The method will be removed in 7.x-3.x.
   *   See https://www.drupal.org/project/cfr/issues/3165150.
   */
  public function getEmptyness() {
    return new ConfEmptyness_Array();
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {

    if (!\is_array($conf)) {
      $conf = [];
    }

    if (NULL !== $label && '' !== $label && 0 !== $label) {
      $form = [
        '#type' => 'container',
        '#title' => $label,
      ];
    }
    else {
      $form = [
        '#type' => 'container',
      ];
    }

    $form['#attributes']['class'][] = 'cfrapi-sequence';

    $form += [
      '#input' => TRUE,
      '#default_value' => $conf,
      '#process' => [function (array $element, array &$form_state, array $form) use ($conf) {
        return $this->elementProcess($element, $conf, $form_state, $form);
      }],
      '#value_callback' => T::c('_cfrapi_generic_value_callback'),
      '#cfrapi_value_callback' => function (array $element, $input, array &$form_state) {
        return $this->elementValue($element, $input, $form_state);
      },
    ];

    $form['items'] = [];

    return $form;
  }

  /**
   * Called from '#value_callback'.
   *
   * @param array $element
   *   The form element.
   * @param array|mixed|false $input
   *   Raw value from form submission, or FALSE to use #default_value.
   * @param array $form_state
   *   Form state.
   *
   * @return array|bool|mixed
   *   Value for the element.
   */
  private function elementValue(array $element, $input, array &$form_state) {

    if (FALSE === $input) {
      return isset($element['#default_value'])
        ? $element['#default_value']
        : [];
    }

    if (!\is_array($input)) {
      return [];
    }

    $options = $this->getOptions();

    $values = [];
    foreach ($input as $delta => $value) {
      if (!\is_string($value) && !\is_int($value)) {
        continue;
      }
      if ('stop_here' === $delta) {
        break;
      }
      if (!isset($options[$value])) {
        continue;
      }
      $values[$value] = $value;
    }

    drupal_array_set_nested_value($form_state['input'], $element['#parents'], $values);
    drupal_array_set_nested_value($form_state['values'], $element['#parents'], $values);

    return $values;
  }

  /**
   * Called from a '#process' callback.
   *
   * @param array $element
   *   Original form element.
   * @param array $conf
   *   Default configuration.
   * @param array $form_state
   *   Form state.
   * @param array $form
   *   Complete form.
   *
   * @return array
   *   Processed form element.
   */
  private function elementProcess(array $element, array $conf, array $form_state, array $form) {

    $values = $element['#value'];
    if (!\is_array($values)) {
      $values = [];
    }

    $itemElements = [];
    foreach ($this->getOptions() as $v => $label) {
      $itemElement = [];
      $itemElement['value'] = [
        '#input' => FALSE,
        '#type' => 'hidden',
        '#value' => $v,
        '#parents' => array_merge($element['#parents'], ['']),
        '#name' => $element['#name'] . '[]',
      ];
      $itemElement['label'] = [
        '#markup' => check_plain($label),
      ];
      $itemElements[$v] = $itemElement;
    }

    $element['items'] = [];

    // Add enabled items.
    foreach ($values as $v) {
      if (!isset($itemElements[$v])) {
        continue;
      }
      $element['items'][] = $itemElements[$v];
      unset($itemElements[$v]);
    }

    // Add a stopper element.
    $stopperElement = [];
    $stopperElement['value'] = [
      '#input' => FALSE,
      '#type' => 'hidden',
      '#value' => '_',
      '#parents' => array_merge($element['#parents'], ['stop_here']),
      '#name' => $element['#name'] . '[stop_here]',
    ];
    $stopperElement['label'] = [
      '#markup' => '<strong>' . check_plain(t('Items below are disabled.')) . '</strong>',
    ];
    $element['items'][] = $stopperElement;

    // Add remaining items.
    foreach ($itemElements as $itemElement) {
      $element['items'][] = $itemElement;
    }

    $element['items']['#pre_render'][] = T::c('_cfrapi_generic_pre_render');
    $element['items']['#cfrapi_pre_render'][] = function (array $itemsElement) {
      return $this->preRenderItems($itemsElement);
    };

    $element['#attached']['css'][] = drupal_get_path('module', 'cfrapi') . '/css/cfrapi.tabledrag.css';
    $element['#attached']['library'][] = ['system', 'jquery.cookie'];
    $element['#attached']['js'][] = 'misc/tabledrag.js';
    $element['#attached']['js'][] = drupal_get_path('module', 'cfrapi') . '/js/cfrapi.tabledrag.js';

    return $element;
  }

  /**
   * Called from a '#pre_render' callback.
   *
   * @param array $itemsElement
   *   Render element with the items tabledrag.
   *
   * @return array
   *   Processed render element.
   */
  private function preRenderItems(array $itemsElement) {

    $rows = [];
    foreach (element_children($itemsElement) as $delta) {
      if (!is_numeric($delta)) {
        continue;
      }
      $itemElement = $itemsElement[$delta];
      $cells = [];
      $cells[] = drupal_render($itemElement);

      $row = ['data' => $cells];
      if (isset($itemElement['value']) || TRUE) {
        $row['class'][] = 'draggable';
      }

      $rows[] = $row;
      unset($itemsElement[$delta]);
    }

    if ([] === $rows) {
      return $itemsElement;
    }

    $tableElement = [
      '#theme' => T::th('theme_table'),
      '#rows' => $rows,
    ];

    if (!empty($itemsElement['#attributes'])) {
      $tableElement['#attributes'] = $itemsElement['#attributes'];
    }

    $tableElement['#attributes']['class'][] = 'cfrapi-tabledrag';

    $itemsElement['table'] = $tableElement;

    return $itemsElement;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {

    if (!\is_array($conf)) {
      if (NULL !== $conf) {
        return t('Incompatible configuration');
      }
      $conf = [];
    }

    foreach ($conf as $k => $value) {
      if (!\is_string($value) && !\is_int($value)) {
        return t('Unexpected value at @key', [
          '@key' => var_export($k, TRUE),
        ]);
      }
    }

    $options = $this->getOptions();

    $values = array_combine($conf, $conf);

    $labels = array_intersect_key($options, $values);

    return implode(', ', $labels);
  }

  /**
   * Gets the summary for empty configuration.
   *
   * @return string
   *   Summary.
   */
  protected function getEmptySummary() {
    return '- ' . t('None') . ' -';
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    return $this->confDoGetValue($conf);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    try {
      $value = $this->confDoGetValue($conf);
    }
    catch (ConfToValueException $e) {
      return $helper->incompatibleConfiguration($conf, $e->getMessage());
    }

    return var_export($value, TRUE);
  }

  /**
   * Called from ->confGetValue() and ->confGetPhp().
   *
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   *   If valid, the format is: $[] = $id.
   *
   * @return mixed[]
   *   Value to be used in the application.
   *   Format: $[$id] = $id.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   *   If the configuration is not as expected.
   */
  private function confDoGetValue($conf) {

    if (!\is_array($conf)) {
      if (NULL !== $conf) {
        throw new ConfToValueException("Unexpected value found.");
      }
      $conf = [];
    }

    foreach ($conf as $k => $value) {
      if (!\is_string($value) && !\is_int($value)) {
        throw new ConfToValueException(strtr("Unexpected value at @key.", [
          // Variables in strtr() are not sanitized, but that's ok for an
          // exception message.
          '@key' => var_export($k, TRUE),
        ]));
      }
    }

    $options = $this->getOptions();

    $values = array_combine($conf, $conf);

    return array_intersect_key($values, $options);
  }

  /**
   * Gets the available ids with label.
   *
   * @return string[]
   *   Format: $[$id] = $label
   */
  abstract protected function getOptions();

}
