<?php

namespace Drupal\cfrapi\Configurator\Id;

/**
 * Flat select configurator with an added value callback.
 *
 * @todo The ->confGetPHP() is not implemented.
 *
 * @todo This is not used anywhere. Consider to remove.
 */
class Configurator_FlatOptionsAndValueCallbackSelect extends Configurator_FlatOptionsSelect {

  /**
   * @var callable
   */
  private $valueCallback;

  /**
   * Constructor.
   *
   * @param callable $valueCallback
   *   Callback to get a value for a chosen id.
   * @param string[] $options
   *   Flat select options.
   * @param bool $required
   *   TRUE, to make this required.
   * @param string|null $defaultId
   *   Default id.
   */
  public function __construct($valueCallback, array $options, $required = TRUE, $defaultId = NULL) {
    if (!\is_callable($valueCallback)) {
      throw new \InvalidArgumentException("Value callback must be callable.");
    }
    parent::__construct($options, $required, $defaultId);
    $this->valueCallback = $valueCallback;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    $id = parent::confGetValue($conf);
    return \call_user_func($this->valueCallback, $id);
  }

}
