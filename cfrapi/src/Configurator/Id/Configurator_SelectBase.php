<?php

/** @noinspection PhpDeprecationInspection */

namespace Drupal\cfrapi\Configurator\Id;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\ConfEmptyness\ConfEmptyness_Enum;
use Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;
use Drupal\themekit\T;

/**
 * Base class for select configurators.
 */
abstract class Configurator_SelectBase implements OptionalConfiguratorInterface {

  /**
   * @var bool
   */
  private $required;

  /**
   * @var string|null
   */
  private $defaultId;

  /**
   * Constructor.
   *
   * @param bool $required
   *   TRUE, to make the select element required.
   * @param string|null $defaultId
   *   Default value to use if the element is not required.
   */
  public function __construct($required = TRUE, $defaultId = NULL) {
    $this->required = $required;
    $this->defaultId = $defaultId;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {

    $form = [
      '#title' => $label,
      '#type' => 'select',
      '#options' => $this->getSelectOptions(),
      '#default_value' => $id = $this->confGetId($conf),
    ];

    if (NULL !== $id && !$this->idIsKnown($id)) {
      $form['#options'][$id] = t("Unknown id '@id'", ['@id' => $id]);
      $form['#illegal_options'][$id] = TRUE;
      $form['#element_validate'][] = T::c([self::class, 'elementValidateOrphanId']);
    }

    if ($this->required) {
      $form['#required'] = TRUE;
    }
    else {
      $form['#empty_value'] = '';
    }

    return $form;
  }

  /**
   * Callback for '#element_validate' which fails for the "orphan id".
   *
   * If a stored configuration contains an id that is not part of the available
   * options, an "orphan" option is added to the '#options' list, but the user
   * is forced to change the value to something else.
   *
   * @param array $element
   *   The select form element.
   */
  public static function elementValidateOrphanId(array $element) {
    $id = $element['#value'];
    if (!empty($element['#illegal_options'][$id])) {
      form_error(
        $element,
        t(
          "Unknown id %id. Maybe the id did exist in the past, but it currently does not.",
          ['%id' => $id]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {

    if (NULL !== $id = $this->confGetId($conf)) {
      return $this->idGetLabel($id);
    }

    return $this->getEmptySummary();
  }

  /**
   * Gets a summary for the case when an empty option was chosen.
   *
   * @return string
   *   Summary for the empty option.
   */
  protected function getEmptySummary() {
    return $this->required
      ? '- ' . t('Missing') . ' -'
      : '- ' . t('None') . ' -';
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {

    if (NULL === $id = $this->confGetId($conf)) {
      if ($this->required) {
        throw new ConfToValueException('Required id missing.');
      }

      return NULL;
    }

    if (!$this->idIsKnown($id)) {
      throw new ConfToValueException("Unknown id '$id'.");
    }

    return $id;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    if (is_numeric($conf)) {
      $conf = (string) $conf;
    }
    elseif (NULL === $conf || '' === $conf) {
      if ($this->required) {
        return $helper->incompatibleConfiguration($conf, "Required id missing.");
      }

      return var_export(NULL, TRUE);
    }
    elseif (!\is_string($conf)) {
      return $helper->incompatibleConfiguration($conf, "Id must be a string or integer.");
    }
    elseif (!$this->idIsKnown($conf)) {
      return $helper->incompatibleConfiguration($conf, "Unknown id.");
    }
    return $helper->export($conf);
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated
   *   The method will be removed in 7.x-3.x.
   *   See https://www.drupal.org/project/cfr/issues/3165150.
   */
  public function getEmptyness() {
    return $this->required
      ? NULL
      : new ConfEmptyness_Enum();
  }

  /**
   * Gets the id from configuration.
   *
   * @param mixed $conf
   *   Configuration.
   *   If valid, this is a string or integer id.
   *
   * @return string|null
   *   String representation of the id, or NULL for empty or invalid conf.
   */
  private function confGetId($conf) {

    if (is_numeric($conf)) {
      return (string) $conf;
    }

    if (NULL === $conf || '' === $conf || !\is_string($conf)) {
      return $this->defaultId;
    }

    return $conf;
  }

  /**
   * Gets the select options.
   *
   * @return string[]|string[][]|mixed[]
   *   Options suitable for the '#options' in a '#type' => 'select' element.
   *   Format (mixed):
   *   - $[$optgroup_label][$id] = $label
   *   - $[$id] = $label
   */
  abstract protected function getSelectOptions();

  /**
   * Gets the label for a specific id.
   *
   * @param string $id
   *   The id.
   *
   * @return string|null
   *   Label for the id, or NULL if not found.
   */
  abstract protected function idGetLabel($id);

  /**
   * Checks if an id is in the available options.
   *
   * @param string $id
   *   The id.
   *
   * @return bool
   *   TRUE, if the id is in the available options.
   */
  abstract protected function idIsKnown($id);

}
