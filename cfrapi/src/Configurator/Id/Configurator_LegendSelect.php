<?php

namespace Drupal\cfrapi\Configurator\Id;

use Drupal\cfrapi\Legend\LegendInterface;

/**
 * Select configurator using a legend object.
 */
class Configurator_LegendSelect extends Configurator_LegendSelectBase {

  /**
   * Static factory. Creates a required version.
   *
   * @param \Drupal\cfrapi\Legend\LegendInterface $legend
   *   Legend object which holds the select options.
   * @param string|null $defaultId
   *   Default id.
   *
   * @return self
   *   Created instance.
   */
  public static function createRequired(LegendInterface $legend, $defaultId = NULL) {
    return new self($legend, TRUE, $defaultId);
  }

  /**
   * Static factory. Creates an optional version.
   *
   * @param \Drupal\cfrapi\Legend\LegendInterface $legend
   *   Legend object which holds the select options.
   * @param string|null $defaultId
   *   Default id.
   *
   * @return self
   *   Created instance.
   */
  public static function createOptional(LegendInterface $legend, $defaultId = NULL) {
    return new self($legend, FALSE, $defaultId);
  }

}
