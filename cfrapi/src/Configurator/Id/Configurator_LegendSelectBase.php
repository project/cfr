<?php

namespace Drupal\cfrapi\Configurator\Id;

use Drupal\cfrapi\Legend\LegendInterface;

/**
 * Base class for select configurators with legend object.
 */
abstract class Configurator_LegendSelectBase extends Configurator_SelectBase {

  /**
   * @var \Drupal\cfrapi\Legend\LegendInterface
   */
  private $legend;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Legend\LegendInterface $legend
   *   Legend that holds the select options.
   * @param bool $required
   *   TRUE to make this required.
   * @param string|null $defaultId
   *   The default id.
   */
  public function __construct(LegendInterface $legend, $required = TRUE, $defaultId = NULL) {
    $this->legend = $legend;
    parent::__construct($required, $defaultId);
  }

  /**
   * {@inheritdoc}
   */
  protected function getSelectOptions() {
    return $this->legend->getSelectOptions();
  }

  /**
   * {@inheritdoc}
   */
  protected function idGetLabel($id) {
    return $this->legend->idGetLabel($id);
  }

  /**
   * {@inheritdoc}
   */
  protected function idIsKnown($id) {
    return $this->legend->idIsKnown($id);
  }

}
