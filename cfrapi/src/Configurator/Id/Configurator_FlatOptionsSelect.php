<?php

namespace Drupal\cfrapi\Configurator\Id;

/**
 * Select configurator without optgroups, with a fixed list of options.
 */
class Configurator_FlatOptionsSelect extends Configurator_SelectBase {

  /**
   * @var string[]
   */
  private $options;

  /**
   * Static factory. Creates a required version.
   *
   * @param string[] $options
   *   Options for the select element, without optgroups.
   *   Format: $[$id] = $label.
   * @param string|null $defaultId
   *   The default id.
   *
   * @return self
   *   Created instance.
   */
  public static function createRequired(array $options, $defaultId = NULL) {
    return new self($options, TRUE, $defaultId);
  }

  /**
   * Static factory. Creates an optional version.
   *
   * @param string[] $options
   *   Options for the select element, without optgroups.
   *   Format: $[$id] = $label.
   * @param string|null $defaultId
   *   The default id.
   *
   * @return self
   *   Created instance.
   */
  public static function createOptional(array $options, $defaultId = NULL) {
    return new self($options, FALSE, $defaultId);
  }

  /**
   * Constructor.
   *
   * @param string[] $options
   *   Options for the select element, without optgroups.
   *   Format: $[$id] = $label.
   * @param bool $required
   *   TRUE, to make this required.
   * @param string|null $defaultId
   *   The default id.
   */
  public function __construct(array $options, $required = TRUE, $defaultId = NULL) {
    parent::__construct($required, $defaultId);
    $this->options = $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSelectOptions() {
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  protected function idGetLabel($id) {
    return isset($this->options[$id])
      ? $this->options[$id]
      : $id;
  }

  /**
   * {@inheritdoc}
   */
  protected function idIsKnown($id) {
    return isset($this->options[$id]);
  }

}
