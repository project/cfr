<?php

namespace Drupal\cfrapi\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Configurator that stores fixed configuration which is also used as a value.
 *
 * It does not provide any options to the user.
 *
 * This is important if a plugin was configured with a specific context, and
 * later the configuration is evaluated without this context.
 *
 * @todo This is not used anywhere. Consider to remove.
 */
class Configurator_FixedConf implements ConfiguratorInterface {

  /**
   * @var string
   */
  private $value;

  /**
   * Constructor.
   *
   * @param string $value
   *   Value to store in config and to return in ->confGetValue().
   */
  public function __construct($value) {
    $this->value = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    return [
      '#type' => 'value',
      '#value' => $this->value,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    return var_export($this->value, TRUE);
  }

}
