<?php

namespace Drupal\cfrapi\Configurator\Unconfigurable;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\ValueProvider\ValueProviderInterface;

/**
 * Implementation that uses a value provider.
 *
 * @todo This seems to be unused. Consider to remove. See #3165296.
 */
class Configurator_FromValueProvider extends Configurator_OptionlessBase {

  /**
   * @var \Drupal\cfrapi\ValueProvider\ValueProviderInterface
   */
  private $valueProvider;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\ValueProvider\ValueProviderInterface $valueProvider
   *   Object that provides the value to return in ->confGetValue().
   */
  public function __construct(ValueProviderInterface $valueProvider) {
    $this->valueProvider = $valueProvider;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    return $this->valueProvider->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    return $this->valueProvider->getPhp($helper);
  }

}
