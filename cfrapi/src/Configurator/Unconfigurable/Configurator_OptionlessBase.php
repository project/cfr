<?php

namespace Drupal\cfrapi\Configurator\Unconfigurable;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\PossiblyOptionless\PossiblyOptionlessInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Base class for optionless configurators.
 */
abstract class Configurator_OptionlessBase implements ConfiguratorInterface, PossiblyOptionlessInterface {

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isOptionless() {
    return TRUE;
  }

}
