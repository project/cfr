<?php

namespace Drupal\cfrapi\Configurator\Unconfigurable;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * Implementation with a fixed value for ->confGetValue().
 */
class Configurator_FixedValue extends Configurator_OptionlessBase {

  /**
   * @var mixed
   */
  private $fixedValue;

  /**
   * @var string|false|null
   */
  private $php;

  /**
   * Constructor.
   *
   * @param mixed $fixedValue
   *   Fixed value to return in ->confGetValue().
   * @param string|false|null $php
   *   (optional) Fixed PHP code for ->confGetPHP().
   *   If NULL, ->confGetPHP() will attempt to export $fixedValue.
   *   If FALSE, the code from ->confGetPHP() will throw an exception.
   */
  public function __construct($fixedValue, $php = NULL) {
    $this->fixedValue = $fixedValue;
    $this->php = $php;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    return $this->fixedValue;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    if (FALSE === $this->php) {
      $type = \gettype($this->fixedValue);
      return $helper->notSupported($this, $conf, "This fixed value of type '$type' does not support code generation.");
    }

    if (NULL === $this->php) {
      return $helper->export($this->fixedValue);
    }

    return $this->php;
  }

}
