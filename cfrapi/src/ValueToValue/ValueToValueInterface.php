<?php

namespace Drupal\cfrapi\ValueToValue;

/**
 * Object which produces one value from another value.
 *
 * @deprecated
 *   This interface and its implementations will be removed in 7.x-3.x.
 *   See https://www.drupal.org/node/3168360.
 */
interface ValueToValueInterface {

  /**
   * Processes or replaces the value.
   *
   * @param mixed $value
   *   Original value.
   *
   * @return mixed
   *   Replaced value.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   *   If the value cannot be processed/transformed.
   */
  public function valueGetValue($value);

}
