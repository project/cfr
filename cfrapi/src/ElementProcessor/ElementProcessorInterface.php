<?php

namespace Drupal\cfrapi\ElementProcessor;

/**
 * Callable be used as form element '#process' callback.
 *
 * @see \form_builder()
 */
interface ElementProcessorInterface {

  /**
   * Processes the form element.
   *
   * @param array $element
   *   Original form element.
   * @param array $form_state
   *   Form state.
   *
   * @return array
   *   Processed form element.
   */
  public function __invoke(array $element, array &$form_state);

}
