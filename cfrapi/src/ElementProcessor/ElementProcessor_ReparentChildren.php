<?php

namespace Drupal\cfrapi\ElementProcessor;

/**
 * Element '#process' callback to append to '#parents' in child elements.
 *
 * @todo This is not used anywhere, consider to remove. See #3165296.
 */
class ElementProcessor_ReparentChildren implements ElementProcessorInterface {

  /**
   * @var string[][]
   */
  private $keysReparent;

  /**
   * Constructor.
   *
   * @param string[][] $keysReparent
   *   Trails of keys to modify the '#parents' properties of child elements.
   *   Format: $[$child_key][] = $parent_key_to_append.
   */
  public function __construct(array $keysReparent) {
    $this->keysReparent = $keysReparent;
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke(array $element, array &$form_state) {
    foreach ($this->keysReparent as $key => $parents) {
      if (isset($element[$key])) {
        $element[$key]['#parents'] = array_merge($element['#parents'], $parents);
      }
    }
    return $element;
  }

}
