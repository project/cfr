<?php

namespace Drupal\cfrapi\ConfToValue;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * Object which can produce a value from configuration.
 *
 * Most implementations also implement the child interface,
 * 'ConfiguratorInterface'.
 *
 * @see \Drupal\cfrapi\Configurator\ConfiguratorInterface
 */
interface ConfToValueInterface {

  /**
   * Produces a value from configuration.
   *
   * Each implementation and instance can have its own way of generating values
   * from config.
   *
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   *
   * @return mixed
   *   Value to be used in the application.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   *   If the configuration does not have the expected format, or no value can
   *   be created from the configuration.
   */
  public function confGetValue($conf);

  /**
   * Generates a PHP expression for the configuration.
   *
   * The value from the PHP expression shall be identical to the value produced
   * with the ->confGetValue() method above.
   *
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *   Helper object to export values and to manage failure.
   *
   * @return string
   *   PHP expression which, when executed, shall either return a value as
   *   expected for the given configuration, OR throw an exception.
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper);

}
