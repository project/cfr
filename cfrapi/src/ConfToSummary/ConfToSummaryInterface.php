<?php

namespace Drupal\cfrapi\ConfToSummary;

use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Object to produce a summary from given configuration.
 */
interface ConfToSummaryInterface {

  /**
   * Creates a summary for the given configuration.
   *
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface $summaryBuilder
   *   An object that controls the format of the summary.
   *
   * @return mixed|string|null
   *   A string summary is always allowed. But other values may be returned if
   *   $summaryBuilder generates them.
   *
   * @todo Rethink the signature and the helper object.
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder);

}
