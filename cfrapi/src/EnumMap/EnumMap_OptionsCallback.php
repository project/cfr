<?php

namespace Drupal\cfrapi\EnumMap;

/**
 * Implementation where the options are coming from a callback.
 */
class EnumMap_OptionsCallback implements EnumMapInterface {

  /**
   * @var callable
   */
  private $optionsCallback;

  /**
   * @var string[]|null
   */
  private $optionsBuffer;

  /**
   * Constructor.
   *
   * @param callable $optionsCallback
   *   Callback to provide the options.
   */
  public function __construct($optionsCallback) {
    $this->optionsCallback = $optionsCallback;
  }

  /**
   * {@inheritdoc}
   */
  public function idIsKnown($id) {
    if (NULL !== $this->optionsBuffer) {
      return array_key_exists($id, $this->optionsBuffer);
    }
    // Check if this is a "smart" callback..
    $result = \call_user_func($this->optionsCallback, $id);
    if (\is_array($result)) {
      $this->optionsBuffer = $result;
      return array_key_exists($id, $this->optionsBuffer);
    }

    return FALSE !== $result && NULL !== $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectOptions() {
    if (NULL === $this->optionsBuffer) {
      $this->optionsBuffer = \call_user_func($this->optionsCallback);
      if (!\is_array($this->optionsBuffer)) {
        // @todo Throw an exception or something?
        return $this->optionsBuffer = [];
      }
    }
    return $this->optionsBuffer;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetLabel($id) {
    if (NULL !== $this->optionsBuffer) {
      return array_key_exists($id, $this->optionsBuffer);
    }
    // Check if this is a "smart" callback..
    $result = \call_user_func($this->optionsCallback, $id);
    if (\is_array($result)) {
      $this->optionsBuffer = $result;
      return array_key_exists($id, $this->optionsBuffer);
    }

    if (\is_string($result)) {
      return $result;
    }

    return NULL;
  }

}
