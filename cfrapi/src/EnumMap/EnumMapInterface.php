<?php

namespace Drupal\cfrapi\EnumMap;

use Drupal\cfrapi\Legend\LegendInterface;

/**
 * Alias of LegendInterface.
 *
 * @todo Merge with LegendInterface.
 */
interface EnumMapInterface extends LegendInterface {

  /**
   * Checks if a string key is part of the available options.
   *
   * @param string|mixed $id
   *   String key or numeric id.
   *
   * @return bool
   *   TRUE, if the id is one of the available options.
   *
   * @todo This seems to be redundant with the parent method declaration.
   */
  public function idIsKnown($id);

}
