<?php

namespace Drupal\cfrapi\Legend;

/**
 * Object that provides options for a select configurator.
 */
interface LegendInterface {

  /**
   * Gets the available options in a format suitable for 'select' form element.
   *
   * @return mixed[]
   *   Options to be used in the '#options' array of a '#type' => 'select' form
   *   element.
   *   Format (mixed):
   *   - $[$group_label][$id] = $label, for options within an optgroup.
   *   - $[$id] = $label, for top-level options.
   */
  public function getSelectOptions();

  /**
   * Gets a label for a given string key / id.
   *
   * @param string|mixed $id
   *   The string key / id.
   *
   * @return string|null
   *   The item label, or NULL if not found.
   */
  public function idGetLabel($id);

  /**
   * Checks if a string key is part of the available options.
   *
   * @param string|mixed $id
   *   String key or numeric id.
   *
   * @return bool
   *   TRUE, if the id is one of the available options.
   */
  public function idIsKnown($id);

}
