<?php

namespace Drupal\cfrapi\Legend;

/**
 * Implementation with a fixed options array.
 */
class Legend_FixedOptions implements LegendInterface {

  /**
   * @var string[]|string[][]|mixed[]
   */
  private $deepOptions;

  /**
   * @var string[]
   */
  private $flatOptions = [];

  /**
   * Constructor.
   *
   * @param array $options
   *   Options, as for a '#type' => 'select' element.
   *   Format: $[$optgroup][$id] = $label; $[$id2] = $label2;.
   */
  public function __construct(array $options) {
    $this->deepOptions = $options;

    foreach ($options as $keyOrGroupLabel => $groupOrLabel) {
      if (!\is_array($groupOrLabel)) {
        $this->flatOptions[$keyOrGroupLabel] = $groupOrLabel;
      }
      else {
        $this->flatOptions += $groupOrLabel;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectOptions() {
    return $this->deepOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetLabel($id) {
    return isset($this->flatOptions[$id])
      ? $this->flatOptions[$id]
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function idIsKnown($id) {
    return isset($this->flatOptions[$id]);
  }

}
