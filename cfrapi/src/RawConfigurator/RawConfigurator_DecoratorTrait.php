<?php

namespace Drupal\cfrapi\RawConfigurator;

use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Decorator trait for RawConfiguratorInterface objects.
 *
 * This is almost the same as RawConfigurator_CfrDecoratorTrait, except the type
 * hint for the decorated configurator.
 *
 * @see \Drupal\cfrapi\RawConfigurator\RawConfiguratorInterface
 * @see \Drupal\cfrapi\RawConfigurator\RawConfigurator_CfrDecoratorTrait
 *
 * @todo This is not used anywhere. Instead, *_CfrDecoratorTrait is used.
 */
trait RawConfigurator_DecoratorTrait {

  /**
   * @var \Drupal\cfrapi\RawConfigurator\RawConfiguratorInterface
   */
  private $decorated;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\RawConfigurator\RawConfiguratorInterface $decorated
   *   Decorated raw configurator.
   */
  public function __construct(RawConfiguratorInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * Creates a form element to configure a value.
   *
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param string|null $label
   *   Label for the form element, specifying the purpose where it is used.
   *
   * @return array
   *   Drupal form element array.
   *
   * @see \Drupal\cfrapi\ConfToForm\ConfToFormInterface::confGetForm()
   */
  public function confGetForm($conf, $label) {
    return $this->decorated->confGetForm($conf, $label);
  }

  /**
   * Creates a summary for the given configuration.
   *
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface $summaryBuilder
   *   An object that controls the format of the summary.
   *
   * @return null|string
   *   The summary, or NULL for no summary.
   *
   * @see \Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface::confGetSummary()
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return $this->decorated->confGetSummary($conf, $summaryBuilder);
  }

}
