<?php

namespace Drupal\cfrapi\RawConfigurator;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Decorator trait for RawConfiguratorInterface.
 *
 * @see \Drupal\cfrapi\RawConfigurator\RawConfiguratorInterface
 */
trait RawConfigurator_CfrDecoratorTrait {

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private $decorated;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $decorated
   *   Decorated configurator.
   */
  public function __construct(ConfiguratorInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    return $this->decorated->confGetForm($conf, $label);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return $this->decorated->confGetSummary($conf, $summaryBuilder);
  }

}
