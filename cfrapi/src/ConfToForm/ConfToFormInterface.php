<?php

namespace Drupal\cfrapi\ConfToForm;

/**
 * Object to produce a form element to configure a configuration value.
 */
interface ConfToFormInterface {

  /**
   * Creates a form element to configure a value.
   *
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param string|null $label
   *   Label for the form element, specifying the purpose where it is used.
   *
   * @return array
   *   Drupal form element array.
   */
  public function confGetForm($conf, $label);

}
