<?php

namespace Drupal\cfrapi\PossiblyOptionless;

/**
 * Object that could be "optionless".
 *
 * This is used in combination with interfaces where implementations would
 * normally have configuration options, but in some cases might have none.
 */
interface PossiblyOptionlessInterface {

  /**
   * Checks if the object is optionless.
   *
   * The value of an optionless configurator does not at all depend on the
   * configuration.
   *
   * @return bool
   *   TRUE, if optionless.
   */
  public function isOptionless();

}
