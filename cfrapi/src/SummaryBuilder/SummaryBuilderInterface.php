<?php

namespace Drupal\cfrapi\SummaryBuilder;

use Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface;

/**
 * Object to be passed to ->confGetSummary() methods.
 *
 * @todo This is awkward. Rethink in future version.
 *
 * @see \Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface::confGetSummary()
 */
interface SummaryBuilderInterface {

  /**
   * Creates a summary for a drilldown element.
   *
   * @param string $label
   *   Label for the chosen drilldown option.
   * @param \Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface $optionsConfToSummary
   *   Nested configurator for the chosen drilldown option.
   * @param mixed $optionsConf
   *   Configuration to be sent to the sub-configurator.
   *
   * @return mixed
   *   Generated summary. Usually a string of HTML.
   */
  public function idConf($label, ConfToSummaryInterface $optionsConfToSummary, $optionsConf);

  /**
   * Starts a group of named settings.
   *
   * @return \Drupal\cfrapi\SummaryBuilder\Group\SummaryBuilderGroupInterface
   *   Group summary builder, for method-chaining.
   */
  public function startGroup();

  /**
   * Starts a group of unnamed settings.
   *
   * @return \Drupal\cfrapi\SummaryBuilder\Inline\SummaryBuilderInlineInterface
   *   Group summary builder, for method-chaining.
   *
   * @todo This seems to be unused. Consider to remove.
   */
  public function startInline();

  /**
   * Creates a summary for a sequence.
   *
   * @param \Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface $confToSummary
   *   Configurator for the sequence items.
   * @param mixed[] $confItems
   *   Configuration for each sequence item.
   *
   * @return mixed
   *   Generated summary. Usually a string of HTML.
   */
  public function buildSequence(ConfToSummaryInterface $confToSummary, array $confItems);

}
