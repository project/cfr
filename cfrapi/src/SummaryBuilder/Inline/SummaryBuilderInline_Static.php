<?php

namespace Drupal\cfrapi\SummaryBuilder\Inline;

use Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Default implementation.
 */
class SummaryBuilderInline_Static implements SummaryBuilderInlineInterface {

  /**
   * @var \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface
   */
  private $summaryBuilder;

  /**
   * @var string
   */
  private $separator;

  /**
   * @var string[]
   */
  private $pieces = [];

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface $summaryBuilder
   *   The host summary builder.
   * @param string $separator
   *   Separator to use between options.
   */
  public function __construct(SummaryBuilderInterface $summaryBuilder, $separator = ', ') {
    $this->summaryBuilder = $summaryBuilder;
    $this->separator = $separator;
  }

  /**
   * {@inheritdoc}
   */
  public function addSetting(ConfToSummaryInterface $confToSummary, $conf) {
    $this->pieces[] = $confToSummary->confGetSummary($conf, $this->summaryBuilder);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildSummary() {
    return implode($this->separator, $this->pieces);
  }

}
