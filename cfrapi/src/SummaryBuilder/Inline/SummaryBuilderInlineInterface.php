<?php

namespace Drupal\cfrapi\SummaryBuilder\Inline;

use Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface;

/**
 * Object to build a summary for a group of configurators.
 *
 * @see \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface::startGroup()
 *
 * @todo Remove this, after removing SummaryBuilderInterface::startGroup().
 */
interface SummaryBuilderInlineInterface {

  /**
   * Adds an unnamed group setting.
   *
   * @param \Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface $confToSummary
   *   Configurator for this item.
   * @param mixed $conf
   *   Configuration for this item.
   *
   * @return $this
   */
  public function addSetting(ConfToSummaryInterface $confToSummary, $conf);

  /**
   * Gets a summary for the items added so far.
   *
   * @return mixed
   *   Generated summary. Usually a HTML string.
   */
  public function buildSummary();

}
