<?php

namespace Drupal\cfrapi\SummaryBuilder\Group;

use Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface;

/**
 * Object to build a summary for a group of configurators.
 *
 * @see \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface::startGroup()
 */
interface SummaryBuilderGroupInterface {

  /**
   * Adds a setting for one of the member configurators.
   *
   * @param string $label
   *   Label for the group member.
   * @param \Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface $confToSummary
   *   The member configurator.
   * @param mixed $conf
   *   Configuration for this group member.
   *
   * @return $this
   */
  public function addSetting($label, ConfToSummaryInterface $confToSummary, $conf);

  /**
   * Gets the summary for the settings that were added so far.
   *
   * @return mixed
   *   Generated summary. Usually a html string.
   */
  public function buildSummary();

}
