<?php

namespace Drupal\cfrapi\SummaryBuilder\Group;

use Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Default implementation.
 */
class SummaryBuilderGroup_Static implements SummaryBuilderGroupInterface {

  /**
   * @var \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface
   */
  private $summaryBuilder;

  /**
   * @var string
   */
  private $summary = '';

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface $summaryBuilder
   *   Host summary builder.
   */
  public function __construct(SummaryBuilderInterface $summaryBuilder) {
    $this->summaryBuilder = $summaryBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public function addSetting($label, ConfToSummaryInterface $confToSummary, $conf) {
    $this->summary .= '<li>' . check_plain($label) . ': ' . $confToSummary->confGetSummary($conf, $this->summaryBuilder) . '</li>';

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildSummary() {
    return '' !== $this->summary
      ? '<ul>' . $this->summary . '</ul>'
      : NULL;
  }

}
