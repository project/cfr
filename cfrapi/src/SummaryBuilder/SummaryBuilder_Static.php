<?php

namespace Drupal\cfrapi\SummaryBuilder;

use Drupal\cfrapi\ConfToSummary\ConfToSummaryInterface;
use Drupal\cfrapi\SummaryBuilder\Group\SummaryBuilderGroup_Static;
use Drupal\cfrapi\SummaryBuilder\Inline\SummaryBuilderInline_Static;

/**
 * Default implementation.
 */
class SummaryBuilder_Static implements SummaryBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function idConf($label, ConfToSummaryInterface $optionsConfToSummary, $optionsConf) {
    $optionsConfSummary = $optionsConfToSummary->confGetSummary($optionsConf, $this);
    if (!\is_string($optionsConfSummary) || '' === $optionsConfSummary) {
      return check_plain($label);
    }
    return check_plain($label) . ': ' . $optionsConfSummary;
  }

  /**
   * {@inheritdoc}
   */
  public function startGroup() {
    return new SummaryBuilderGroup_Static($this);
  }

  /**
   * {@inheritdoc}
   */
  public function startInline() {
    return new SummaryBuilderInline_Static($this);
  }

  /**
   * {@inheritdoc}
   */
  public function buildSequence(ConfToSummaryInterface $confToSummary, array $confItems) {
    $summary = '';
    foreach ($confItems as $delta => $deltaConf) {
      if ((string) (int) $delta !== (string) $delta || $delta < 0) {
        // Fail on non-numeric and negative keys.
        return '- ' . t('Noisy configuration') . ' -';
      }
      $deltaSummary = $confToSummary->confGetSummary($deltaConf, $this);
      if (\is_string($deltaSummary) && '' !== $deltaSummary) {
        $summary .= '<li>' . $deltaSummary . '</li>';
      }
    }
    return '' !== $summary
      ? '<ol>' . $summary . '</ol>'
      : NULL;
  }

}
