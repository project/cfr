<?php

namespace Drupal\cfrapi\LegendItem;

/**
 * Represents an option in a select element, with group label and option label.
 */
interface LegendItemInterface {

  /**
   * Gets the option label.
   *
   * @return string
   *   The option label.
   */
  public function getLabel();

  /**
   * Gets the group label.
   *
   * @return string|null
   *   The group label, e.g. for a select optgroup.
   */
  public function getGroupLabel();

  /**
   * Immutable setter. Sets the two labels.
   *
   * @param string $label
   *   The option label.
   * @param string $groupLabel
   *   The group label.
   *
   * @return static
   *   Modified clone of $this.
   */
  public function withLabels($label, $groupLabel);

}
