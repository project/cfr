<?php

namespace Drupal\cfrapi\LegendItem;

/**
 * Default implementation with fixed values.
 */
class LegendItem implements LegendItemInterface {

  /**
   * @var string
   */
  private $label;

  /**
   * @var string|null
   */
  private $groupLabel;

  /**
   * Constructor.
   *
   * @param string $label
   *   Label for this option.
   * @param string|null $groupLabel
   *   Optroup label for this option.
   */
  public function __construct($label, $groupLabel) {
    $this->label = $label;
    $this->groupLabel = $groupLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupLabel() {
    return $this->groupLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function withLabels($label, $groupLabel) {
    return new self($label, $groupLabel);
  }

}
