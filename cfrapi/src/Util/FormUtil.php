<?php

namespace Drupal\cfrapi\Util;

use Drupal\themekit\T;

/**
 * Utility class with form-related functionality.
 */
final class FormUtil extends UtilBase {

  /**
   * Gets a callback to find an element in a nested elements tree.
   *
   * @param string $property
   *   Property name.
   * @param mixed $value
   *   Expected value for the property.
   *
   * @return \Closure
   *   Callback to find an element in a nested elements tree.
   *   This is suitable for $element['#ajax']['callback'].
   */
  public static function f_findNestedElement($property, $value) {
    return static function (array $form) use ($property, $value) {
      return self::findNestedElement($form, $property, $value);
    };
  }

  /**
   * Finds an element in a nested elements tree.
   *
   * @param array $element
   *   Parent render or form element.
   * @param string $property
   *   Property name.
   * @param mixed $value
   *   Expected property value.
   *
   * @return array|null
   *   The element with the expected property value, or NULL if not found.
   */
  public static function findNestedElement(array $element, $property, $value) {
    if (isset($element[$property]) && $element[$property] === $value) {
      return $element;
    }
    foreach (element_children($element) as $name) {
      $candidate = self::findNestedElement($element[$name], $property, $value);
      if ($candidate !== NULL) {
        return $candidate;
      }
    }
    return NULL;
  }

  /**
   * Form element #process callback.
   *
   * Makes the second form element depend on the first, with AJAX.
   *
   * @param array $element
   *   Original element with two children for which to build the dependency.
   * @param array $form_state
   *   Form state.
   * @param array $form
   *   Complete form.
   *
   * @return array
   *   Processed element.
   *
   * @deprecated
   *   This method is confusing, and should no longer be used.
   *   See #3164116.
   */
  public static function elementsBuildDependency(array $element, array &$form_state, array $form) {

    $keys = element_children($element);
    if (\count($keys) < 2) {
      return $element;
    }
    list($dependedKey, $dependingKey) = element_children($element);
    $dependedElement =& $element[$dependedKey];
    $dependingElement =& $element[$dependingKey];

    if (!\is_array($dependingElement)) {
      return $element;
    }

    $form_build_id = $form['form_build_id']['#value'];
    $uniqid = sha1($form_build_id . serialize($element['#parents']));

    // See https://www.drupal.org/node/752056 "AJAX Forms in Drupal 7".
    $dependedElement['#ajax'] = [
      'callback' => T::c('_cfrapi_depended_element_ajax_callback'),
      'wrapper' => $uniqid,
      'method' => 'replace',
    ];

    $dependedElement['#depending_element_reference'] =& $dependingElement;

    // Special handling of ajax for views.
    /* @see views_ui_edit_form() */
    // See https://www.drupal.org/node/1183418
    if (1
      && isset($form_state['view'])
      && module_exists('views_ui')
      && $form_state['view'] instanceof \view
    ) {
      // @todo Does this always work?
      $dependedElement['#ajax']['path'] = empty($form_state['url'])
        ? url($_GET['q'], ['absolute' => TRUE])
        : $form_state['url'];
    }

    if (empty($dependingElement)) {
      $dependingElement += [
        '#type' => 'themekit_container',
        '#markup' => '<!-- -->',
      ];
    }

    $dependingElement['#prefix'] = '<div id="' . $uniqid . '" class="cfrapi-depending-element-container">';
    $dependingElement['#suffix'] = '</div>';
    $dependingElement['#tree'] = TRUE;

    return $element;
  }

}
