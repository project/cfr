<?php

namespace Drupal\cfrapi\Util;

/**
 * Utility class to process configuration arrays.
 */
final class ConfUtil extends UtilBase {

  /**
   * Splits drilldown configuration into id and options.
   *
   * @param mixed $conf
   *   Original configuration.
   *   If valid, the format is [$k0 => $id, $k1 => $optionsConf].
   * @param string $k0
   *   The id key.
   * @param string $k1
   *   The options key.
   *
   * @return array
   *   Format: [$id, $optionsConf].
   */
  public static function confGetIdOptions($conf, $k0 = 'id', $k1 = 'options') {
    if (!isset($conf[$k0])) {
      return [NULL, NULL];
    }
    $id = $conf[$k0];
    if ('' === $id) {
      return [NULL, NULL];
    }
    if (!\is_string($id) && !\is_int($id)) {
      return [NULL, NULL];
    }
    if (!isset($conf[$k1])) {
      return [$id, NULL];
    }

    return [$id, $conf[$k1]];
  }

  /**
   * Reads keyed values from an associative configuration array.
   *
   * @param mixed $conf
   *   Original configuration.
   *   If valid, this is an associative array.
   * @param string[] $keys
   *   Keys to look up.
   *   Format: $[] = $key.
   *
   * @return mixed[]
   *   Values for the given keys. Gaps are filled with NULL.
   *   Format: $[] = $conf[$key]|NULL.
   *
   * @todo It seems this is not used at all.
   */
  public static function confExtractOptions($conf, array $keys) {
    if (!\is_array($conf) || empty($conf)) {
      return array_fill(0, \count($keys), NULL);
    }
    $return = [];
    foreach ($keys as $k) {
      $return[] = isset($conf[$k]) ? $conf[$k] : NULL;
    }
    return $return;
  }

  /**
   * Reads nested values from a configuration array.
   *
   * @param mixed $conf
   *   Configuration, typically a nested array.
   * @param string[] $parents
   *   Trail of keys indicating an array position within $conf.
   *
   * @return mixed
   *   Value at $conf[$parents[0]][$parents[1]].., or NULL if not found.
   */
  public static function confExtractNestedValue(&$conf, array $parents) {
    if ([] === $parents) {
      return $conf;
    }
    if (!\is_array($conf)) {
      return NULL;
    }
    $key = array_shift($parents);
    if (!isset($conf[$key])) {
      return NULL;
    }
    if ([] === $parents) {
      return $conf[$key];
    }
    if (!\is_array($conf[$key])) {
      return NULL;
    }
    return self::confExtractNestedValue($conf[$key], $parents);
  }

  /**
   * Merges an array value into a nested position of a tree-like configuration.
   *
   * @param array $conf
   *   (by reference) Configuration, typically a nested array.
   *   This will be modified.
   * @param string[] $parents
   *   Trail of keys indicating an array position within $conf.
   * @param array $value
   *   The array to merge into the nested position.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public static function confMergeNestedValue(array &$conf, array $parents, array $value) {
    if ([] === $parents) {
      $conf += $value;
      return TRUE;
    }
    $key = array_shift($parents);
    if (!isset($conf[$key])) {
      $conf[$key] = [];
    }
    elseif (!\is_array($conf[$key])) {
      return FALSE;
    }
    if ([] === $parents) {
      $conf[$key] += $value;
      return TRUE;
    }
    return self::confMergeNestedValue($conf[$key], $parents, $value);
  }

  /**
   * Sets a value into a nested position of a tree-like configuration.
   *
   * @param mixed $conf
   *   (by reference) Configuration, typically a nested array.
   * @param string[] $parents
   *   Trail of keys indicating an array position within $conf.
   * @param mixed $value
   *   The value to write into the nested position.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public static function confSetNestedValue(&$conf, array $parents, $value) {
    if ([] === $parents) {
      $conf = $value;
      return TRUE;
    }
    if (!\is_array($conf)) {
      return FALSE;
    }
    $key = array_shift($parents);
    if ([] === $parents) {
      $conf[$key] = $value;
      return TRUE;
    }
    if (!isset($conf[$key])) {
      $conf[$key] = [];
    }
    return self::confSetNestedValue($conf[$key], $parents, $value);
  }

  /**
   * Unsets a value in a nested position of a tree-like configuration.
   *
   * @param mixed $conf
   *   Configuration, typically a nested array.
   * @param string[] $parents
   *   Trail of keys indicating an array position within $conf.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public static function confUnsetNestedValue(&$conf, array $parents) {
    if ([] === $parents) {
      $conf = [];
      return TRUE;
    }
    if (!\is_array($conf)) {
      return FALSE;
    }
    $key = array_shift($parents);
    if ([] === $parents) {
      unset($conf[$key]);
      return TRUE;
    }
    if (!isset($conf[$key])) {
      return TRUE;
    }
    return self::confUnsetNestedValue($conf[$key], $parents);
  }

}
