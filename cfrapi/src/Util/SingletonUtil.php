<?php

namespace Drupal\cfrapi\Util;

/**
 * Replacement for drupal_static() and the $drupal_static_fast pattern.
 *
 * Allows to register references to variables for reset events.
 */
final class SingletonUtil extends UtilBase {

  /**
   * @var mixed[]
   *   Format: $[$key] =& $variable
   */
  private static $variables = [];

  /**
   * @var mixed[]
   */
  private static $defaults = [];

  /**
   * Registers a new singleton reference with a specific key.
   *
   * @param string $key
   *   Key to identify the value.
   * @param mixed $variable
   *   Reference to a (static) variable that should hold the singleton value.
   * @param mixed $value
   *   Value to assign to the variable.
   * @param mixed|null $default
   *   Default value to be used when resetting.
   *
   * @return mixed
   *   Copy of the value.
   */
  public static function init($key, &$variable, $value, $default = NULL) {
    self::$variables[$key] =& $variable;
    self::$defaults[$key] = $default;
    return $variable = $value;
  }

  /**
   * Resets all stored variables.
   */
  public static function resetAll() {
    foreach (self::$variables as $key => $variable) {
      self::$variables[$key] = self::$defaults[$key];
    }
  }

  /**
   * Resets the value for a specific key to its registered default.
   *
   * @param string $key_or_null
   *   Key to reset.
   */
  public static function resetKey($key_or_null) {
    if (array_key_exists($key_or_null, self::$variables)) {
      self::$variables[$key_or_null] = self::$defaults[$key_or_null];
    }
  }

}
