<?php

namespace Drupal\cfrapi\Util;

use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\Exception\PhpGenerationNotSupportedException;

/**
 * Utility class with methods to be called from generated code.
 */
final class CodegenFailureUtil extends UtilBase {

  /**
   * Throws an exception for the case when an object cannot be exported.
   *
   * @param string $class
   *   The class of the object to be exported.
   *   This can be read by looking at the exported code.
   *
   * @throws \Drupal\cfrapi\Exception\PhpGenerationNotSupportedException
   */
  public static function cannotExportObject($class) {
    throw new PhpGenerationNotSupportedException("Cannot export object to PHP.");
  }

  /**
   * Throws an exception when attempting to export an anonymous function.
   *
   * @param string $message
   *   Message for the exception.
   *
   * @throws \Drupal\cfrapi\Exception\PhpGenerationNotSupportedException
   */
  public static function cannotExportClosure($message) {
    throw new PhpGenerationNotSupportedException($message);
  }

  /**
   * Throws an exception when a recursion occured in code generation.
   *
   * @throws \Drupal\cfrapi\Exception\PhpGenerationNotSupportedException
   */
  public static function recursionDetected() {
    throw new PhpGenerationNotSupportedException("Recursion detected.");
  }

  /**
   * Throws an exception when an array has recursive references.
   *
   * @throws \Drupal\cfrapi\Exception\PhpGenerationNotSupportedException
   */
  public static function recursiveArray() {
    throw new PhpGenerationNotSupportedException("Cannot export recursive arrays.");
  }

  /**
   * Throws an exception when configuration is incompatible.
   *
   * @param mixed $conf
   *   Configuration. This can be read by looking at the generated code.
   * @param string $message
   *   Message for the exception.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   */
  public static function incompatibleConfiguration($conf, $message) {
    throw new ConfToValueException($message);
  }

  /**
   * Throws an exception when PHP generation is not supported.
   *
   * @param mixed $conf
   *   Configuration.
   * @param string $message
   *   Message for the exception.
   *
   * @throws \Drupal\cfrapi\Exception\PhpGenerationNotSupportedException
   */
  public static function notSupported($conf, $message) {
    throw new PhpGenerationNotSupportedException($message);
  }

}
