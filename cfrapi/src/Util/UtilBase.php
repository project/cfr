<?php

namespace Drupal\cfrapi\Util;

/**
 * Base class for utility classes with only static methods.
 *
 * Typically, subclasses will also be marked as final.
 */
abstract class UtilBase {

  /**
   * Private constructor.
   *
   * This prevents the class from being instantiated.
   */
  final private function __construct() {}

}
