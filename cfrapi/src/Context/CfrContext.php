<?php

namespace Drupal\cfrapi\Context;

/**
 * Default implementation.
 */
class CfrContext implements CfrContextInterface {

  /**
   * @var mixed[]
   */
  private $values;

  /**
   * @var string|null
   */
  private $machineName;

  /**
   * Static factory.
   *
   * This is mostly an alias of the constructor.
   *
   * @param mixed[] $values
   *   Values that could be passed to contextual parameters.
   *
   * @return static
   *   Created instance.
   */
  public static function create(array $values = []) {
    return new static($values);
  }

  /**
   * Constructor.
   *
   * @param mixed[] $values
   *   Values that could be passed to contextual parameters.
   */
  public function __construct(array $values = []) {
    $this->values = $values;
  }

  /**
   * Fluent setter. Adds / sets a value for a key / parameter name.
   *
   * @param string $paramName
   *   Key / parameter name.
   * @param mixed $value
   *   Value that should be passed to parameters with this name.
   *
   * @return $this
   */
  public function paramNameSetValue($paramName, $value) {
    $this->values[$paramName] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function paramValueExists(\ReflectionParameter $param) {
    if ($typeHintReflClass = $param->getClass()) {
      if ($typeHintReflClass->getName() === CfrContextInterface::class) {
        return TRUE;
      }
    }
    return $this->paramNameHasValue($param->getName());
  }

  /**
   * {@inheritdoc}
   */
  public function paramGetValue(\ReflectionParameter $param) {
    if ($typeHintReflClass = $param->getClass()) {
      if ($typeHintReflClass->getName() === CfrContextInterface::class) {
        return $this;
      }
    }
    return $this->paramNameGetValue($param->getName());
  }

  /**
   * {@inheritdoc}
   */
  public function paramNameHasValue($paramName) {
    return array_key_exists($paramName, $this->values);
  }

  /**
   * {@inheritdoc}
   */
  public function paramNameGetValue($paramName) {
    return array_key_exists($paramName, $this->values)
      ? $this->values[$paramName]
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMachineName() {
    return isset($this->machineName)
      ? $this->machineName
      : $this->machineName = md5(serialize($this->values));
  }

}
