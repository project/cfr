<?php

namespace Drupal\cfrapi\Context;

/**
 * Object that constrains the possible values in a configurator form element.
 *
 * This is passed to factory methods which create a configurator, like so:
 * - If the factory method has a parameter with type hint CfrContextInterface,
 *   and default value NULL, then the context object is passed as-is.
 *   Typically, the factory method will pass this on to nested configurators.
 * - (default implementation) If the factory method has a parameter without a
 *   type hint, but with a parameter name that matches one of the keys inside
 *   the context object, and default value NULL, then the value stored inside
 *   the context object for the given key is passed into the parameter.
 *
 * @todo Rethink this in a future version:
 *   - This is just a key/value store with obscure magic on top.
 *     Use a simple array instead?
 *   - Give up on the parameter magic?
 *   - Consider to pass this to form builder methods, instead of configurator
 *     factories.
 *   - When?
 *     Probably this has to wait for a big rewrite.
 */
interface CfrContextInterface {

  /**
   * Checks if the context has a value for a given parameter.
   *
   * @param \ReflectionParameter $param
   *   Parameter for which to check if a value is present.
   *   In the default implementation, this will be done based on the parameter
   *   name.
   *
   * @return bool
   *   TRUE, if a value exists that could/should be passed to the parameter.
   */
  public function paramValueExists(\ReflectionParameter $param);

  /**
   * Gets a value that could be passed to the given parameter.
   *
   * @param \ReflectionParameter $param
   *   Parameter which the value should be passed to.
   *
   * @return mixed|null
   *   The value to pass, or NULL.
   */
  public function paramGetValue(\ReflectionParameter $param);

  /**
   * Checks if the context has a value for a given parameter name.
   *
   * @param string $paramName
   *   Parameter name.
   *
   * @return bool
   *   TRUE, if a value exists that could/should be passed to the parameter.
   */
  public function paramNameHasValue($paramName);

  /**
   * Gets a value that could be passed to a parameter with the given name.
   *
   * @param string $paramName
   *   Parameter name.
   *
   * @return mixed|null
   *   The value to pass, or NULL.
   */
  public function paramNameGetValue($paramName);

  /**
   * Gets a machine name used for caching.
   *
   * @return string
   *   Machine name which can be used as a cache id.
   */
  public function getMachineName();

}
