<?php

namespace Drupal\cfrapi\CfrCodegenHelper;

use Donquixote\CallbackReflection\CodegenHelper\CodegenHelperInterface;

/**
 * Helper object that is passed into code-generating methods on configurators.
 *
 * The methods shall generate PHP code which, when executed, shall either return
 * a value, or throw an exception.
 *
 * The default implementation also keeps a log of failures.
 *
 * @see \Drupal\cfrapi\ConfToValue\ConfToValueInterface::confGetPhp()
 */
interface CfrCodegenHelperInterface extends CodegenHelperInterface {

  /**
   * Generates failing PHP code if recursion is detected during code generation.
   *
   * @param mixed $conf
   *   Configuration data that caused the recursion.
   * @param string $message
   *   Message to pass along.
   *
   * @return string
   *   Exception-throwing PHP expression.
   */
  public function recursionDetected($conf, $message);

  /**
   * Generates failing PHP code for unexpected configuration.
   *
   * The idea is that configuration is never "invalid" in itself, it can only be
   * incompatible with the expected schema.
   * This can occur e.g. for outdated configuration, if the schema has changed
   * since then, due to modules being disabled or updated.
   *
   * @param mixed $conf
   *   Configuration data that is incompatible with the current schema.
   * @param string $message
   *   Message to explain what is wrong with the configuration.
   *
   * @return string
   *   Exception-throwing PHP expression.
   */
  public function incompatibleConfiguration($conf, $message);

  /**
   * Generates failing PHP code if code generation is not supported.
   *
   * @param object $object
   *   Configurator or helper object that does not support code generation.
   * @param mixed $conf
   *   Configuration which was meant to be exported.
   * @param string $message
   *   Message explaining the problem.
   *
   * @return string
   *   Exception-throwing PHP expression.
   */
  public function notSupported($object, $conf, $message);

}
