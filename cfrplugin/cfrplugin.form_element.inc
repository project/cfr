<?php

use Drupal\cfrapi\Context\CfrContext;
use Drupal\cfrreflection\Util\StringUtil;
use Drupal\themekit\T;

/**
 * Implements hook_element_info().
 */
function cfrplugin_element_info() {

  return [
    'cfrplugin' => [
      '#process' => [T::c('_cfrplugin_element_process')],
      '#after_build' => [T::c('_cfrplugin_element_after_build')],
      '#cfrplugin_interface' => NULL,
      '#cfrplugin_context' => NULL,
    ],
    // Drilldown form elements can exist for many purposes.
    // But the main purpose is to choose the id and options for a cfr plugin.
    // While other drilldowns use a regular '#type' => 'container', the
    // drilldowns for cfrplugin use '#type' => '#cfrplugin_drilldown_container'.
    // For such containers we want to be able to add additional links and tools.
    'cfrplugin_drilldown_container' => [
      '#theme_wrappers' => [t::th('theme_themekit_container')],
      '#cfrplugin_interface' => NULL,
      '#cfrplugin_context' => NULL,
      '#pre_render' => [T::c('_cfrplugin_pre_render_drilldown_container')],
    ],
  ];
}

/**
 * Form element '#process' callback for 'cfrplugin' element type.
 *
 * @param array $element
 *   Original form element with '#type' => 'cfrplugin'.
 * @param array $form_state
 *   Form state.
 * @param array $form
 *   Entire form.
 *
 * @return array
 *   Processed form element.
 */
function _cfrplugin_element_process(array $element, array &$form_state, array &$form) {

  $cfrContext = $element['#cfrplugin_context'];
  if (is_array($cfrContext)) {
    $cfrContext = new CfrContext($cfrContext);
  }

  $configurator = empty($element['#required'])
    ? cfrplugin()->interfaceGetOptionalConfigurator($element['#cfrplugin_interface'], $cfrContext)
    : cfrplugin()->interfaceGetConfigurator($element['#cfrplugin_interface'], $cfrContext);

  $element['cfrplugin'] = $configurator->confGetForm($element['#default_value'], $element['#title']);

  $element['cfrplugin']['#parents'] = $element['#parents'];

  return $element;
}

/**
 * Form element '#value_callback' callback for 'cfrplugin' element.
 *
 * @param array $element
 *   Form element with '#type' => 'cfrplugin'.
 * @param mixed|false $input
 *   Value from a form submission, or FALSE if this is not during a submission.
 * @param array $form_state
 *   Form state.
 *
 * @return mixed
 *   Element value.
 */
function _cfrplugin_element_value(array $element, $input = FALSE, array $form_state = []) {

  if (FALSE !== $input) {
    return $input;
  }

  if (isset($element['#default_value'])) {
    return $element['#default_value'];
  }

  return [];
}

/**
 * Form element '#after_build' callback for 'cfrplugin' element type.
 *
 * @param array $element
 *   Original form element with '#type' => 'cfrplugin'.
 * @param array $form_state
 *   Form state.
 *
 * @return array
 *   Processed form element.
 *
 * @see \form_builder()
 */
function _cfrplugin_element_after_build(array $element, array &$form_state) {
  return $element;
}

/**
 * Callback for '#pre_render' for 'cfrplugin_drilldown_container' element type.
 *
 * @param array $element
 *   Original element.
 *
 * @return array
 *   Processed element.
 */
function _cfrplugin_pre_render_drilldown_container(array $element) {

  $interface = $element['#cfrplugin_interface'];
  $interface_label = StringUtil::interfaceGenerateLabel($interface);
  $replacements = ['@type' => $interface_label];
  $interface_slug = _cfrplugin_interface_slug($interface);

  $element['#attributes']['data:cfrplugin_interface'] = $interface;

  $tools = [];

  $tools['copy']['#markup']
    = '<a class="cfrplugin-copy">' . t('Copy "@type" configuration (local storage)', $replacements) . '</a>';

  $tools['paste']['#markup']
    = '<a class="cfrplugin-paste">' . t('Paste "@type" configuration (local storage)', $replacements) . '</a>';

  $tools[]['#markup'] = '<hr/>';

  if (user_access('view cfrplugin report')) {
    $tools['report'] = [
      '#markup' => l(
        t('About "@type" plugins', $replacements),
        "admin/reports/cfrplugin/$interface_slug",
        ['attributes' => [
          'target' => '_blank',
          'title' => t('Definitions for @type plugins.', $replacements),
        ]]),
    ];

    $tools['demo'] = [
      '#markup' => l(
        t('Demo / Code generator.'),
        "admin/reports/cfrplugin/$interface_slug/demo",
        ['attributes' => [
          'class' => ['cfrplugin-demo'],
          'target' => '_blank',
          'title' => t('Demo / Code generator.'),
        ]]),
    ];
  }

  $element['tools'] = [
    '#weight' => -999,
    '#type' => 'container',
    '#attributes' => [
      'class' => ['cfrplugin-tools'],
      'style' => 'display: none;',
    ],
    'top' => [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'cfrplugin-tools-handle',
      ],
      '#children' => t('tools'),
    ],
    'items' => $tools + [
      '#theme_wrappers' => [T::th('theme_container')],
      '#attributes' => [
        'class' => ['cfrplugin-tools-dropdown'],
      ],
    ],
  ];

  $element['#attached']['css'][] = drupal_get_path('module', 'cfrplugin') . '/css/cfrplugin.drilldown-tools.css';
  $element['#attached']['js'][] = drupal_get_path('module', 'cfrplugin') . '/js/cfrplugin.drilldown-tools.js';

  return $element;
}
