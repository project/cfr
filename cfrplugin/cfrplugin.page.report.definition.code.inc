<?php

use Drupal\cfrplugin\Util\DefinitionsUiUtil;
use Drupal\cfrplugin\Util\DefinitionUtil;
use Drupal\cfrplugin\Util\UiUtil;

/**
 * Page callback for a plugin definition code page.
 *
 * @param array $df
 *   Array with interface, plugin id, and plugin definition.
 *
 * @return array
 *   Page content render element.
 */
function _cfrplugin_report_definition_code_page(array $df) {

  $definition = $df['definition'];

  $file = DefinitionUtil::definitionGetFile($definition);

  $html = '';

  if (NULL === $file) {
    $html .= t('No PHP file could be found for the definition.');
  }
  elseif (!file_exists($file)) {
    $html .= t('File does not exist: @file', ['@file' => $file]);
  }
  elseif (!is_readable($file)) {
    $html .= t('File is not readable: @file', ['@file' => $file]);
  }
  else {
    $php = file_get_contents($file);
    $html .= UiUtil::highlightPhp($php);
  }

  $content = [];

  $content['messages'] = DefinitionsUiUtil::definitionBuildMessages(
    $definition);

  $content['code']['#markup'] = $html;

  return $content;
}
