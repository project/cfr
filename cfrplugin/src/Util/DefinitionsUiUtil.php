<?php

namespace Drupal\cfrplugin\Util;

use Drupal\cfrapi\Util\UtilBase;
use Drupal\cfrplugin\Hub\CfrPluginHub;
use Drupal\themekit\T;

/**
 * Utility class with static methods plugin definition reports.
 */
final class DefinitionsUiUtil extends UtilBase {

  /**
   * Builds messages to show above a list of plugin definitions.
   *
   * @param int $n_inactive
   *   Number of inactive plugin definitions.
   * @param int $n_broken
   *   Number of broken plugin definitions.
   *
   * @return array
   *   Render element.
   */
  public static function buildDefinitionsPageMessages($n_inactive, $n_broken) {
    $messages = [];
    if ($n_inactive) {
      $messages['status'][] = format_plural(
          $n_inactive,
          'Found @count inactive plugin.',
          'Found @count inactive plugins.')
        . '<br/>'
        . t('This can have legitimate reasons.');
    }
    if ($n_broken) {
      $messages['error'][] = format_plural(
          $n_broken,
          'Found @count broken plugin.',
          'Found @count broken plugins.')
        . '<br/>'
        . t('Clear the cache, or look for bugs in the plugin declaration.');
    }
    if (!$messages) {
      return [];
    }
    return [
      '#theme' => T::th('theme_themekit_status_messages'),
      '#messages' => $messages,
    ];
  }

  /**
   * Builds messages to show at the top of a page for a single definition.
   *
   * @param array $definition
   *   Plugin definition.
   *
   * @return array
   *   Render element.
   */
  public static function definitionBuildMessages(array $definition) {
    $messages = [];

    $services = CfrPluginHub::getContainer();
    $definitionToConfigurator = $services->definitionToConfigurator;

    try {
      $configurator = $definitionToConfigurator->definitionGetConfigurator($definition);
      if ($configurator !== NULL) {
        return [];
      }
      $messages['status'][] = t('This plugin is inactive.')
        . '<br/>'
        . t('This can have legitimate reasons.');
    }
    catch (\Exception $e) {
      $messages['error'][] = t('This plugin is broken.')
        . ':<br/>'
        . '<em>'
        . check_plain($e->getMessage())
        . '</em>'
        . '<br/>'
        . t('Clear the cache, or look for bugs in the plugin declaration.');
    }

    if (!$messages) {
      return [];
    }

    return [
      '#theme' => T::th('theme_themekit_status_messages'),
      '#messages' => $messages,
    ];
  }

}
