<?php

namespace Drupal\cfrplugin\Util;

use Drupal\cfrapi\Util\UtilBase;
use Drupal\cfrfamily\ArgDefToConfigurator\ArgDefToConfigurator_ConfiguratorObject;
use Drupal\cfrfamily\ArgDefToConfigurator\ArgDefToConfigurator_FixedValue;
use Drupal\cfrreflection\CfrGen\ArgDefToConfigurator\ArgDefToConfigurator_Callback;
use Drupal\cfrreflection\CfrGen\CallbackToConfigurator\CallbackToConfigurator_ConfiguratorFactory;
use Drupal\cfrreflection\CfrGen\CallbackToConfigurator\CallbackToConfiguratorInterface;
use Drupal\cfrreflection\ValueToCallback\CallableToCallback;
use Drupal\cfrreflection\ValueToCallback\ClassNameToCallback;

/**
 * Utility class with factories used in the service container.
 */
final class ServiceFactoryUtil extends UtilBase {

  /**
   * Gets mappers to create configurators from plugin definitions.
   *
   * @param \Drupal\cfrreflection\CfrGen\CallbackToConfigurator\CallbackToConfiguratorInterface $valueCtc
   *   Object to create a configurator from a value callback.
   *   This is used in _some_ of the mappers.
   *
   * @return \Drupal\cfrfamily\ArgDefToConfigurator\ArgDefToConfiguratorInterface[]
   *   Mappers to create configurators from plugin definitions.
   *   The array keys are keys in the definition.
   */
  public static function createDeftocfrMappers(CallbackToConfiguratorInterface $valueCtc) {
    $classToCallback = new ClassNameToCallback();
    $callbackToCallback = new CallableToCallback();
    $cfrFactoryCtc = new CallbackToConfigurator_ConfiguratorFactory();
    return [
      'configurator' => new ArgDefToConfigurator_ConfiguratorObject(),
      'configurator_class' => new ArgDefToConfigurator_Callback($classToCallback, 'configurator_arguments', $cfrFactoryCtc),
      'configurator_factory' => new ArgDefToConfigurator_Callback($callbackToCallback, 'configurator_arguments', $cfrFactoryCtc),
      'handler' => new ArgDefToConfigurator_FixedValue(),
      'handler_class' => new ArgDefToConfigurator_Callback($classToCallback, 'handler_arguments', $valueCtc),
      'handler_factory' => new ArgDefToConfigurator_Callback($callbackToCallback, 'handler_arguments', $valueCtc),
      'class' => new ArgDefToConfigurator_Callback($classToCallback, 'configurator_arguments', $cfrFactoryCtc),
      'factory' => new ArgDefToConfigurator_Callback($callbackToCallback, 'configurator_arguments', $cfrFactoryCtc),
    ];
  }

}
