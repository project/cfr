<?php

namespace Drupal\cfrplugin\Util;

use Donquixote\CallbackReflection\Util\CodegenUtil;
use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelper;
use Drupal\cfrapi\Util\UtilBase;

/**
 * Utility class with static methods for the UI pages.
 */
final class UiUtil extends UtilBase {

  /**
   * Gets PHP code from a class file.
   *
   * @param string $class
   *   A PHP class name.
   *
   * @return string|null
   *   File contents of the file which declares the class, or NULL if not found.
   */
  public static function classGetPhp($class) {

    try {
      $reflectionClass = new \ReflectionClass($class);
    }
    catch (\ReflectionException $e) {
      return NULL;
    }

    $filename = $reflectionClass->getFileName();

    if (FALSE === $filename || !is_readable($filename)) {
      return NULL;
    }

    return file_get_contents($filename);
  }

  /**
   * Enhances raw PHP code with decorative HTML for syntax colors.
   *
   * This is taking inspiration from a function found in the 'codefilter'
   * module.
   *
   * @param string $php
   *   Raw PHP code.
   *
   * @return string
   *   Decorated PHP code in HTML format.
   *
   * @see \codefilter_process_php()
   * @see \highlight_string()
   */
  public static function highlightPhp($php) {
    // Note, pay attention to odd preg_replace-with-/e behaviour on slashes.
    // Undo possible linebreak filter conversion.
    $text = preg_replace('@</?(br|p)\s*/?>@', '', str_replace('\"', '"', $php));
    // Undo the escaping in the prepare step.
    $text = decode_entities($text);
    // Trim leading and trailing linebreaks.
    $text = trim($text, "\r\n");
    // Highlight as PHP.
    $text = '<div class="codeblock"><pre>' . highlight_string($text, TRUE) . '</pre></div>';

    // Fix spaces.
    $text = preg_replace('@&nbsp;(?!&nbsp;)@', ' ', $text);
    // A single space before text is ignored by browsers. If a single space
    // follows a break tag, replace it with a non-breaking space.
    $text = preg_replace('@<br /> ([^ ])@', '<br />&nbsp;$1', $text);

    return $text;
  }

  /**
   * Checks if a string has the format of an interface name.
   *
   * @param string $interface
   *   String which could be an interface name.
   *
   * @return bool
   *   TRUE, if the string is a valid interface name.
   */
  public static function interfaceNameIsValid($interface) {
    $fragment = DRUPAL_PHP_FUNCTION_PATTERN;
    $backslash = preg_quote('\\', '/');
    $regex = '/^' . $fragment . '(' . $backslash . $fragment . ')*$/';
    return 1 === preg_match($regex, $interface);
  }

  /**
   * Displays an exception for debugging purposes.
   *
   * @param \Exception $e
   *   The exception.
   *
   * @return array
   *   Render element showing the exception.
   */
  public static function displayException(\Exception $e) {

    $file = $e->getFile();
    $e_class = \get_class($e);
    $e_reflection = new \ReflectionObject($e);

    return [
      'text' => [
        '#markup' => ''
          // @todo This should probably be in a template. One day.
          . '<dl>'
          . '  <dt>' . t('Exception in line %line of %file', ['%line' => $e->getLine(), '%file' => basename($file)]) . '</dt>'
          . '  <dd><code>' . check_plain($file) . '</code></dd>'
          . '  <dt>' . t('Exception class: %class', ['%class' => $e_reflection->getShortName()]) . '</dt>'
          . '  <dd>' . check_plain($e_class) . '</dt>'
          . '  <dt>' . t('Exception message:') . '</dt>'
          . '  <dd>' . check_plain($e->getMessage()) . '</dd>'
          . '</dl>',
      ],
      'trace_label' => [
        '#markup' => '<div>' . t('Exception stack trace') . ':</div>',
      ],
      'trace' => self::dumpData(BacktraceUtil::exceptionGetRelativeNicetrace($e)),
    ];
  }

  /**
   * Displays a data dump in a fieldset.
   *
   * @param mixed $data
   *   The data to display.
   * @param string $fieldset_label
   *   Label for the fieldset.
   *
   * @return array
   *   Render element.
   */
  public static function dumpDataInFieldset($data, $fieldset_label) {

    return self::dumpData($data)
      + [
        '#type' => 'fieldset',
        '#title' => $fieldset_label,
      ];
  }

  /**
   * Displays a data dump.
   *
   * @param mixed $data
   *   Data to dump.
   *
   * @return array
   *   Render element.
   */
  public static function dumpData($data) {

    $element = [];

    if (\function_exists('krumong')) {
      $element['dump']['#markup'] = krumong()->dump($data);
    }
    elseif (\function_exists('dpm')) {
      $element['dump']['#markup'] = krumo_ob($data);
      $element['notice']['#markup'] = '<p>' . t('Install krumong to see private and protected member variables.') . '</p>';
    }
    else {
      $element['notice']['#markup'] = t('No dump utility available. Install devel and/or krumong.');
    }

    return $element;
  }

  /**
   * Exports data as formatted PHP expression.
   *
   * @param mixed $data
   *   Data to export.
   *
   * @return string
   *   HTML with data export.
   */
  public static function exportData($data) {

    $helper = new CfrCodegenHelper();

    $php = $helper->export($data);
    $php = CodegenUtil::autoIndent($php, '  ');

    // Prepend the opening tag, because highlight_string() needs it.
    $php = "<?php $php";

    // Undo the escaping in the prepare step.
    $text = decode_entities($php);

    // Trim leading and trailing linebreaks.
    $text = trim($text, "\r\n");

    // Add syntax colors.
    $html = highlight_string($text, TRUE);

    // Remove the opening tag.
    $html = preg_replace('@<span[^>]*>&lt;\\?php&nbsp;</span>@', '', $html, 1);

    return $html;
  }

}
