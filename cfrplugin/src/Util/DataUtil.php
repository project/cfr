<?php

namespace Drupal\cfrplugin\Util;

/**
 * Utility functions to sanitize request data from json.
 */
class DataUtil {

  /**
   * Parses a json string, and sanitizes the data.
   *
   * @param string $json
   *   Json string.
   * @param bool $success
   *   Will be set to TRUE if successful.
   *
   * @return mixed|null
   *   Decoded and sanitized data from json, or NULL on failure.
   */
  public static function jsonDecodeSafe($json, &$success = FALSE) {
    $data = self::jsonDecode($json, $success);
    self::dataStripDangerousKeys($data);
    return $data;
  }

  /**
   * Parses a json string.
   *
   * @param string $json
   *   Json string.
   * @param bool $success
   *   Will be set to TRUE if successful.
   *
   * @return mixed|null
   *   Decoded data from json, or NULL on failure.
   */
  public static function jsonDecode($json, &$success = FALSE) {
    $success = FALSE;
    if ('' === $json) {
      return NULL;
    }
    $data = json_decode($json, TRUE);
    if (json_last_error()) {
      return NULL;
    }
    $success = TRUE;
    return $data;
  }

  /**
   * Removes dangerous array keys from untrusted data.
   *
   * This is a simplified version of the core request sanitizer.
   *
   * @param mixed $data
   *   Data to be cleaned up.
   *
   * @see \DrupalRequestSanitizer::stripDangerousValues()
   */
  public static function dataStripDangerousKeys(&$data) {
    if (is_array($data)) {
      self::arrayStripDangerousKeysRecursive($data);
    }
  }

  /**
   * Removes dangerous array keys from an untrusted nested array.
   *
   * @param array $data
   *   Data to be cleaned up.
   */
  private static function arrayStripDangerousKeysRecursive(array &$data) {
    foreach ($data as $key => $value) {
      if ($key !== '' && is_string($key) && $key[0] === '#') {
        unset($data[$key]);
      }
      elseif (is_array($value)) {
        self::arrayStripDangerousKeysRecursive($data[$key]);
      }
    }
  }

}
