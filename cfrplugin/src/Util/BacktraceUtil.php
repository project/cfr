<?php

namespace Drupal\cfrplugin\Util;

use Donquixote\Nicetrace\Util\NicetraceUtil;
use Drupal\cfrapi\Util\UtilBase;

/**
 * Utility class for backtrace processing.
 */
final class BacktraceUtil extends UtilBase {

  /**
   * Gets a "nicetrace" for the relevant part of an exception backtrace.
   *
   * @param \Exception $e
   *   The exception.
   *
   * @return array[]
   *   Nicetrace items from exception that are not part of current backtrace.
   */
  public static function exceptionGetRelativeNicetrace(\Exception $e) {
    $relative_backtrace = self::traceDiff($e->getTrace());
    return self::nicetrace($relative_backtrace);
  }

  /**
   * Subtracts one backtrace from another.
   *
   * @param array[] $trace
   *   First backtrace.
   * @param array[]|null $othertrace
   *   (optional) Trace to subtract from the first trace.
   *   If not provided, the current backtrace is used.
   *
   * @return array[]
   *   Part of $trace that is not shared in $othertrace.
   */
  public static function traceDiff(array $trace, array $othertrace = NULL) {

    if (NULL === $othertrace) {
      $othertrace = debug_backtrace();
    }

    $trace_reverse = array_reverse($trace);
    $othertrace_reverse = array_reverse($othertrace);

    $trace_reverse_diff = self::reverseTraceDiff($trace_reverse, $othertrace_reverse);

    return array_reverse($trace_reverse_diff);
  }

  /**
   * Helper method. Subtracts one reverse backtrace from another.
   *
   * @param array[] $trace
   *   Reverse trace.
   * @param array[] $othertrace
   *   Reverse trace to subtract from the first trace.
   *
   * @return array[]
   *   Part of $trace that is not shared in $othertrace.
   */
  private static function reverseTraceDiff(array $trace, array $othertrace) {

    for ($i = 0;; ++$i) {
      if (!isset($trace[$i], $othertrace[$i])) {
        break;
      }
      // @todo Is this comparison reliable and sufficient?
      if ($trace[$i] !== $othertrace[$i]) {
        break;
      }
    }

    return \array_slice($trace, $i);
  }

  /**
   * Transforms a backtrace in a more human-readable way.
   *
   * @param array[] $backtrace
   *   Original backtrace, as returned by exceptions or debug_backtrace().
   *
   * @return array[]
   *   Transformed backtrace.
   */
  public static function nicetrace(array $backtrace) {

    $paths = [DRUPAL_ROOT => \strlen(DRUPAL_ROOT) + 1];
    return NicetraceUtil::backtraceGetNicetrace($backtrace, $paths);
  }

}
