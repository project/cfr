<?php

namespace Drupal\cfrplugin\ConfDisplay;

/**
 * Object to get a render element for a plugin configuration.
 *
 * This is used on UI pages which contain a configurator form and several
 * preview elements.
 */
interface ConfDisplayInterface {

  /**
   * Gets a render element from given configuration.
   *
   * @param mixed $conf
   *   Configuration that could be sent to a configurator.
   *
   * @return array
   *   Render element based on the configuration..
   */
  public function buildWithConf($conf);

}
