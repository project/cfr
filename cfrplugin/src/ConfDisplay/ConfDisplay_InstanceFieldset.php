<?php

namespace Drupal\cfrplugin\ConfDisplay;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrplugin\Util\UiUtil;

/**
 * Implementation which shows a dump of the object instance in a fieldset.
 */
class ConfDisplay_InstanceFieldset extends ConfDisplay_InstanceBase {

  /**
   * @var string
   */
  private $interface;

  /**
   * @var bool|null
   */
  private $collapsed;

  /**
   * Constructor.
   *
   * @param string $interface
   *   Interface name.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator.
   * @param bool|null $collapsed
   *   TRUE, to collapse the fieldset initially.
   */
  public function __construct($interface, ConfiguratorInterface $configurator, $collapsed = NULL) {
    $this->interface = $interface;
    parent::__construct($configurator);
    $this->collapsed = $collapsed;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildException(ConfToValueException $e, $non_empty_conf) {
    $fieldset = $this->buildFieldset();

    if ($e instanceof ConfToValueException) {
      drupal_set_message(t('The configuration is incomplete or invalid.'), 'warning');
    }
    else {
      drupal_set_message(t('The confGetValue() method threw an unexpected type of exception.'), 'warning');
    }

    $fieldset['content'] = UiUtil::displayException($e);
    $fieldset['#title'] = t('Exception');
    $fieldset['#description'] = '<p>'
      . t('Cfrplugin was unable to generate a behavior object for the given configuration.')
      . '</p>';

    return $fieldset;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildInstance($instance, $non_empty_conf) {
    $fieldset = $this->buildFieldset();

    $fieldset['content'] = UiUtil::dumpData($instance);

    if (!$instance instanceof $this->interface) {

      drupal_set_message(
        t('The confGetValue() method had an unexpected return value.'),
        'warning');

      $fieldset['#title'] = t('Unexpected value or object');
      return $fieldset;
    }

    $fieldset['#title'] = t('Behavior object');

    return $fieldset;
  }

  /**
   * @return array
   *   Render element.
   */
  private function buildFieldset() {

    $fieldset = [
      '#type' => 'fieldset',
    ];

    if ($this->collapsed) {
      $fieldset['#attributes']['class'] = ['collapsible', 'collapsed'];
      $fieldset['#attached']['library'][] = ['system', 'drupal.collapse'];
    }
    elseif (FALSE === $this->collapsed) {
      $fieldset['#attributes']['class'] = ['collapsible'];
      $fieldset['#attached']['library'][] = ['system', 'drupal.collapse'];
    }

    return $fieldset;
  }

}
