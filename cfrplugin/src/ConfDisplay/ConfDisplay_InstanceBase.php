<?php

namespace Drupal\cfrplugin\ConfDisplay;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\Exception\ConfToValueException;

/**
 * Base class for conf displays that show the object instance.
 */
abstract class ConfDisplay_InstanceBase implements ConfDisplayInterface {

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private $configurator;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator.
   */
  public function __construct(ConfiguratorInterface $configurator) {
    $this->configurator = $configurator;
  }

  /**
   * {@inheritdoc}
   */
  public function buildWithConf($conf) {
    if (!$conf) {
      return [];
    }

    try {
      $instance = $this->configurator->confGetValue($conf);
    }
    catch (ConfToValueException $e) {
      // This case is dealt with elsewhere, hopefully.
      return $this->buildException($e, $conf);
    }

    return $this->buildInstance($instance, $conf);
  }

  /**
   * Creates a render element to display an exception.
   *
   * @param \Drupal\cfrapi\Exception\ConfToValueException $e
   *   Exception that was thrown in a configurator's confGetValue() method.
   * @param mixed $non_empty_conf
   *   Non-empty configuration that was sent to the configurator.
   *
   * @return array
   *   Render element to display the exception.
   */
  abstract protected function buildException(ConfToValueException $e, $non_empty_conf);

  /**
   * Creates a render element to display an object returned from a configurator.
   *
   * @param mixed $instance
   *   Instance returned from the ->confGetValue() method of a configurator.
   * @param mixed $non_empty_conf
   *   Non-empty configuration that was sent to the configurator.
   *
   * @return array
   *   Render element showing the instance.
   */
  abstract protected function buildInstance($instance, $non_empty_conf);

}
