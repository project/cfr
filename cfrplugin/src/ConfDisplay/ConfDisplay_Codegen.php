<?php

namespace Drupal\cfrplugin\ConfDisplay;

use Donquixote\CallbackReflection\Util\CodegenUtil;
use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelper;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrplugin\Util\UiUtil;

/**
 * Display that shows generated code.
 */
class ConfDisplay_Codegen implements ConfDisplayInterface {

  /**
   * @var string
   */
  private $interface;

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private $configurator;

  /**
   * Constructor.
   *
   * @param string $interface
   *   Interface name.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator.
   */
  public function __construct($interface, ConfiguratorInterface $configurator) {
    $this->interface = $interface;
    $this->configurator = $configurator;
  }

  /**
   * {@inheritdoc}
   */
  public function buildWithConf($conf) {
    if (!$conf) {
      return [];
    }

    $helper = new CfrCodegenHelper();

    $php = $this->configurator->confGetPhp($conf, $helper);
    $php = CodegenUtil::autoIndent($php, '  ', '    ');
    $aliases = CodegenUtil::aliasify($php);

    $aliases_php = '';
    foreach ($aliases as $class => $alias) {
      if (TRUE === $alias) {
        $aliases_php .= 'use ' . $class . ";\n";
      }
      else {
        $aliases_php .= 'use ' . $class . ' as ' . $alias . ";\n";
      }
    }

    if ('' !== $aliases_php) {
      $aliases_php = "\n" . $aliases_php;
    }

    $php = <<<EOT
<?php
$aliases_php
class C {

  /**
   * @CfrPlugin("myPlugin", "My plugin")
   *
   * @return \\{$this->interface}
   */
  public static function create() {

    return $php;
  }
}
EOT;

    // @todo Is it a good idea to send full HTML to t()?
    // phpcs:disable
    $help = t(
      <<<EOT
<p>You can use the code below as a starting point for a custom plugin in a custom module.</p>
<p>If you do so, don't forget to:</p>
<ul>
  <li>Implement <code>hook_cfrplugin_info()</code> similar to how other modules do it.</li>
  <li>Set up a PSR-4 namespace directory structure for your class files.</li>
  <li>Replace "myPlugin", "My plugin" and "class C" with more suitable names, and put the class into a namespace.</li>
  <li>Leave the <code>@return</code> tag in place, because it tells cfrplugindiscovery about the plugin type.</li>
  <li>Fix all <code>@todo</code> items. These occur if code generation was incomplete.</li>
</ul>
EOT
    );
    // phpcs:enable

    $element = [];
    $element['help']['#markup'] = $help;
    $element['code']['#markup'] = UiUtil::highlightPhp($php);

    return $element;
  }

}
