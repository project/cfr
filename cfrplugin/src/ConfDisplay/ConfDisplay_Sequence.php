<?php

namespace Drupal\cfrplugin\ConfDisplay;

/**
 * Sequence of conf displays.
 */
class ConfDisplay_Sequence implements ConfDisplayInterface {

  /**
   * @var \Drupal\cfrplugin\ConfDisplay\ConfDisplayInterface[]
   */
  private $displays;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrplugin\ConfDisplay\ConfDisplayInterface[] $displays
   *   Sequence of displays, with sequential or associative keys.
   */
  public function __construct(array $displays) {
    $this->displays = $displays;
  }

  /**
   * {@inheritdoc}
   */
  public function buildWithConf($conf) {
    $elements = [];
    foreach ($this->displays as $key => $display) {
      $elements[$key] = $display->buildWithConf($conf);
    }
    return $elements;
  }

}
