<?php

namespace Drupal\cfrplugin\ConfDisplay;

use Drupal\cfrplugin\Util\UiUtil;

/**
 * Display that shows a dump of the configuration.
 */
class ConfDisplay_ConfDump implements ConfDisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function buildWithConf($conf) {

    if (!$conf) {
      return [];
    }

    return UiUtil::dumpData($conf);
  }

}
