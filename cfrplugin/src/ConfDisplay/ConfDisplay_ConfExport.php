<?php

namespace Drupal\cfrplugin\ConfDisplay;

use Donquixote\CallbackReflection\Util\CodegenUtil;
use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelper;
use Drupal\cfrplugin\Util\UiUtil;

/**
 * Display that shows a PHP export of the configuration.
 */
class ConfDisplay_ConfExport implements ConfDisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function buildWithConf($conf) {

    if (!$conf) {
      return [];
    }

    $helper = new CfrCodegenHelper();

    $php = $helper->export($conf);
    $php = CodegenUtil::autoIndent($php, '  ');

    $php = <<<EOT
<?php

return $php;
EOT;

    $markup = UiUtil::highlightPhp($php);

    return ['#markup' => $markup];
  }

}
