<?php

namespace Drupal\cfrplugin\ConfDisplay;

/**
 * Decorator that wraps the content in a fieldset with label.
 */
class ConfDisplay_Fieldset implements ConfDisplayInterface {

  /**
   * @var \Drupal\cfrplugin\ConfDisplay\ConfDisplayInterface
   */
  private $decorated;

  /**
   * @var string
   */
  private $label;

  /**
   * @var bool|null
   */
  private $collapsed;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrplugin\ConfDisplay\ConfDisplayInterface $decorated
   *   Decorated display.
   * @param string $label
   *   Fieldset label.
   * @param bool|null $collapsed
   *   TRUE for collapsed, FALSE for collapsible, NULL for non-collapsible.
   */
  public function __construct(ConfDisplayInterface $decorated, $label, $collapsed = NULL) {
    $this->decorated = $decorated;
    $this->label = $label;
    $this->collapsed = $collapsed;
  }

  /**
   * {@inheritdoc}
   */
  public function buildWithConf($conf) {
    $element = $this->decorated->buildWithConf($conf);
    if ([] === $element) {
      return [];
    }
    $fieldset = [
      '#type' => 'fieldset',
      '#title' => $this->label,
      'content' => $element,
    ];
    if ($this->collapsed) {
      $fieldset['#attributes']['class'] = ['collapsible', 'collapsed'];
      $fieldset['#attached']['library'][] = ['system', 'drupal.collapse'];
    }
    elseif (FALSE === $this->collapsed) {
      $fieldset['#attributes']['class'] = ['collapsible'];
      $fieldset['#attached']['library'][] = ['system', 'drupal.collapse'];
    }
    return $fieldset;
  }

}
