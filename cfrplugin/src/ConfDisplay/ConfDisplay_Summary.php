<?php

namespace Drupal\cfrplugin\ConfDisplay;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilder_Static;

/**
 * Conf display which shows a summary.
 */
class ConfDisplay_Summary implements ConfDisplayInterface {

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private $configurator;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator.
   */
  public function __construct(ConfiguratorInterface $configurator) {
    $this->configurator = $configurator;
  }

  /**
   * {@inheritdoc}
   */
  public function buildWithConf($non_empty_conf) {

    if (!$non_empty_conf) {
      return [];
    }

    $summary = $this->configurator->confGetSummary(
      $non_empty_conf,
      new SummaryBuilder_Static());

    return ['#markup' => $summary];
  }

}
