<?php

namespace Drupal\cfrplugin\DIC;

use Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfigurator_Mappers;
use Drupal\cfrplugin\TypeToConfigurator\TypeToConfigurator_CfrPlugin;
use Drupal\cfrplugin\Util\ServiceFactoryUtil;
use Drupal\cfrrealm\Container\CfrRealmContainerBase;
use Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndId_Cache;
use Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndId_HookDiscovery;
use Drupal\cfrrealm\DefinitionToConfigurator\DefinitionToConfigurator_Proxy;
use Drupal\cfrrealm\TypeToConfigurator\TypeToConfigurator_Buffer;
use Drupal\cfrrealm\TypeToConfigurator\TypeToConfigurator_ViaCfrFamily;
use Drupal\cfrrealm\TypeToDefinitionsbyid\TypeToDefinitionsbyid;
use Drupal\cfrrealm\TypeToDefmap\TypeToDefmap;
use Drupal\cfrreflection\CfrGen\CallbackToConfigurator\CallbackToConfigurator_ValueCallback;
use Drupal\cfrreflection\CfrGen\ParamToConfigurator\ParamToConfigurator;
use Drupal\cfrreflection\ParamToLabel\ParamToLabel;

/**
 * Default implementation of the plugin realm DI container.
 */
class CfrPluginRealmContainer extends CfrRealmContainerBase implements CfrPluginRealmContainerInterface {

  /**
   * @var string|null
   */
  private $cacheSuffix;

  /**
   * Static factory. Creates a container instance with cache enabled.
   *
   * @return self
   *   Created instance.
   */
  public static function createWithCache() {
    return new self($GLOBALS['language']->language);
  }

  /**
   * Static factory. Creates a container instance with cache disabled.
   *
   * @return self
   *   Created instance.
   */
  public static function createWithoutCache() {
    return new self(NULL);
  }

  /**
   * Constructor.
   *
   * @param string|null $cacheSuffix
   *   The langcode to append to the cache id, or NULL to have no cache.
   */
  public function __construct($cacheSuffix) {
    $this->cacheSuffix = $cacheSuffix;
  }

  /**
   * {@inheritdoc}
   */
  protected function get_typeToConfigurator() {
    $typeToConfigurator = new TypeToConfigurator_ViaCfrFamily($this->typeToCfrFamily);
    // Additional decorator layer, which is not in the parent implementation.
    $typeToConfigurator = new TypeToConfigurator_CfrPlugin($typeToConfigurator);
    return new TypeToConfigurator_Buffer($typeToConfigurator);
  }

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndIdInterface
   *   Nested list of plugin definitions by type and id.
   *
   * @see $definitionsByTypeAndId
   */
  protected function get_definitionsByTypeAndId() {
    $definitionsByTypeAndId = new DefinitionsByTypeAndId_HookDiscovery('cfrplugin_info');
    if (NULL !== $this->cacheSuffix) {
      $definitionsByTypeAndId = new DefinitionsByTypeAndId_Cache($definitionsByTypeAndId, 'cfrplugin:definitions-all:' . $this->cacheSuffix);
    }
    return $definitionsByTypeAndId;
  }

  /**
   * {@inheritdoc}
   */
  protected function get_typeToDefmap() {
    $typeToDefinitionsById = new TypeToDefinitionsbyid($this->definitionsByTypeAndId);
    return new TypeToDefmap($typeToDefinitionsById, 'cfrplugin:definitions:' . $this->cacheSuffix);
  }

  /**
   * {@inheritdoc}
   */
  protected function get_definitionToConfigurator() {
    // Use a stub to allow circular dependency.
    return new DefinitionToConfigurator_Proxy(
      function () {
        $definitionToConfigurator = new DefinitionToConfigurator_Mappers();
        // $this can be used since PHP 5.4.
        $mappers = ServiceFactoryUtil::createDeftocfrMappers($this->callbackToConfigurator);
        foreach ($mappers as $key => $mapper) {
          $definitionToConfigurator->keySetMapper($key, $mapper);
        }
        return $definitionToConfigurator;
      });
  }

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrreflection\CfrGen\CallbackToConfigurator\CallbackToConfiguratorInterface
   *   Object to create a configurator from an callback specified for a plugin.
   *
   * @see $callbackToConfigurator
   */
  protected function get_callbackToConfigurator() {
    return new CallbackToConfigurator_ValueCallback($this->paramToConfigurator, $this->paramToLabel);
  }

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrreflection\CfrGen\ParamToConfigurator\ParamToConfiguratorInterface
   *   Object to magically get a configurator for a reflection parameter.
   *
   * @see $paramToConfigurator
   */
  protected function get_paramToConfigurator() {
    return new ParamToConfigurator($this->typeToConfigurator);
  }

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrreflection\ParamToLabel\ParamToLabelInterface
   *   Object to get a label from a reflection parameter.
   *
   * @see $paramToLabel
   */
  protected function get_paramToLabel() {
    return new ParamToLabel();
  }

}
