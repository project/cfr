<?php

namespace Drupal\cfrplugin\DIC;

use Drupal\cfrrealm\Container\CfrRealmContainerInterface;

/**
 * DI container to get objects related to a "plugin realm".
 *
 * A "realm" is a collection of types or interfaces, where for each type there
 * is a family of plugins that can be presented as a drilldown.
 *
 * @property \Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndIdInterface $definitionsByTypeAndId
 *   Nested list of plugin definitions by type and id.
 *
 * Part of the cycle using reflection:
 * @property \Drupal\cfrreflection\CfrGen\CallbackToConfigurator\CallbackToConfiguratorInterface $callbackToConfigurator
 *   Object to create a configurator from an callback specified for a plugin.
 * @property \Drupal\cfrreflection\CfrGen\ParamToConfigurator\ParamToConfiguratorInterface $paramToConfigurator
 *   Object to magically get a configurator for a reflection parameter.
 * @property \Drupal\cfrreflection\ParamToLabel\ParamToLabelInterface $paramToLabel
 *   Object to get a label from a reflection parameter.
 */
interface CfrPluginRealmContainerInterface extends CfrRealmContainerInterface {}
