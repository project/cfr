<?php

namespace Drupal\cfrplugin\UrlTokenPolicy;

/**
 * Object to write and verify url signature tokens.
 */
interface UrlTokenPolicyInterface {

  /**
   * Checks if a url query is properly signed.
   *
   * @param array $query
   *   Query parameters, e.g. from $_GET, including 'q' for the path.
   *   The order of parameters does not matter.
   *
   * @return bool
   *   TRUE, if the query contains the expected token.
   */
  public function queryIsSigned(array $query);

  /**
   * Signs a url query with a generated generated token.
   *
   * This may be called when building a url for a link or a redirect.
   *
   * Behavior expected of ALL implementations:
   * - The key 'q', if present, will be ignored.
   * - An existing token will be ignored and then overwritten.
   * - Only the token itself is modified, all other values remain unchanged.
   *
   * @param string $path
   *   The path for the url.
   * @param array $query
   *   Query parameters to be modified.
   */
  public function pathQuerySetToken($path, array &$query);

}
