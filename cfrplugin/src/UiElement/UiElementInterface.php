<?php

namespace Drupal\cfrplugin\UiElement;

/**
 * Object that can produce a render element for use in a UI page.
 *
 * A similar interface exists in renderkit, but cfr:* is not meant to depend on
 * renderkit.
 *
 * @see \Drupal\renderkit\BuildProvider\BuildProviderInterface
 */
interface UiElementInterface {

  /**
   * Builds a render element for the UI element.
   *
   * @return array
   *   Render element.
   */
  public function build();

}
