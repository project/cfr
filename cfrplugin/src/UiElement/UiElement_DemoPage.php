<?php

namespace Drupal\cfrplugin\UiElement;

use Drupal\cfrplugin\ConfDisplay\ConfDisplayInterface;
use Drupal\cfrplugin\UrlTokenPolicy\UrlTokenPolicy_Common;
use Drupal\cfrplugin\Util\DataUtil;
use Drupal\themekit\GenericForm;
use Drupal\themekit\T;

/**
 * UI element for the demo page.
 */
class UiElement_DemoPage implements UiElementInterface {

  /**
   * @var string
   */
  private $interface;

  /**
   * @var \Drupal\cfrplugin\ConfDisplay\ConfDisplayInterface
   */
  private $confDisplay;

  /**
   * Constructor.
   *
   * @param string $interface
   *   Interface name.
   * @param \Drupal\cfrplugin\ConfDisplay\ConfDisplayInterface $display
   *   Objects to display / preview the configuration.
   */
  public function __construct($interface, ConfDisplayInterface $display) {
    $this->interface = $interface;
    $this->confDisplay = $display;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $element = [];

    if (isset($_GET['json'])) {
      $conf = DataUtil::jsonDecodeSafe($_GET['json'], $json_success);
      if (!$json_success) {
        $element['sorry_json'] = [
          '#theme' => T::th('theme_themekit_status_messages'),
          '#messages' => [
            'warning' => [
              t('The json in the url could not be parsed.'),
            ],
          ],
        ];
      }
    }
    else {
      $conf = NULL;
    }

    $element['form'] = GenericForm::build(
      $this->confBuildForm($conf),
      self::class);

    if ($conf === NULL) {
      return $element;
    }

    if (!static::getTokenPolicy()->queryIsSigned($_GET)) {
      // End here.
      $element['sorry']['#markup'] = '<div>'
        . t('The url is not signed with a valid token. Probably it was copied from elsewhere.')
        . '<br/>'
        . t('Please review and submit again.')
        . '</div>';
      return $element;
    }

    $element['preview'] = $this->confDisplay->buildWithConf($conf);

    return $element;
  }

  /**
   * Builds the form.
   *
   * This can be overridden in a subclass.
   *
   * @param mixed $conf
   *   Configuration from url.
   *
   * @return array
   *   Render element representing a form.
   */
  protected function confBuildForm($conf) {

    $form = [];

    $form['conf'] = [
      '#type' => 'cfrplugin',
      '#cfrplugin_interface' => $this->interface,
      '#title' => t('Plugin'),
      '#default_value' => $conf,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Show'),
    ];

    $form['#submit'][] = T::c([static::class, 'submit']);

    return $form;
  }

  /**
   * Submit handler. Sets a redirect url.
   *
   * This is static, to avoid serializing the object with the callback.
   *
   * @param array $form
   *   Complete form.
   * @param array $form_state
   *   Form state.
   */
  public static function submit(array $form, array &$form_state) {
    // The $form_state['redirect'] might already contain query parameters from a
    // child implementation of this method.
    $form_state['redirect'][0] = $_GET['q'];
    $form_state['redirect'][1]['query']['json'] = json_encode($form_state['values']['conf']);
    static::getTokenPolicy()->pathQuerySetToken(
      $form_state['redirect'][0],
      $form_state['redirect'][1]['query']);
  }

  /**
   * Gets a token policy object to sign url queries with a token.
   *
   * This is static, so it can be called from the static submit handler.
   * It is public to allow external code to use the token policy.
   *
   * @return \Drupal\cfrplugin\UrlTokenPolicy\UrlTokenPolicyInterface
   *   Token policy object.
   */
  public static function getTokenPolicy() {
    return new UrlTokenPolicy_Common(
      'token',
      ['json']);
  }

}
