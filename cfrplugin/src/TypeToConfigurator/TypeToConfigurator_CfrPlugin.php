<?php

namespace Drupal\cfrplugin\TypeToConfigurator;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\Configurator\Composite\Configurator_IdConfGrandBase;
use Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface;

/**
 * Decorator to add the "drilldown tools" to the created configurator.
 */
class TypeToConfigurator_CfrPlugin implements TypeToConfiguratorInterface {

  /**
   * @var \Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface
   */
  private $decorated;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface $decorated
   *   Decorated TypeToConfigurator* object.
   */
  public function __construct(TypeToConfiguratorInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function typeGetConfigurator($type, CfrContextInterface $context = NULL) {
    $configurator = $this->decorated->typeGetConfigurator($type, $context);
    return $this->processConfigurator($configurator, $type, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function typeGetOptionalConfigurator($type, CfrContextInterface $context = NULL, $defaultValue = NULL) {
    $configurator = $this->decorated->typeGetOptionalConfigurator($type, $context, $defaultValue);
    /** @var \Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface $configurator */
    $configurator = $this->processConfigurator($configurator, $type, $context);
    return $configurator;
  }

  /**
   * Modifies the returned configurator.
   *
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Original configurator returned from decorated TypeToConfigurator* object.
   * @param string $type
   *   Plugin type / interface name.
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   Context to restrict available options.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Processed configurator.
   */
  private function processConfigurator(ConfiguratorInterface $configurator, $type, CfrContextInterface $context = NULL) {

    if ($configurator instanceof Configurator_IdConfGrandBase) {
      $configurator = $configurator->withFormProcessCallback(static function ($element) use ($type, $context) {
        /* @see cfrplugin_element_info() */
        $element['#type'] = 'cfrplugin_drilldown_container';
        $element['#cfrplugin_interface'] = $type;
        $element['#cfrplugin_context'] = $context;
        return $element;
      });
    }

    return $configurator;
  }

}
