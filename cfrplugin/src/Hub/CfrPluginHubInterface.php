<?php

namespace Drupal\cfrplugin\Hub;

use Drupal\cfrreflection\CfrGen\InterfaceToConfigurator\InterfaceToConfiguratorInterface;

/**
 * Object used as a "global" access point to cfrplugin functionality.
 *
 * Inherited methods:
 * - InterfaceToConfiguratorInterface::*()
 *   Gets a configurator to configure instances of the given interface.
 *
 * @see \cfrplugin()
 */
interface CfrPluginHubInterface extends InterfaceToConfiguratorInterface {

  /**
   * Gets labels for the interfaces.
   *
   * @return string[]
   *   Format: $[$interface] = $label
   */
  public function getInterfaceLabels();

  /**
   * Gets the drilldown legend for the interface.
   *
   * @param string $interface
   *   The interface name.
   *
   * @return \Drupal\cfrfamily\CfrLegend\CfrLegendInterface
   *   The drilldown legend for the interface.
   *
   * @todo This is not used in a relevant way. Consider to remove, see #3165296.
   */
  public function interfaceGetCfrLegendOrNull($interface);

}
