<?php

namespace Drupal\cfrplugin\Hub;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrapi\Util\SingletonUtil;
use Drupal\cfrfamily\CfrLegendProvider\CfrLegendProviderInterface;
use Drupal\cfrplugin\DIC\CfrPluginRealmContainer;
use Drupal\cfrplugin\DIC\CfrPluginRealmContainerInterface;
use Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndIdInterface;
use Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface;
use Drupal\cfrreflection\Util\StringUtil;

/**
 * Default implementation of the "hub".
 */
class CfrPluginHub implements CfrPluginHubInterface {

  /**
   * @var \Drupal\cfrplugin\DIC\CfrPluginRealmContainer|null
   */
  private static $container;

  /**
   * @var \Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface
   */
  private $interfaceToConfigurator;

  /**
   * @var \Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndIdInterface
   */
  private $definitionsByTypeAndId;

  /**
   * Resets singleton container instance.
   */
  public static function clearContainerInstance() {
    self::$container = NULL;
  }

  /**
   * Gets the singleton container instance.
   *
   * @return \Drupal\cfrplugin\DIC\CfrPluginRealmContainer
   *   Singleton container instance.
   */
  public static function getContainer() {
    return self::$container
      ?: SingletonUtil::init(
        __METHOD__,
        self::$container,
        self::$container = CfrPluginRealmContainer::createWithCache());
  }

  /**
   * Static factory, using the singleton for the container.
   *
   * @return \Drupal\cfrplugin\Hub\CfrPluginHubInterface
   *   Created instance.
   */
  public static function create() {
    return self::createFromContainer(self::getContainer());
  }

  /**
   * Static factory, from a passed-in container.
   *
   * @param \Drupal\cfrplugin\DIC\CfrPluginRealmContainerInterface $container
   *   Container instance.
   *
   * @return \Drupal\cfrplugin\Hub\CfrPluginHubInterface
   *   Created instance.
   */
  public static function createFromContainer(CfrPluginRealmContainerInterface $container) {
    return new self(
      $container->typeToConfigurator,
      $container->definitionsByTypeAndId);
  }

  /**
   * Constructor.
   *
   * @param \Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface $interfaceToConfigurator
   *   Object to get a configurator for an interface.
   * @param \Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndIdInterface $definitionsByTypeAndId
   *   Object to get plugin definitions for all types.
   */
  public function __construct(
    TypeToConfiguratorInterface $interfaceToConfigurator,
    DefinitionsByTypeAndIdInterface $definitionsByTypeAndId
  ) {
    $this->interfaceToConfigurator = $interfaceToConfigurator;
    $this->definitionsByTypeAndId = $definitionsByTypeAndId;
  }

  /**
   * {@inheritdoc}
   */
  public function getInterfaceLabels() {

    $labels = [];
    foreach ($this->definitionsByTypeAndId->getDefinitionsByTypeAndId() as $interface => $definitions) {
      $labels[$interface] = StringUtil::interfaceGenerateLabel($interface);
    }

    return $labels;
  }

  /**
   * {@inheritdoc}
   */
  public function interfaceGetConfigurator($interface, CfrContextInterface $context = NULL) {
    return $this->interfaceToConfigurator->typeGetConfigurator($interface, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function interfaceGetOptionalConfigurator($interface, CfrContextInterface $context = NULL, $defaultValue = NULL) {
    return $this->interfaceToConfigurator->typeGetOptionalConfigurator($interface, $context, $defaultValue);
  }

  /**
   * {@inheritdoc}
   */
  public function interfaceGetCfrLegendOrNull($interface) {
    $configurator = $this->interfaceToConfigurator->typeGetConfigurator($interface);
    if (!$configurator instanceof CfrLegendProviderInterface) {
      return NULL;
    }
    return $configurator->getCfrLegend();
  }

}
