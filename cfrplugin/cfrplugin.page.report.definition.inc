<?php

use Drupal\cfrplugin\Hub\CfrPluginHub;
use Drupal\cfrplugin\Util\DefinitionsUiUtil;
use Drupal\cfrplugin\Util\UiUtil;
use Drupal\cfrreflection\Util\StringUtil;
use Drupal\themekit\T;

/**
 * Page callback for a definition page.
 *
 * @param array $df
 *   Array with interface, plugin id, and plugin definition.
 *
 * @return array
 *   Page content render element.
 */
function _cfrplugin_report_definition_page(array $df) {

  $key = $df['id'];
  $interface = $df['interface'];
  $definition = $df['definition'];

  $services = CfrPluginHub::getContainer();
  $definitionToLabel = $services->definitionToLabel;
  $definitionToGroupLabel = $services->definitionToGrouplabel;
  $definitionToConfigurator = $services->definitionToConfigurator;

  $rows = [];

  $rows[] = [
    t('Interface'),
    StringUtil::interfaceGenerateLabel($interface)
      . '<br/>'
      . '<code>' . check_plain($interface) . '</code>'
      . '<br/>'
    . l(t('List of plugins'), 'admin/reports/cfrplugin/' . _cfrplugin_interface_slug($interface))
    . ' | '
    . l(t('Interface declaration'), 'admin/reports/cfrplugin/' . _cfrplugin_interface_slug($interface) . '/code'),
  ];

  $rows[] = [
    t('Label'),
    '<h3>' . $definitionToLabel->definitionGetLabel($definition, $key) . '</h3>',
  ];

  $rows[] = [
    t('Plugin id'),
    '<pre>' . check_plain($key) . '</pre>',
  ];

  if (NULL !== $groupLabel = $definitionToGroupLabel->definitionGetLabel($definition, NULL)) {
    $rows[] = [
      t('Group label'),
      $groupLabel,
    ];
  }

  $rows[] = [
    t('Definition'),
    '<pre>' . UiUtil::exportData($definition) . '</pre>',
  ];

  try {
    $configurator = $definitionToConfigurator->definitionGetConfigurator($definition);
    if ($configurator === NULL) {
      $rows[] = [
        t('Status'),
        t('inactive'),
      ];
    }
    else {
      $element = UiUtil::dumpData($configurator);
      $rows[] = [
        t('Configurator'),
        drupal_render($element),
      ];
    }
  }
  catch (\Exception $e) {
    $element = UiUtil::displayException($e);
    $rows[] = [
      t('Exception'),
      drupal_render($element),
    ];
  }

  $content = [];

  $content['messages'] = DefinitionsUiUtil::definitionBuildMessages(
    $definition);

  $content['table'] = [
    '#theme' => T::th('theme_table'),
    '#rows' => $rows,
  ];

  return $content;
}
