<?php

use Drupal\cfrplugin\Hub\CfrPluginHub;
use Drupal\cfrplugin\Util\DefinitionsUiUtil;
use Drupal\cfrreflection\Util\StringUtil;
use Drupal\themekit\T;

/**
 * Page callback for the overview page at 'admin/reports/cfrplugin'.
 *
 * @return array
 *   Page content render element.
 */
function _cfrplugin_report_overview_page() {

  $services = CfrPluginHub::getContainer();
  $definitions = $services->definitionsByTypeAndId->getDefinitionsByTypeAndId();
  $definitionToConfigurator = $services->definitionToConfigurator;

  $rows = [];
  $rows_grouped = [];
  $n_broken_total = 0;
  $n_inactive_total = 0;
  foreach ($definitions as $interface => $interface_definitions) {

    $label = StringUtil::interfaceGenerateLabel($interface);

    $n_definitions = count($interface_definitions);

    // Count broken plugins.
    $n_broken = 0;
    $n_inactive = 0;
    foreach ($interface_definitions as $definition) {
      try {
        $configurator = $definitionToConfigurator->definitionGetConfigurator($definition);
        if ($configurator === NULL) {
          ++$n_inactive;
        }
      }
      catch (\Exception $e) {
        ++$n_broken;
      }
    }

    $n_inactive_total += $n_inactive;
    $n_broken_total += $n_broken;

    $n_active = $n_definitions - $n_broken - $n_inactive;

    $count = t('@n plugin definitions', ['@n' => $n_definitions]);

    if ($n_broken || $n_inactive) {
      $parts = [];
      if ($n_broken) {
        $parts[] = '<strong>'
          . t('@n broken', ['@n' => $n_broken])
          . '</strong>';
      }
      if ($n_inactive) {
        $parts[] = t('@n inactive', ['@n' => $n_inactive]);
      }
      $parts[] = t('@n active', ['@n' => $n_active]);
      $count .= '<br/>'
        . implode(', ', $parts);
    }

    $interface_shortname = StringUtil::classGetShortname($interface);
    $interface_basename = preg_replace('@Interface$@', '', $interface_shortname);
    $path = 'admin/reports/cfrplugin/' . _cfrplugin_interface_slug($interface);

    $row = [];
    $row[] = $label;
    // Link to a list of plugins for the interface.
    $row[] = l($count, $path, ['html' => TRUE]);
    // Link to a demo form.
    $row[] = l(t('Demo: @name', ['@name' => $interface_basename . '*']), $path . '/demo');
    // Link to a page showing the interface declaration.
    $row[] = l('<code>' . $interface . '</code>', $path . '/code', ['html' => TRUE]);

    $fragments = explode('\\', $interface);
    if (1
      && 'Drupal' === $fragments[0]
      && isset($fragments[2])
      && module_exists($fragments[1])
    ) {
      $rows_grouped[$fragments[1]][] = $row;
    }
    else {
      $rows[] = $row;
    }
  }

  $modules_info = system_get_info('module_enabled');

  foreach ($rows_grouped as $module => $module_rows) {

    $module_label = isset($modules_info[$module])
      ? $modules_info[$module]['name']
      : $module;

    $rows[] = [
      [
        'colspan' => 4,
        'data' => '<h3>' . $module_label . '</h3>',
      ],
    ];

    foreach ($module_rows as $row) {
      $rows[] = $row;
    }
  }

  $content = [];

  $content['messages'] = DefinitionsUiUtil::buildDefinitionsPageMessages(
    $n_inactive_total,
    $n_broken_total);

  $content['table'] = [
    '#header' => [
      t('Human name'),
      t('List of plugins'),
      t('Demo'),
      t('Interface declaration'),
    ],
    '#theme' => T::th('theme_table'),
    '#rows' => $rows,
  ];

  return $content;
}
