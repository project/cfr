<?php

use Drupal\cfrplugin\Hub\CfrPluginHub;
use Drupal\cfrplugin\Util\DefinitionsUiUtil;
use Drupal\cfrplugin\Util\UiUtil;
use Drupal\themekit\T;

/**
 * Page callback. Shows a list of plugins for the interface.
 *
 * @param string $interface
 *   The interface.
 *
 * @return array
 *   Page content render element.
 */
function _cfrplugin_report_interface_page($interface) {

  $services = CfrPluginHub::getContainer();
  $definitionToLabel = $services->definitionToLabel;
  $definitionToGroupLabel = $services->definitionToGrouplabel;
  $definitionToConfigurator = $services->definitionToConfigurator;

  $definitionMap = $services->typeToDefmap->typeGetDefmap($interface);

  $rows = [];
  $rows_grouped = [];
  $n_inactive = 0;
  $n_broken = 0;
  foreach ($definitionMap->getDefinitionsById() as $key => $definition) {

    $row = [
      l(
        $definitionToLabel->definitionGetLabel($definition, $key),
        'admin/reports/cfrplugin/' . _cfrplugin_interface_slug($interface) . '/plugin/' . $key),
      '<code>' . check_plain($key) . '</code>',
      UiUtil::exportData($definition),
    ];

    try {
      $configurator = $definitionToConfigurator->definitionGetConfigurator($definition);
      if ($configurator === NULL) {
        $row[0] .= ' (' . t('inactive') . ')';
        ++$n_inactive;
      }
    }
    catch (\Exception $e) {
      $row[0] .= ' (' . t('broken') . ')';
      ++$n_broken;
    }

    if (NULL !== $groupLabelOrNull = $definitionToGroupLabel->definitionGetLabel($definition, NULL)) {
      $rows_grouped[$groupLabelOrNull][] = $row;
    }
    else {
      // The plugin has no "group label". This does usually not happen.
      // Show at the top, before the grouped rows, for completeness.
      $rows[] = $row;
    }
  }

  foreach ($rows_grouped as $groupLabel => $rowsInGroup) {
    $rows[] = [
      [
        'colspan' => 3,
        'data' => '<h3>' . check_plain($groupLabel) . '</h3>',
      ],
    ];
    foreach ($rowsInGroup as $row) {
      $rows[] = $row;
    }
  }

  $content = [];

  $content['messages'] = DefinitionsUiUtil::buildDefinitionsPageMessages(
    $n_inactive,
    $n_broken);

  $content['table'] = [
    '#theme' => T::th('theme_table'),
    '#rows' => $rows,
    '#header' => [
      t('Plugin'),
      t('Machine name'),
      t('Plugin definition'),
    ],
  ];

  return $content;
}
