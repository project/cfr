<?php

use Drupal\cfrplugin\ConfDisplay\ConfDisplay_Codegen;
use Drupal\cfrplugin\ConfDisplay\ConfDisplay_ConfDump;
use Drupal\cfrplugin\ConfDisplay\ConfDisplay_ConfExport;
use Drupal\cfrplugin\ConfDisplay\ConfDisplay_Fieldset;
use Drupal\cfrplugin\ConfDisplay\ConfDisplay_InstanceFieldset;
use Drupal\cfrplugin\ConfDisplay\ConfDisplay_Sequence;
use Drupal\cfrplugin\ConfDisplay\ConfDisplay_Summary;
use Drupal\cfrplugin\UiElement\UiElement_DemoPage;

/**
 * Page callback. Shows a demo form for an interface.
 *
 * @param string $interface
 *   Interface from url, passed through _cfrplugin_interface_load().
 *
 * @return array
 *   Render element.
 */
function _cfrplugin_report_interface_demo_page($interface) {
  if (isset($_GET['plugin']) && !isset($_GET['json'])) {
    // Support legacy urls that did not use json. See #3165518.
    $query = $_GET;
    $query['json'] = json_encode($query['plugin']);
    unset($query['plugin']);
    unset($query['q']);
    drupal_goto($_GET['q'], ['query' => $query]);
  }
  $configurator = cfrplugin()->interfaceGetConfigurator($interface);
  $display = new ConfDisplay_Sequence([
    'summary' => new ConfDisplay_Fieldset(
      new ConfDisplay_Summary($configurator),
      t('Summary'),
      TRUE),
    'conf' => new ConfDisplay_Fieldset(
      new ConfDisplay_ConfDump(),
      t('Configuration data - dump'),
      TRUE),
    'conf_export' => new ConfDisplay_Fieldset(
      new ConfDisplay_ConfExport(),
      t('Configuration data - export'),
      TRUE),
    'object' => new ConfDisplay_InstanceFieldset($interface, $configurator, TRUE),
    'codegen' => new ConfDisplay_Fieldset(
      new ConfDisplay_Codegen($interface, $configurator),
      t('Generated factory code')),
  ]);
  $ui_element = new UiElement_DemoPage($interface, $display);
  return $ui_element->build();
}
