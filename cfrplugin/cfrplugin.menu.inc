<?php

/**
 * @file
 * Contains hook_menu() and menu-related callbacks.
 */

use Drupal\cfrplugin\Hub\CfrPluginHub;
use Drupal\cfrplugin\Util\UiUtil;
use Drupal\cfrreflection\Util\StringUtil;
use Drupal\themekit\T;

/**
 * Implements hook_menu().
 */
function cfrplugin_menu() {
  $items = [];

  $items[$base = 'admin/reports/cfrplugin'] = [
    'type' => MENU_NORMAL_ITEM,
    /* @see cfrplugin_permission() */
    'access arguments' => [$perm = 'view cfrplugin report'],
    'title' => 'Cfr plugins',
    'page callback' => T::c('_cfrplugin_report_overview_page'),
    'file' => 'cfrplugin.page.report.inc',
  ];

  $interface_base = $base . '/' . T::w('_cfrplugin_interface_load');

  $items[$interface_base] = [
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => [$perm],
    'title callback' => T::c('_cfrplugin_report_interface_page_title'),
    'title arguments' => [3],
    'page callback' => T::c('_cfrplugin_report_interface_page'),
    'page arguments' => [3],
    'file' => 'cfrplugin.page.report.interface.inc',
  ];

  $items[$interface_base . '/list'] = [
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => 'List of plugins',
    'weight' => -10,
  ];

  $items[$interface_base . '/code'] = [
    'type' => MENU_LOCAL_TASK,
    'access arguments' => [$perm],
    'title' => 'Interface declaration',
    'page callback' => T::c('_cfrplugin_report_interface_code_page'),
    'page arguments' => [3],
    'file' => 'cfrplugin.page.report.interface.code.inc',
  ];

  $items[$interface_base . '/demo'] = [
    'type' => MENU_LOCAL_TASK,
    'access arguments' => [$perm],
    'title' => 'Demo',
    'page callback' => T::c('_cfrplugin_report_interface_demo_page'),
    'page arguments' => [3],
    'file' => 'cfrplugin.page.report.interface.demo.inc',
  ];

  $plugin_base = $interface_base . '/plugin/' . T::w('_cfrplugin_definition_load');

  $items[$plugin_base] = [
    'type' => MENU_NORMAL_ITEM,
    'load arguments' => [3],
    'access arguments' => [$perm],
    'title callback' => T::c('_cfrplugin_definition_page_title'),
    'title arguments' => [5],
    'page callback' => T::c('_cfrplugin_report_definition_page'),
    'page arguments' => [5],
    'file' => 'cfrplugin.page.report.definition.inc',
  ];

  $items[$plugin_base . '/definition'] = [
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'load arguments' => [3],
    'title' => 'Plugin definition',
    'weight' => -10,
  ];

  $items[$plugin_base . '/code'] = [
    'type' => MENU_LOCAL_TASK,
    'load arguments' => [3],
    'access arguments' => [$perm],
    'title' => 'Declaration in code',
    'page callback' => T::c('_cfrplugin_report_definition_code_page'),
    'page arguments' => [5],
    'file' => 'cfrplugin.page.report.definition.code.inc',
  ];

  return $items;
}

/**
 * Menu wildcard loader for '%_cfrplugin_interface'.
 *
 * This is the reverse of _cfrplugin_interface_slug().
 *
 * The loader does not check whether an interface actually exists, it only
 * checks the format.
 *
 * @param string $arg
 *   Path fragment from url.
 *   An acceptable value is an interface name where namespace separators are
 *   replaced with '.'.
 *
 * @return string|false
 *   An interface name, or FALSE if the value is not valid.
 *
 * @see _cfrplugin_interface_slug()
 */
function _cfrplugin_interface_load($arg) {
  $interface = str_replace('.', '\\', $arg);
  if (!UiUtil::interfaceNameIsValid($interface)) {
    return FALSE;
  }
  // At this point, $interface looks like a valid class name. But it could still
  // be a non-existing interface, and possibly something ridiculously long.
  // Avoid interface_exists(), because autoload can have side effects.
  return $interface;
}

/**
 * Produces a url fragment from an interface.
 *
 * This is the reverse of _cfrplugin_interface_load().
 *
 * @param string $interface
 *   The interface name.
 *
 * @return string
 *   Url fragment, obtained by replacing namespace separators with dots.
 *
 * @see \_cfrplugin_interface_load()
 */
function _cfrplugin_interface_slug($interface) {
  return str_replace('\\', '.', $interface);
}

/**
 * Page title callback for an interface page.
 *
 * @param string $interface
 *   Interface name from _cfrplugin_interface_load().
 *   It is not guaranteed whether the interface exists.
 *
 * @return string
 *   A page title for the routing system.
 */
function _cfrplugin_report_interface_page_title($interface) {
  return StringUtil::interfaceGenerateLabel($interface);
}

/**
 * Menu wildcard loader for '%_cfrplugin_definition'.
 *
 * Loads a plugin definition by interface name and plugin name.
 *
 * @param string $arg
 *   Url fragment for the plugin name.
 * @param string $interface_arg
 *   Url fragment for the interface name.
 *
 * @return array|false
 *   Array with interface name, plugin id, and plugin definition, or FALSE if
 *   not found.
 */
function _cfrplugin_definition_load($arg, $interface_arg) {

  if (FALSE === $interface = _cfrplugin_interface_load($interface_arg)) {
    return FALSE;
  }

  if (NULL === $definition = CfrPluginHub::getContainer()->typeToDefmap->typeGetDefmap($interface)->idGetDefinition($arg)) {
    return FALSE;
  }

  return [
    'interface' => $interface,
    'id' => $arg,
    'definition' => $definition,
  ];
}

/**
 * Page title callback for a plugin definition page.
 *
 * @param array $df
 *   Array with plugin definition, interface name and plugin id, as returned
 *   from _cfrplugin_definition_load().
 *
 * @return string
 *   A page title for the routing system.
 */
function _cfrplugin_definition_page_title(array $df) {

  if (isset($df['definition']['label'])) {
    return $df['definition']['label'];
  }

  return $df['id'];
}
