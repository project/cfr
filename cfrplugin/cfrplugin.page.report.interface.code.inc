<?php

use Drupal\cfrplugin\Util\UiUtil;

/**
 * Page callback for the interface code page.
 *
 * This page shows the code of the PHP file where the interface is declared.
 *
 * @param string $interface
 *   Interface name.
 *
 * @return string
 *   Page content html string.
 */
function _cfrplugin_report_interface_code_page($interface) {

  $html = '';

  if (!interface_exists($interface) && !class_exists($interface)) {
    $html .= t('There is no class or interface by this name.');
  }
  elseif (NULL === $php = UiUtil::classGetPhp($interface)) {
    $html .= t('The class has no doc comment.');
  }
  else {
    $html .= UiUtil::highlightPhp($php);
  }

  return $html;
}
