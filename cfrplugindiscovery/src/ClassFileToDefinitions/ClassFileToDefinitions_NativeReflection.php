<?php

namespace Drupal\cfrplugindiscovery\ClassFileToDefinitions;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrplugindiscovery\DocToAnnotations\DocToAnnotations;
use Drupal\cfrplugindiscovery\DocToAnnotations\DocToAnnotationsInterface;
use Drupal\cfrplugindiscovery\Util\DefinitionUtil;
use Drupal\cfrplugindiscovery\Util\DocUtil;

/**
 * Implementation that analyses classes and methods using native reflection.
 *
 * An alternative would be to use a parser-based solution, to avoid loading the
 * classes, at the cost of some performance.
 *
 * The native reflection has turned out to be sufficient for cfr, because the
 * discovery is opt-in. Modules that implement hook_cfrplugin_info() can decide
 * to call cfrplugindiscovery() or do something else.
 */
class ClassFileToDefinitions_NativeReflection implements ClassFileToDefinitionsInterface {

  /**
   * @var \Drupal\cfrplugindiscovery\DocToAnnotations\DocToAnnotationsInterface
   */
  private $docToAnnotations;

  /**
   * @var string
   */
  private $tagName;

  /**
   * Static factory with some defaults.
   *
   * @param string $tagName
   *   Annotation tag name, e.g. 'CfrPlugin'.
   *
   * @return self
   *   Created instance.
   */
  public static function create($tagName) {
    return new self(
      DocToAnnotations::create($tagName),
      $tagName);
  }

  /**
   * Constructor.
   *
   * @param \Drupal\cfrplugindiscovery\DocToAnnotations\DocToAnnotationsInterface $docToAnnotations
   *   Object to read annotations from doc comments.
   * @param string $tagName
   *   Annotation tag name, e.g. 'CfrPlugin'.
   */
  public function __construct(DocToAnnotationsInterface $docToAnnotations, $tagName) {
    $this->docToAnnotations = $docToAnnotations;
    $this->tagName = $tagName;
  }

  /**
   * {@inheritdoc}
   */
  public function classFileGetDefinitions($class, $file) {

    if (FALSE === $php = file_get_contents($file)) {
      return [];
    }

    if (FALSE === strpos($php, '@' . $this->tagName)) {
      return [];
    }

    try {
      $reflectionClass = new \ReflectionClass($class);
    }
    catch (\ReflectionException $e) {
      // @todo Log this?
      return [];
    }

    if ($reflectionClass->isInterface() || $reflectionClass->isTrait()) {
      return [];
    }

    // Cause an error if the class is defined elsewhere.
    require_once $file;

    return $this->reflectionClassGetDefinitions($reflectionClass);
  }

  /**
   * Reads plugin definitions from a class.
   *
   * @param \ReflectionClass $reflectionClass
   *   The class.
   *
   * @return array[][]
   *   Format: $[$pluginType][$pluginId] = $pluginDefinition.
   */
  private function reflectionClassGetDefinitions(\ReflectionClass $reflectionClass) {

    $definitionsByTypeAndId = [];

    if (!$reflectionClass->isAbstract()) {
      $definitionsByTypeAndId = $this->reflectionClassGetDefinitionsForClass($reflectionClass);
    }

    foreach ($reflectionClass->getMethods() as $methodReflection) {

      if (0
        || !$methodReflection->isStatic()
        || $methodReflection->isAbstract()
        || $methodReflection->isConstructor()
        // Ignore methods declared in a parent class.
        || $methodReflection->getDeclaringClass()->getName() !== $reflectionClass->getName()
      ) {
        continue;
      }

      foreach ($this->staticMethodGetDefinitions($methodReflection) as $type => $definitions) {
        foreach ($definitions as $id => $definition) {
          $definitionsByTypeAndId[$type][$id] = $definition;
        }
      }
    }

    return $definitionsByTypeAndId;
  }

  /**
   * Reads class-level plugin definitions from the class doc.
   *
   * These plugins use the class constructor as their factory.
   *
   * @param \ReflectionClass $reflectionClass
   *   The class.
   *
   * @return array[][]
   *   Format: $[$pluginType][$pluginId] = $pluginDefinition
   */
  private function reflectionClassGetDefinitionsForClass(\ReflectionClass $reflectionClass) {

    if (FALSE === $docComment = $reflectionClass->getDocComment()) {
      return [];
    }

    try {
      $annotations = $this->docToAnnotations->docGetAnnotations($docComment);
    }
    catch (\Exception $e) {
      // Annotations have unexpected format.
      $this->reportFailingDeclaration(
        'Unexpected annotation format in class doc on @class. Annotations will be ignored.',
        ['@class' => $reflectionClass->getName()]);

      return [];
    }

    if ([] === $annotations) {
      return [];
    }

    if ($reflectionClass->implementsInterface(ConfiguratorInterface::class)) {
      $stubDefinition = ['configurator_class' => $reflectionClass->getName()];

      try {
        $confGetValueMethod = $reflectionClass->getMethod('confGetValue');
      }
      catch (\ReflectionException $e) {
        throw new \RuntimeException('Unexpected exception', 0, $e);
      }
      if (NULL === $confGetValueMethod) {
        return [];
      }

      $pluginTypeNames = $this->reflectionMethodGetReturnTypeNames($confGetValueMethod);
    }
    else {
      $stubDefinition = ['handler_class' => $reflectionClass->getName()];

      $pluginTypeNames = self::classGetPluginTypeNames($reflectionClass);
    }

    if ([] === $pluginTypeNames) {
      return [];
    }

    $className = $reflectionClass->getShortName();

    $definitionsById = DefinitionUtil::buildDefinitionsById($stubDefinition, $annotations, $className);

    // The same plugin definition can apply to more than one plugin type.
    return array_fill_keys($pluginTypeNames, $definitionsById);
  }

  /**
   * Reads plugin definitions from a static method.
   *
   * The static method may either return an instance or a configurator.
   *
   * @param \ReflectionMethod $method
   *   The static method to analyse.
   *
   * @return array[][]
   *   Format: $[$pluginType][$pluginId] = $pluginDefinition
   */
  private function staticMethodGetDefinitions(\ReflectionMethod $method) {

    if (FALSE === $docComment = $method->getDocComment()) {
      return [];
    }

    $name = $method->getDeclaringClass()->getName() . '::' . $method->getName();

    try {
      $annotations = $this->docToAnnotations->docGetAnnotations($docComment);
    }
    catch (\Exception $e) {
      // The doc comment had an unexpected format.
      $this->reportFailingDeclaration(
        'Unexpected annotation format in method @method. Annotations will be ignored.',
        ['@method' => $name]);

      return [];
    }

    if (!$annotations) {
      // No annotations were found.
      return [];
    }

    if ([] === $returnTypeNames = $this->reflectionMethodGetReturnTypeNames($method)) {
      // The doc comment had an unexpected format.
      $this->reportFailingDeclaration(
        'Return type not declared in @method. Annotation will be ignored.',
        ['@method' => $name]);

      return [];
    }

    foreach ($returnTypeNames as $returnTypeName) {
      if (is_a($returnTypeName, ConfiguratorInterface::class, TRUE)) {
        // The method returns a configurator object.
        // The actual plugin type will be determined from the interfaces the
        // class implements.
        return self::configuratorFactoryGetDefinitions($method, $annotations);
      }
    }

    $definition = [
      'handler_factory' => $name,
    ];

    $definitionsById = DefinitionUtil::buildDefinitionsById($definition, $annotations, $name);

    // The same plugin definition can apply to more than one plugin type.
    $definitions = array_fill_keys($returnTypeNames, $definitionsById);

    if (!$definitions) {
      // None of the annotations produced a definition.
      $this->reportFailingDeclaration(
        'No definitions for annotation in @method.',
        ['@method' => $name]);
    }

    return $definitions;
  }

  /**
   * Gets definitions from a static method that returns a configurator object.
   *
   * @param \ReflectionMethod $method
   *   The static method.
   * @param array[] $annotations
   *   E.g. [['id' => 'entityTitle', 'label' => 'Entity title'], ..].
   *
   * @return array[][]
   *   Format: $[$pluginType][$pluginId] = $pluginDefinition.
   */
  private static function configuratorFactoryGetDefinitions(\ReflectionMethod $method, array $annotations) {

    $name = $method->getDeclaringClass()->getName() . '::' . $method->getName();

    $definition = [
      'configurator_factory' => $name,
    ];

    $pluginTypeNames = self::classGetPluginTypeNames($method->getDeclaringClass());
    $definitionsById = DefinitionUtil::buildDefinitionsById($definition, $annotations, $name);

    // The same plugin definition can apply to more than one plugin type.
    return array_fill_keys($pluginTypeNames, $definitionsById);
  }

  /**
   * Gets the return type names for a static method.
   *
   * These can be defined either in the doc comment or in native type hints.
   *
   * The return types become the plugin types.
   *
   * @param \ReflectionMethod $reflectionMethod
   *   The method.
   *
   * @return string[]
   *   List of return type names.
   *   Format: $[] = $interface.
   */
  private function reflectionMethodGetReturnTypeNames(\ReflectionMethod $reflectionMethod) {

    if (FALSE === $docComment = $reflectionMethod->getDocComment()) {
      return [];
    }

    if ([] === $returnTypeClassNames = DocUtil::docGetReturnTypeClassNames($docComment, $reflectionMethod->getDeclaringClass()->getName())) {
      return [];
    }

    $returnTypeInterfaceNames = [];
    foreach ($returnTypeClassNames as $returnTypeClassName) {
      try {
        $reflClass = new \ReflectionClass($returnTypeClassName);
      }
      catch (\ReflectionException $e) {
        continue;
      }
      if ($reflClass->isInterface()) {
        $returnTypeInterfaceNames[] = $returnTypeClassName;
      }
      elseif (!$reflClass->isTrait()) {
        foreach (self::classGetPluginTypeNames($reflClass) as $interfaceName) {
          $returnTypeInterfaceNames[] = $interfaceName;
        }
      }
    }

    return array_unique($returnTypeInterfaceNames);
  }

  /**
   * Gets plugin type names for a class.
   *
   * In the current implementation this is the top-level interface names that
   * the class implements.
   *
   * @param \ReflectionClass $reflectionClass
   *   The class.
   *
   * @return string[]
   *   Format: $[] = $interface.
   */
  private static function classGetPluginTypeNames(\ReflectionClass $reflectionClass) {

    if ($reflectionClass->isInterface()) {
      return [$reflectionClass->getName()];
    }

    $interfaces = $reflectionClass->getInterfaces();
    foreach ($interfaces as $interfaceName => $reflectionInterface) {
      if (!isset($interfaces[$interfaceName])) {
        continue;
      }
      foreach ($reflectionInterface->getInterfaceNames() as $nameToUnset) {
        unset($interfaces[$nameToUnset]);
      }
    }

    return array_keys($interfaces);
  }

  /**
   * Reports a failing plugin declaration.
   *
   * @param string $message
   *   Message with placeholders.
   * @param array $replacements
   *   Replacements as for t().
   */
  private function reportFailingDeclaration($message, array $replacements) {
    // @todo Also show this in the messages area, based on conditions.
    watchdog(
      'cfrplugindiscovery',
      $message,
      $replacements);
  }

}
