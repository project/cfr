<?php

namespace Drupal\cfrplugindiscovery\ClassFileToDefinitions;

/**
 * Object to detect plugin definitions in a class file.
 */
interface ClassFileToDefinitionsInterface {

  /**
   * Detects plugin definitions in a class file.
   *
   * @param string $class
   *   The class name.
   * @param string $file
   *   Path to the class file.
   *
   * @return array[][]
   *   Plugin definitions found.
   *   Format: $[$pluginType][$pluginId] = $pluginDefinition
   */
  public function classFileGetDefinitions($class, $file);

}
