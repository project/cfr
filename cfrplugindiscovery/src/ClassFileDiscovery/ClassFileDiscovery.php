<?php

namespace Drupal\cfrplugindiscovery\ClassFileDiscovery;

use Drupal\cfrplugindiscovery\Util\DiscoveryUtil;

/**
 * Default implementation to search a PSR-4 directory structure.
 */
class ClassFileDiscovery implements ClassFileDiscoveryInterface {

  /**
   * {@inheritdoc}
   */
  public function dirNspGetClassFiles($directory, $namespace) {
    DiscoveryUtil::normalizeDirectory($directory);
    DiscoveryUtil::normalizeNamespace($namespace);
    return $this->dirNspFindClassFilesRecursive($directory, $namespace);
  }

  /**
   * Finds class files in a PSR-4 structure, recursively.
   *
   * @param string $parentDir
   *   Directory.
   * @param string $parentNamespace
   *   Namespace mapped to the directory.
   *
   * @return string[]
   *   Format: $[$file] = $class
   */
  private function dirNspFindClassFilesRecursive($parentDir, $parentNamespace) {
    $classFiles = [];
    foreach (scandir($parentDir, SCANDIR_SORT_ASCENDING) as $candidate) {
      if ('.' === $candidate[0]) {
        continue;
      }
      $path = $parentDir . '/' . $candidate;
      if ('.php' === substr($candidate, -4)) {
        $name = substr($candidate, 0, -4);
        $class = $parentNamespace . $name;
        $classFiles[$path] = $class;
      }
      elseif (is_dir($path)) {
        $classFiles += $this->dirNspFindClassFilesRecursive($path, $parentNamespace . $candidate . '\\');
      }
    }
    return $classFiles;
  }

}
