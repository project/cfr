<?php

namespace Drupal\cfrplugindiscovery\ClassFileDiscovery;

/**
 * Object to discover class files in a directory structure.
 */
interface ClassFileDiscoveryInterface {

  /**
   * Discovers class files in a directory structure.
   *
   * @param string $directory
   *   Parent directory which contains the class files.
   *   Typically this is a PSR-4 directory.
   * @param string $namespace
   *   Namespace associated with the directory.
   *
   * @return string[]
   *   Class files discovered.
   *   Format: $[$file] = $class
   */
  public function dirNspGetClassFiles($directory, $namespace);

}
