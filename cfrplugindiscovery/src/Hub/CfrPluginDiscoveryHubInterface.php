<?php

namespace Drupal\cfrplugindiscovery\Hub;

/**
 * Object used as a "global" access point to cfrplugindiscovery functionality.
 *
 * Returned by cfrplugindiscovery().
 *
 * @see \cfrplugindiscovery()
 */
interface CfrPluginDiscoveryHubInterface {

  /**
   * Discovers plugin definitions in a PSR-4 namespace directory.
   *
   * @param string $directory
   *   Path to the PSR-4 directory.
   * @param string $namespace
   *   Namespace associated with the directory.
   *
   * @return array[][]
   *   Discovered plugin definitions.
   *   Format: $[$pluginType][$pluginId] = $pluginDefinition
   */
  public function discoverByInterface($directory, $namespace);

  /**
   * Discovers plugin definitions for a Drupal module with PSR-4 directory.
   *
   * This works for modules that follow the Drupal 8 convention of mapping the
   * /src/ directory to the module namespace "Drupal\\$module_name".
   *
   * The signature of the method allows to copy + paste the calling code to
   * other modules, without replacing the module name.
   *
   * @param string $__FILE__
   *   Path to a *.module file.
   *   Since this is usually called from such a file, modules will usually pass
   *   __FILE__.
   *
   * @return array[][]
   *   Discovered plugin definitions.
   *   Format: $[$pluginType][$pluginId] = $pluginDefinition
   */
  public function moduleFileScanPsr4($__FILE__);

}
