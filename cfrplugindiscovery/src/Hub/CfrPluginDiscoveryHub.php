<?php

namespace Drupal\cfrplugindiscovery\Hub;

use Drupal\cfrplugin\Util\UiUtil;
use Drupal\cfrplugindiscovery\ClassFileDiscovery\ClassFileDiscovery;
use Drupal\cfrplugindiscovery\ClassFileDiscovery\ClassFileDiscoveryInterface;
use Drupal\cfrplugindiscovery\ClassFileToDefinitions\ClassFileToDefinitions_NativeReflection;
use Drupal\cfrplugindiscovery\ClassFileToDefinitions\ClassFileToDefinitionsInterface;

/**
 * Default implementation.
 */
class CfrPluginDiscoveryHub implements CfrPluginDiscoveryHubInterface {

  /**
   * @var \Drupal\cfrplugindiscovery\ClassFileDiscovery\ClassFileDiscoveryInterface
   */
  private $classFileDiscovery;

  /**
   * @var \Drupal\cfrplugindiscovery\ClassFileToDefinitions\ClassFileToDefinitionsInterface
   */
  private $classFileToDefinitions;

  /**
   * @var string[]
   */
  private $classesToExclude = [];

  /**
   * Static factory.
   *
   * @param string $tagName
   *   Annotation tag name.
   *
   * @return static
   *   Created instance.
   */
  public static function create($tagName = 'CfrPlugin') {
    return new self(
      new ClassFileDiscovery(),
      ClassFileToDefinitions_NativeReflection::create($tagName));
  }

  /**
   * Constructor.
   *
   * @param \Drupal\cfrplugindiscovery\ClassFileDiscovery\ClassFileDiscoveryInterface $classFileDiscovery
   *   Object to discover class files in a PSR-4 directory structure.
   * @param \Drupal\cfrplugindiscovery\ClassFileToDefinitions\ClassFileToDefinitionsInterface $classFileToDefinitions
   *   Object to discover plugin definitions inside a class file.
   */
  public function __construct(
    ClassFileDiscoveryInterface $classFileDiscovery,
    ClassFileToDefinitionsInterface $classFileToDefinitions
  ) {
    $this->classFileDiscovery = $classFileDiscovery;
    $this->classFileToDefinitions = $classFileToDefinitions;
  }

  /**
   * Immutable setter. Excludes a class from discovery.
   *
   * This can be necessary e.g. if the base class is defined in another module
   * which is not an explicit dependency, so including the class file would lead
   * to a fatal error if the other module is not enabled.
   *
   * @param string $classToExclude
   *   Class to be excluded in the discovery process.
   *
   * @return static
   *   Modified cloned instance.
   */
  public function withoutClass($classToExclude) {
    $clone = clone $this;
    $clone->classesToExclude[$classToExclude] = $classToExclude;
    return $clone;
  }

  /**
   * Immutable setter. Excludes classes from discovery.
   *
   * This is a multiple-value version of the above.
   *
   * @param string[] $classesToExclude
   *   Classes to be excluded in the discovery process.
   *
   * @return static
   *   Modified cloned instance.
   */
  public function withoutClasses(array $classesToExclude) {
    $clone = clone $this;
    foreach ($classesToExclude as $classToExclude) {
      $clone->classesToExclude[$classToExclude] = $classToExclude;
    }
    return $clone;
  }

  /**
   * {@inheritdoc}
   */
  public function moduleFileScanPsr4($__FILE__) {

    // The regex could use '\w' instead of '[A-Za-z0-9_]', but we don't.
    /** @noinspection NotOptimalRegularExpressionsInspection */
    if (!preg_match('@/([A-Za-z_][A-Za-z0-9_]*)\.module$@', $__FILE__, $m)) {
      throw new \InvalidArgumentException("Not a module file path: '$__FILE__'.");
    }

    $module = $m[1];

    return $this->discoverByInterface(\dirname($__FILE__) . '/src', 'Drupal\\' . $module);
  }

  /**
   * {@inheritdoc}
   */
  public function discoverByInterface($directory, $namespace) {

    $classFiles = $this->classFileDiscovery->dirNspGetClassFiles($directory, $namespace);

    if ([] !== $this->classesToExclude) {
      // This does preserve keys.
      $classFiles = array_diff($classFiles, $this->classesToExclude);
    }

    $definitionss = $this->classFilesGetDefinitions($classFiles, $duplicates);

    if ($duplicates) {
      watchdog(
        'cfrplugindiscovery',
        'Duplicate plugin declarations in @namespace: !duplicates.',
        [
          '@namespace' => $namespace,
          '!duplicates' => UiUtil::exportData($duplicates),
        ]);
    }

    return $definitionss;
  }

  /**
   * Gets plugin definitions for a list of class files.
   *
   * @param string[] $classFiles
   *   Format: $[$file] = $class.
   * @param array[][][] $duplicates
   *   Format: $[$type][$id][] = $definition.
   *
   * @return array[][]
   *   Format: $[$pluginType][$pluginId] = $pluginDefinition.
   */
  private function classFilesGetDefinitions(array $classFiles, array &$duplicates = NULL) {

    if ($duplicates === NULL) {
      $duplicates = [];
    }

    $definitionsByTypeAndId = [];
    $definitionsByTypeAndIdAndFile = [];
    foreach ($classFiles as $file => $class) {
      foreach ($this->classFileToDefinitions->classFileGetDefinitions($class, $file) as $type => $definitionsById) {
        foreach ($definitionsById as $id => $definition) {
          $definitionsByTypeAndId[$type][$id] = $definition;
          $definitionsByTypeAndIdAndFile[$type][$id][$file] = $definition;
        }
      }
    }

    foreach ($definitionsByTypeAndIdAndFile as $type => $definitionsByIdAndFile) {
      foreach ($definitionsByIdAndFile as $id => $definitionsByFile) {
        if (count($definitionsByFile) > 1) {
          $duplicates[$type][$id] = $definitionsByFile;
        }
      }
    }

    return $definitionsByTypeAndId;
  }

}
