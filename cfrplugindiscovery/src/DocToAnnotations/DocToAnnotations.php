<?php

namespace Drupal\cfrplugindiscovery\DocToAnnotations;

use Donquixote\Annotation\DocCommentUtil;
use Donquixote\Annotation\Resolver\AnnotationResolver_PrimitiveResolver;
use Donquixote\Annotation\Resolver\AnnotationResolverInterface;
use Donquixote\Annotation\Value\DoctrineAnnotation\DoctrineAnnotationInterface;

/**
 * Default implementation.
 */
class DocToAnnotations implements DocToAnnotationsInterface {

  /**
   * @var string
   */
  private $tagName;

  /**
   * @var \Donquixote\Annotation\Resolver\AnnotationResolverInterface
   */
  private $annotationResolver;

  /**
   * Static factory.
   *
   * @param string $tagName
   *   Annotation tag name to look for, without the '@'.
   *
   * @return self
   *   Created instance.
   */
  public static function create($tagName) {
    return new self($tagName, AnnotationResolver_PrimitiveResolver::create());
  }

  /**
   * Constructor.
   *
   * @param string $tagName
   *   Annotation tag name to look for, without the '@'.
   * @param \Donquixote\Annotation\Resolver\AnnotationResolverInterface $annotationResolver
   *   Object to resolve values inside a raw annotation.
   */
  public function __construct($tagName, AnnotationResolverInterface $annotationResolver) {
    $this->tagName = $tagName;
    $this->annotationResolver = $annotationResolver;
  }

  /**
   * {@inheritdoc}
   */
  public function docGetAnnotations($docComment) {

    if (FALSE === strpos($docComment, '@' . $this->tagName)) {
      return [];
    }

    $annotations = [];
    foreach (DocCommentUtil::docGetDoctrineAnnotations($docComment, $this->tagName, $this->annotationResolver) as $doctrineAnnotation) {
      $annotation = [];
      foreach ($doctrineAnnotation->getArguments() as $k => $v) {
        if ($v instanceof DoctrineAnnotationInterface) {
          if (NULL === $v = $this->resolveAnnotation($v->getName(), $v->getArguments())) {
            continue;
          }
        }
        elseif (\is_object($v)) {
          continue;
        }
        $annotation[$k] = $v;
      }
      $annotations[] = $annotation;
    }

    return $annotations;
  }

  /**
   * Resolves a child annotation within a top-level annotation.
   *
   * E.g. '[at]CfrPlugin("hello", [at]Translate("Hello"))'.
   *
   * @param string $name
   *   Annotation tag name.
   * @param array $args
   *   Argument values of the child annotation.
   *
   * @return mixed|null
   *   Resolved value, or NULL if not supported.
   */
  private function resolveAnnotation($name, array $args) {

    if ($name === 't' || $name === 'Translate') {
      if (isset($args[0]) && \is_string($args[0])) {
        // Suppress inspection:
        // 'Only string literals should be passed to t() where possible'.
        // phpcs:ignore
        return t($args[0]);
      }
    }

    return NULL;
  }

}
