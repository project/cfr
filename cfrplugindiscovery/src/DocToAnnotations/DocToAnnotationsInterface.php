<?php

namespace Drupal\cfrplugindiscovery\DocToAnnotations;

/**
 * Object to discover annotations in a doc comment.
 */
interface DocToAnnotationsInterface {

  /**
   * Discovers annotations in a doc comment.
   *
   * @param string $docComment
   *   The doc comment on a class or method.
   *
   * @return array[]
   *   List of annotations discovered.
   *
   * @throws \Exception
   *   If an annotation does not have the expected format.
   *   The exception type can differ per implementation.
   */
  public function docGetAnnotations($docComment);

}
