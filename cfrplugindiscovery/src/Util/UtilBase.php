<?php

namespace Drupal\cfrplugindiscovery\Util;

/**
 * Base class for utility classes with only static methods.
 *
 * Typically, subclasses will also be marked as final.
 *
 * An equivalent class exists in cfrapi.
 * This duplicate should have been avoided, but now it is here, it shall stay.
 *
 * @see \Drupal\cfrapi\Util\UtilBase
 */
abstract class UtilBase {

  /**
   * Private constructor.
   *
   * This prevents the class from being instantiated.
   */
  private function __construct() {}

}
