<?php

namespace Drupal\cfrplugindiscovery\Util;

/**
 * Utility class with static methods related to directories and namespaces.
 */
final class DiscoveryUtil extends UtilBase {

  /**
   * Asserts that a directory has the expected format.
   *
   * @param string $directory
   *   Valid path to an existing directory, without trailing slash.
   */
  public static function normalizeDirectory(&$directory) {
    $path_lastchar = substr($directory, -1);
    if ('/' === $path_lastchar || '\\' === $path_lastchar) {
      throw new \InvalidArgumentException('Path must be provided without trailing slash or backslash.');
    }
    if (!is_dir($directory)) {
      throw new \InvalidArgumentException('Not a directory: ' . check_plain($directory));
    }
  }

  /**
   * Normalizes a namespaces to the the expected format 'NN\\NN\\'.
   *
   * @param string $namespace
   *   Namespace name candidate.
   *   Before normalization, it must have the format 'NN\\NN', or '' for the
   *   root namespace.
   *   After normalization, it will have the format 'NN\\NN\\', or '' for the
   *   root namespace.
   */
  public static function normalizeNamespace(&$namespace) {
    if ('\\' === substr($namespace, -1)) {
      throw new \InvalidArgumentException('Namespace must be provided without trailing backslash.');
    }
    if (!empty($namespace) && '\\' === $namespace[0]) {
      throw new \InvalidArgumentException('Namespace must be provided without preceding backslash.');
    }
    if ('' !== $namespace) {
      $namespace .= '\\';
    }
  }

}
