<?php

namespace Drupal\cfrplugindiscovery\Util;

/**
 * Utility class for doc comment parsing.
 */
final class DocUtil extends UtilBase {

  /**
   * Gets the 'return' type names from a doc comment.
   *
   * This assumes that all types are in FQCN or 'self'/'static', as is
   * convention in Drupal. Aliases are NOT supported.
   *
   * @param string $docComment
   *   Doc comment from a method, including the '*' characters.
   * @param string $selfClassName
   *   Class where the method is defined.
   *
   * @return string[]
   *   Class names, with the leading '\\' removed.
   *   Any 'self' or 'static' is replaced with $selfClassName.
   */
  public static function docGetReturnTypeClassNames($docComment, $selfClassName) {

    if (!preg_match('~(?:^/\*\*\ +|\v\h*\* )@return\h+(\S+)~', $docComment, $m)) {
      return [];
    }

    $names = [];
    foreach (explode('|', $m[1]) as $alias) {

      if ('' === $alias) {
        continue;
      }

      if ('\\' === $alias[0] && preg_match('~(\\\\[a-zA-Z_][a-zA-Z_0-9]*)+~', $alias)) {
        $names[] = substr($alias, 1);
      }
      elseif ('self' === $alias || 'static' === $alias) {
        $names[] = $selfClassName;
      }
    }

    return $names;
  }

}
