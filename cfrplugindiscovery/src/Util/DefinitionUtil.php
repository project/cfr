<?php

namespace Drupal\cfrplugindiscovery\Util;

/**
 * Utility class with methods for definition-building.
 */
final class DefinitionUtil extends UtilBase {

  /**
   * Builds a list of definitions from given annotations.
   *
   * @param array $stubDefinition
   *   E.g. ['configurator_class' => 'MyConfigurator'].
   * @param array[] $annotations
   *   E.g. [['id' => 'entityTitle', 'label' => 'Entity title'], ..].
   * @param string $fallbackId
   *   E.g. 'EntityTitle'.
   *
   * @return array[]
   *   Definitions by id.
   */
  public static function buildDefinitionsById(array $stubDefinition, array $annotations, $fallbackId) {

    $definitionsById = [];
    foreach ($annotations as $annotation) {

      if (isset($annotation['id'])) {
        $id = $annotation['id'];
      }
      elseif (isset($annotation[0])) {
        $id = $annotation[0];
      }
      else {
        $id = $fallbackId;
      }

      if (isset($annotation['label'])) {
        $label = $annotation['label'];
      }
      elseif (isset($annotation[1])) {
        $label = $annotation[1];
      }
      else {
        $label = $id;
      }

      $definitionsById[$id] = $stubDefinition;
      $definitionsById[$id]['label'] = $label;

      if (array_key_exists('inline', $annotation) && TRUE === $annotation['inline']) {
        $definitionsById[$id]['inline'] = TRUE;
      }
    }

    return $definitionsById;
  }

  /**
   * Builds a definition list by type and id.
   *
   * @param string[] $types
   *   List of types, e.g. from the 'return' type information.
   *   Format: $[] = $type.
   * @param array[] $definitionsById
   *   List of definitions by id.
   *   Format: $[$id] = $definition.
   *
   * @return array[][]
   *   Format: $[$type][$id] = $definition.
   *
   * @deprecated
   *   This should not be a dedicated method.
   */
  public static function buildDefinitionsByTypeAndId(array $types, array $definitionsById) {

    return array_fill_keys($types, $definitionsById);
  }

}
