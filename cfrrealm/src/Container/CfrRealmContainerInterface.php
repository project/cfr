<?php

namespace Drupal\cfrrealm\Container;

/**
 * Contains services that are used throughout one configurator realm.
 *
 * In the main implementation, some of them have circular dependencies, which is
 * resolved through a proxy object for $definitionToConfigurator.
 *
 * Main cycle of circular dependencies:
 *
 * @property \Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface $typeToConfigurator
 *   Object to get a configurator for a given 'type'.
 * @property \Drupal\cfrrealm\TypeToCfrFamily\TypeToCfrFamilyInterface $typeToCfrFamily
 *   Object to get a "CfrFamily" for a given type / interface.
 * @property \Drupal\cfrfamily\DefmapToCfrFamily\DefmapToCfrFamilyInterface $defmapToCfrFamily
 *   Object to get a "CfrFamily" from a definition map.
 * @property \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface $definitionToConfigurator
 *   Object to produce a configurator from a plugin definition array.
 *
 * Non-circular:
 *
 * @property \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface $definitionToLabel
 *   Object to read the plugin label from a plugin definition array.
 * @property \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface $definitionToGrouplabel
 *   Object to read the group label from a plugin definition array.
 *
 * To be provided by child container:
 *
 * @property \Drupal\cfrrealm\TypeToDefmap\TypeToDefmapInterface $typeToDefmap
 *   Object to get a map of plugin definitions for a given type.
 *
 * Deprecation candidate:
 *
 * @property \Drupal\cfrrealm\TypeToContainer\TypeToContainerInterface $typeToContainer
 *   Object to get a family DI container for a given type / interface.
 * @property \Drupal\cfrfamily\DefmapToContainer\DefmapToContainerInterface $defmapToContainer
 *   Object to create a local DI container from a definition map.
 */
interface CfrRealmContainerInterface {}
