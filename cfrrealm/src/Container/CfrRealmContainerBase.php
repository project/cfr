<?php

namespace Drupal\cfrrealm\Container;

use Donquixote\Containerkit\Container\ContainerBase;
use Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabel;
use Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabel_FromModuleName;
use Drupal\cfrfamily\DefmapToCfrFamily\DefmapToCfrFamily;
use Drupal\cfrfamily\DefmapToCfrFamily\DefmapToCfrFamily_InlineExpanded;
use Drupal\cfrfamily\DefmapToContainer\DefmapToContainer;
use Drupal\cfrrealm\TypeToCfrFamily\TypeToCfrFamily_ViaDefmap;
use Drupal\cfrrealm\TypeToConfigurator\TypeToConfigurator_Buffer;
use Drupal\cfrrealm\TypeToConfigurator\TypeToConfigurator_ViaCfrFamily;
use Drupal\cfrrealm\TypeToContainer\TypeToContainer_Buffer;
use Drupal\cfrrealm\TypeToContainer\TypeToContainer_ViaDefmap;

/**
 * Contains services that are used throughout one configurator realm.
 */
abstract class CfrRealmContainerBase extends ContainerBase implements CfrRealmContainerInterface {

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface
   *   Object to get a configurator for a given 'type'.
   *
   * @see $typeToConfigurator
   */
  protected function get_typeToConfigurator() {
    $typeToConfigurator = new TypeToConfigurator_ViaCfrFamily($this->typeToCfrFamily);
    return new TypeToConfigurator_Buffer($typeToConfigurator);
  }

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrrealm\TypeToCfrFamily\TypeToCfrFamilyInterface
   *   Object to get a "CfrFamily" for a given type / interface.
   *
   * @see $typeToCfrFamily
   */
  protected function get_typeToCfrFamily() {
    return new TypeToCfrFamily_ViaDefmap($this->typeToDefmap, $this->defmapToCfrFamily);
  }

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrfamily\DefmapToCfrFamily\DefmapToCfrFamilyInterface
   *   Object to get a "CfrFamily" from a definition map.
   *
   * @see $defmapToCfrFamily
   */
  protected function get_defmapToCfrFamily() {
    return TRUE
      ? new DefmapToCfrFamily_InlineExpanded($this->definitionToConfigurator, $this->definitionToLabel, $this->definitionToGrouplabel)
      : new DefmapToCfrFamily($this->definitionToConfigurator, $this->definitionToLabel, $this->definitionToGrouplabel);
  }

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface
   *   Object to produce a configurator from a plugin definition array.
   *
   * @see $definitionToConfigurator
   */
  abstract protected function get_definitionToConfigurator();

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface
   *   Object to read the plugin label from a plugin definition array.
   *
   * @see $definitionToLabel
   */
  protected function get_definitionToLabel() {
    return DefinitionToLabel::create();
  }

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface
   *   Object to read the group label from a plugin definition array.
   *
   * @see $definitionToGrouplabel
   */
  protected function get_definitionToGrouplabel() {
    return new DefinitionToLabel_FromModuleName();
  }

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrrealm\TypeToDefmap\TypeToDefmapInterface
   *   Object to get a map of plugin definitions for a given type.
   *
   * @see $typeToDefmap
   */
  abstract protected function get_typeToDefmap();

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrrealm\TypeToContainer\TypeToContainerInterface
   *   Object to get a family DI container for a given type / interface.
   *
   * @see $typeToContainer
   */
  protected function get_typeToContainer() {
    $typeToContainer = new TypeToContainer_ViaDefmap($this->typeToDefmap, $this->defmapToContainer);
    return new TypeToContainer_Buffer($typeToContainer);
  }

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrfamily\DefmapToContainer\DefmapToContainerInterface
   *   Object to create a local DI container from a definition map.
   *
   * @see $defmapToContainer
   */
  protected function get_defmapToContainer() {
    return new DefmapToContainer($this->definitionToConfigurator, $this->definitionToLabel, $this->definitionToGrouplabel);
  }

}
