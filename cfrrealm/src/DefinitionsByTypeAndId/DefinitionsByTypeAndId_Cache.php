<?php

namespace Drupal\cfrrealm\DefinitionsByTypeAndId;

/**
 * Cache decorator.
 */
class DefinitionsByTypeAndId_Cache implements DefinitionsByTypeAndIdInterface {

  const CACHE_BIN = 'cache';

  /**
   * @var \Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndIdInterface
   */
  private $decorated;

  /**
   * @var string
   */
  private $cid;

  /**
   * DefinitionsByTypeAndIdBuffer constructor.
   *
   * @param \Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndIdInterface $decorated
   *   Decorated map of definitions.
   * @param string $cid
   *   Cache id.
   */
  public function __construct(DefinitionsByTypeAndIdInterface $decorated, $cid) {
    $this->decorated = $decorated;
    $this->cid = $cid;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionsByTypeAndId() {
    if ($cache = cache_get($this->cid, self::CACHE_BIN)) {
      return $cache->data;
    }
    $definitions = $this->decorated->getDefinitionsByTypeAndId();
    cache_set($this->cid, $definitions, self::CACHE_BIN);
    return $definitions;
  }

}
