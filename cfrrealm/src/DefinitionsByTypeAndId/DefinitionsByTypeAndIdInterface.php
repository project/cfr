<?php

namespace Drupal\cfrrealm\DefinitionsByTypeAndId;

/**
 * Nested list of plugin definitions by type and id.
 */
interface DefinitionsByTypeAndIdInterface {

  /**
   * Gets a list of plugin definitions by type and id.
   *
   * @return array[][]
   *   Plugin definition arrays by type and id.
   *   Format: $[$type][$id] = $definition
   */
  public function getDefinitionsByTypeAndId();

}
