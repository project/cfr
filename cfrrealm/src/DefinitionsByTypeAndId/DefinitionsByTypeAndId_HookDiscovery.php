<?php

namespace Drupal\cfrrealm\DefinitionsByTypeAndId;

/**
 * Implementation that gets definitions from a Drupal hook.
 */
class DefinitionsByTypeAndId_HookDiscovery implements DefinitionsByTypeAndIdInterface {

  /**
   * @var string
   */
  private $hook;

  /**
   * @var array
   */
  private $arguments;

  /**
   * Constructor.
   *
   * @param string $hook
   *   Hook name.
   * @param array $arguments
   *   Arguments to pass to the hook.
   */
  public function __construct($hook, array $arguments = []) {
    $this->hook = $hook;
    $this->arguments = $arguments;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionsByTypeAndId() {
    $definitions = [];
    $suffix = '_' . $this->hook;
    foreach (module_implements($this->hook) as $module) {
      foreach ($this->moduleGetDefinitionsByTypeAndId($module, $suffix) as $type => $definitionsById) {
        foreach ($definitionsById as $id => $definition) {
          if (!isset($definition['module'])) {
            $definition['module'] = $module;
          }
          else {
            $module = $definition['module'];
          }
          $definitions[$type][$module . '.' . $id] = $definition;
        }
      }
    }
    return $definitions;
  }

  /**
   * Gets the definitions from a specific module that implements the hook.
   *
   * @param string $module
   *   Module name.
   * @param string $suffix
   *   Suffix to append to the module name, that is, "_$hook".
   *
   * @return array[][]
   *   Definitions by type and id declared by this specific module.
   *   Format: $[$type][$id_suffix] = $definition.
   *   The module name will later be prepended to the id, to avoid name clashes
   *   between modules.
   */
  private function moduleGetDefinitionsByTypeAndId($module, $suffix) {
    $function = $module . $suffix;
    if (!\function_exists($function)) {
      return [];
    }
    $moduleDefinitionsByTypeAndId = \call_user_func_array($function, $this->arguments);
    if (!\is_array($moduleDefinitionsByTypeAndId)) {
      return [];
    }
    return $moduleDefinitionsByTypeAndId;
  }

}
