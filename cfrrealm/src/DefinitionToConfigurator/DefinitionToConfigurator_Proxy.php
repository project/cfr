<?php

namespace Drupal\cfrrealm\DefinitionToConfigurator;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface;

/**
 * Implementation using a factory for lazy proxy instantiation.
 */
class DefinitionToConfigurator_Proxy implements DefinitionToConfiguratorInterface {

  /**
   * @var \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface
   */
  private $instance;

  /**
   * @var callable
   */
  private $factory;

  /**
   * Constructor.
   *
   * @param callable $factory
   *   Signature: () -> DefinitionToConfiguratorInterface.
   */
  public function __construct($factory) {
    $this->factory = $factory;
  }

  /**
   * {@inheritdoc}
   */
  public function definitionGetConfigurator(array $definition, CfrContextInterface $context = NULL) {
    if (NULL === $this->instance) {
      $this->instance = \call_user_func($this->factory);
    }
    return $this->instance->definitionGetConfigurator($definition, $context);
  }

}
