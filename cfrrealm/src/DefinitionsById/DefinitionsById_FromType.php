<?php

namespace Drupal\cfrrealm\DefinitionsById;

use Drupal\cfrfamily\DefinitionsById\DefinitionsByIdInterface;
use Drupal\cfrrealm\TypeToDefinitionsbyid\TypeToDefinitionsbyidInterface;

/**
 * Implementation using a TypeToDefinitionsbyid* object.
 */
class DefinitionsById_FromType implements DefinitionsByIdInterface {

  /**
   * @var \Drupal\cfrrealm\TypeToDefinitionsbyid\TypeToDefinitionsbyidInterface
   */
  private $typeToDefinitionsById;

  /**
   * @var string
   */
  private $type;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrrealm\TypeToDefinitionsbyid\TypeToDefinitionsbyidInterface $typeToDefinitionsById
   *   Object that can get definitions for a range of types.
   * @param string $type
   *   Type for which to get definitions.
   */
  public function __construct(TypeToDefinitionsbyidInterface $typeToDefinitionsById, $type) {
    $this->typeToDefinitionsById = $typeToDefinitionsById;
    $this->type = $type;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionsById() {
    return $this->typeToDefinitionsById->typeGetDefinitionsbyid($this->type);
  }

}
