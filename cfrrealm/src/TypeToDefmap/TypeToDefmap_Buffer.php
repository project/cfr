<?php

namespace Drupal\cfrrealm\TypeToDefmap;

/**
 * Buffer decorator.
 */
class TypeToDefmap_Buffer implements TypeToDefmapInterface {

  /**
   * @var \Drupal\cfrrealm\TypeToDefmap\TypeToDefmapInterface
   */
  private $decorated;

  /**
   * @var \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface[]
   */
  private $definitionMaps = [];

  /**
   * Constructor.
   *
   * @param \Drupal\cfrrealm\TypeToDefmap\TypeToDefmapInterface $decorated
   *   Decorated TypeToDefmap* object.
   */
  public function __construct(TypeToDefmapInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function typeGetDefmap($type) {
    return array_key_exists($type, $this->definitionMaps)
      ? $this->definitionMaps[$type]
      : $this->definitionMaps[$type] = $this->decorated->typeGetDefmap($type);
  }

}
