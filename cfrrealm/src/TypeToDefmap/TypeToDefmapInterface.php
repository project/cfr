<?php

namespace Drupal\cfrrealm\TypeToDefmap;

/**
 * Object to get a definition map for a given type.
 */
interface TypeToDefmapInterface {

  /**
   * Get a plugin definition map for a given type.
   *
   * @param string $type
   *   The type or interface.
   *
   * @return \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface
   *   Map of plugin definitions.
   */
  public function typeGetDefmap($type);

}
