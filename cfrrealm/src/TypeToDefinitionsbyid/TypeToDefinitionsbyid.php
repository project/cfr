<?php

namespace Drupal\cfrrealm\TypeToDefinitionsbyid;

use Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndIdInterface;

/**
 * Implementation using a DefinitionsByTypeAndId* object.
 */
class TypeToDefinitionsbyid implements TypeToDefinitionsbyidInterface {

  /**
   * @var \Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndIdInterface
   */
  private $definitionsByTypeAndId;

  /**
   * @var array[][]|null
   */
  private $buffer;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrrealm\DefinitionsByTypeAndId\DefinitionsByTypeAndIdInterface $definitionsByTypeAndId
   *   Object that can provide all definitions for all types at once.
   */
  public function __construct(DefinitionsByTypeAndIdInterface $definitionsByTypeAndId) {
    $this->definitionsByTypeAndId = $definitionsByTypeAndId;
  }

  /**
   * {@inheritdoc}
   */
  public function typeGetDefinitionsbyid($type) {
    if (NULL === $this->buffer) {
      $this->buffer = $this->definitionsByTypeAndId->getDefinitionsByTypeAndId();
    }
    return isset($this->buffer[$type])
      ? $this->buffer[$type]
      : [];
  }

}
