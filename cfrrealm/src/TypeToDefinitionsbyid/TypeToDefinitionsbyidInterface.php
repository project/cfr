<?php

namespace Drupal\cfrrealm\TypeToDefinitionsbyid;

/**
 * Object to get plugin definitions for a given type.
 */
interface TypeToDefinitionsbyidInterface {

  /**
   * Gets plugin definitions for a given type.
   *
   * @param string $type
   *   A type name, usually an interface.
   *
   * @return array[]
   *   Array of all plugin definitions for the given plugin type.
   */
  public function typeGetDefinitionsbyid($type);

}
