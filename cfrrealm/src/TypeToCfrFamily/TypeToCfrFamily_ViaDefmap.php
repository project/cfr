<?php

namespace Drupal\cfrrealm\TypeToCfrFamily;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\DefmapToCfrFamily\DefmapToCfrFamilyInterface;
use Drupal\cfrrealm\TypeToDefmap\TypeToDefmapInterface;

/**
 * Implementation using TypeToDefmap* and DefmapToCfrFamily* objects.
 *
 * For a given type, it first gets the definition map, and from that it builds
 * the CfrFamily* object.
 */
class TypeToCfrFamily_ViaDefmap implements TypeToCfrFamilyInterface {

  /**
   * @var \Drupal\cfrrealm\TypeToDefmap\TypeToDefmapInterface
   */
  private $typeToDefmap;

  /**
   * @var \Drupal\cfrfamily\DefmapToCfrFamily\DefmapToCfrFamilyInterface
   */
  private $defmapToCfrFamily;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrrealm\TypeToDefmap\TypeToDefmapInterface $typeToDefmap
   *   Object to get a definition map for a given type.
   * @param \Drupal\cfrfamily\DefmapToCfrFamily\DefmapToCfrFamilyInterface $defmapToCfrFamily
   *   Object to get a CfrFamily* object for a given definition map.
   */
  public function __construct(TypeToDefmapInterface $typeToDefmap, DefmapToCfrFamilyInterface $defmapToCfrFamily) {
    $this->typeToDefmap = $typeToDefmap;
    $this->defmapToCfrFamily = $defmapToCfrFamily;
  }

  /**
   * {@inheritdoc}
   */
  public function typeGetCfrFamily($type, CfrContextInterface $context = NULL) {
    $defmap = $this->typeToDefmap->typeGetDefmap($type);
    return $this->defmapToCfrFamily->defmapGetCfrFamily($defmap, $context);
  }

}
