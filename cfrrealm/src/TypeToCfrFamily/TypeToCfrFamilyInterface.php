<?php

namespace Drupal\cfrrealm\TypeToCfrFamily;

use Drupal\cfrapi\Context\CfrContextInterface;

/**
 * Object to get a "CfrFamily" for a given type / interface.
 */
interface TypeToCfrFamilyInterface {

  /**
   * Gets a "CfrFamily" for a given type / interface.
   *
   * @param string $type
   *   The type, usually an interface name.
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   (optional) Context to constrain the available options.
   *
   * @return \Drupal\cfrfamily\CfrFamily\CfrFamilyInterface
   *   The "CfrFamily" for the given type.
   *   If the type is unknown, this will be an empty family with zero plugins.
   */
  public function typeGetCfrFamily($type, CfrContextInterface $context = NULL);

}
