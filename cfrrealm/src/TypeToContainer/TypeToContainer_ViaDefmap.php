<?php

namespace Drupal\cfrrealm\TypeToContainer;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\DefmapToContainer\DefmapToContainerInterface;
use Drupal\cfrrealm\TypeToDefmap\TypeToDefmapInterface;

/**
 * Implementation using a definition map.
 */
class TypeToContainer_ViaDefmap implements TypeToContainerInterface {

  /**
   * @var \Drupal\cfrrealm\TypeToDefmap\TypeToDefmapInterface
   */
  private $typeToDefmap;

  /**
   * @var \Drupal\cfrfamily\DefmapToContainer\DefmapToContainerInterface
   */
  private $defmapToContainer;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrrealm\TypeToDefmap\TypeToDefmapInterface $typeToDefmap
   *   Object to get a definition map for a given type.
   * @param \Drupal\cfrfamily\DefmapToContainer\DefmapToContainerInterface $defmapToContainer
   *   Object to get a family DI container for a given definition map.
   */
  public function __construct(TypeToDefmapInterface $typeToDefmap, DefmapToContainerInterface $defmapToContainer) {
    $this->typeToDefmap = $typeToDefmap;
    $this->defmapToContainer = $defmapToContainer;
  }

  /**
   * {@inheritdoc}
   */
  public function typeGetContainer($type, CfrContextInterface $context = NULL) {
    $definitionMap = $this->typeToDefmap->typeGetDefmap($type);
    return $this->defmapToContainer->defmapGetContainer($definitionMap, $context);
  }

}
