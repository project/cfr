<?php

namespace Drupal\cfrrealm\TypeToContainer;

use Drupal\cfrapi\Context\CfrContextInterface;

/**
 * Object to get a family DI container for a given type / interface.
 */
interface TypeToContainerInterface {

  /**
   * Gets a family container for a given type / interface.
   *
   * @param string $type
   *   A type, usually an interface.
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   Context to constrain the available options in configurators.
   *
   * @return \Drupal\cfrfamily\CfrFamilyContainer\CfrFamilyContainerInterface
   *   DI container with objects for the configurator family.
   */
  public function typeGetContainer($type, CfrContextInterface $context = NULL);

}
