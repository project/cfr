<?php

namespace Drupal\cfrrealm\TypeToConfigurator;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrrealm\TypeToCfrFamily\TypeToCfrFamilyInterface;

/**
 * Implementation using a TypeToCfrFamily* object.
 *
 * For a given type, it first gets a CfrFamily* object, and from that it gets a
 * drilldown configurator.
 */
class TypeToConfigurator_ViaCfrFamily implements TypeToConfiguratorInterface {

  /**
   * @var \Drupal\cfrrealm\TypeToCfrFamily\TypeToCfrFamilyInterface
   */
  private $typeToCfrFamily;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrrealm\TypeToCfrFamily\TypeToCfrFamilyInterface $typeToCfrFamily
   *   Object to get a CfrFamily* for a given type.
   */
  public function __construct(TypeToCfrFamilyInterface $typeToCfrFamily) {
    $this->typeToCfrFamily = $typeToCfrFamily;
  }

  /**
   * {@inheritdoc}
   */
  public function typeGetConfigurator($type, CfrContextInterface $context = NULL) {
    return $this->typeToCfrFamily->typeGetCfrFamily($type, $context)->getFamilyConfigurator();
  }

  /**
   * {@inheritdoc}
   */
  public function typeGetOptionalConfigurator($type, CfrContextInterface $context = NULL, $defaultValue = NULL) {
    return $this->typeToCfrFamily->typeGetCfrFamily($type, $context)->getOptionalFamilyConfigurator($defaultValue);
  }

}
