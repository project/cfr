<?php

namespace Drupal\cfrrealm\TypeToConfigurator;

use Drupal\cfrapi\Context\CfrContextInterface;

/**
 * Object to get a configurator for a given 'type'.
 *
 * Usually the 'type' is an interface name, and the configurator allows to
 * configure instances of said interface.
 */
interface TypeToConfiguratorInterface {

  /**
   * Gets a configurator for a given 'type'.
   *
   * @param string $type
   *   Type name, e.g. interface name.
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   Context to constrain the available values in the configurator.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator to configure an instance of the given type.
   */
  public function typeGetConfigurator($type, CfrContextInterface $context = NULL);

  /**
   * Gets an 'optional' configurator for a given 'type'.
   *
   * Check the 'OptionalConfiguratorInterface' for further explanations.
   *
   * @param string $type
   *   Type name, e.g. interface name.
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *   Context to constrain the available values in the configurator.
   * @param mixed $defaultValue
   *   Default value to return if the configuration is considered empty.
   *
   * @return \Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface
   *   Configurator object.
   *   On ->confGetValue(), the returned configurator will:
   *   - return an instance of $type, OR
   *   - return the default value (if the configuration is considered empty), OR
   *   - throw an exception (if the configuration is invalid).
   *
   * @todo The OptionalConfiguratorInterface will be removed, see #3165150.
   *   When this happens, the return type will need to be adjusted.
   */
  public function typeGetOptionalConfigurator($type, CfrContextInterface $context = NULL, $defaultValue = NULL);

}
