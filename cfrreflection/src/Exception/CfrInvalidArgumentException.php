<?php

/** @noinspection PhpDeprecationInspection */

namespace Drupal\cfrreflection\Exception;

use Drupal\cfrapi\Exception\InvalidConfigurationException;

/**
 * Obsolete exception class.
 *
 * @deprecated
 *   Use ConfToValueException instead.
 */
class CfrInvalidArgumentException extends InvalidConfigurationException {

}
