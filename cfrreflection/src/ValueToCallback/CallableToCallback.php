<?php

namespace Drupal\cfrreflection\ValueToCallback;

use Donquixote\CallbackReflection\Util\CallbackUtil;

/**
 * Implementation where the value is a callable.
 */
class CallableToCallback implements ValueToCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function valueGetCallback($callback) {
    return CallbackUtil::callableGetCallback($callback);
  }

}
