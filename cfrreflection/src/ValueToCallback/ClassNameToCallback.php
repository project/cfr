<?php

namespace Drupal\cfrreflection\ValueToCallback;

use Donquixote\CallbackReflection\Callback\CallbackReflection_ClassConstruction;

/**
 * Implementation where the value is a class name.
 */
class ClassNameToCallback implements ValueToCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function valueGetCallback($class) {
    return CallbackReflection_ClassConstruction::createFromClassNameCandidate($class);
  }

}
