<?php

namespace Drupal\cfrreflection\ValueToCallback;

/**
 * Object to turns a callback-like value into a callback-reflection object.
 *
 * The value could be e.g. a class name or method name from a definition array.
 * Different implementations expect different value types.
 *
 * @see \Drupal\cfrreflection\CfrGen\ArgDefToConfigurator\ArgDefToConfigurator_Callback
 */
interface ValueToCallbackInterface {

  /**
   * Gets a callback reflection object from a callback-like value.
   *
   * @param mixed $value
   *   A value that specifies a callback, typically from a plugin definition.
   *   The expected value type depends on the implementation.
   *
   * @return \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface|null
   *   Callback reflection object.
   */
  public function valueGetCallback($value);

}
