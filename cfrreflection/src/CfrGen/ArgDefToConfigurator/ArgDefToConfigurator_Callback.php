<?php

namespace Drupal\cfrreflection\CfrGen\ArgDefToConfigurator;

use Donquixote\CallbackReflection\Callback\CallbackReflection_BoundParameters;
use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrapi\Exception\ConfiguratorFactoryException;
use Drupal\cfrfamily\ArgDefToConfigurator\ArgDefToConfiguratorInterface;
use Drupal\cfrreflection\CfrGen\CallbackToConfigurator\CallbackToConfiguratorInterface;
use Drupal\cfrreflection\ValueToCallback\ValueToCallbackInterface;

/**
 * Implementation where the definition key specifies a callback.
 */
class ArgDefToConfigurator_Callback implements ArgDefToConfiguratorInterface {

  /**
   * @var \Drupal\cfrreflection\ValueToCallback\ValueToCallbackInterface
   */
  private $valueToCallback;

  /**
   * @var string
   */
  private $argsKey;

  /**
   * @var \Drupal\cfrreflection\CfrGen\CallbackToConfigurator\CallbackToConfiguratorInterface
   */
  private $callbackToConfigurator;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrreflection\ValueToCallback\ValueToCallbackInterface $valueToCallback
   *   Object to create a callback from the value stored in the definition.
   * @param string $argsKey
   *   Key in the definition where callback arguments may be stored.
   *   If present, these arguments are used for currying the callback.
   * @param \Drupal\cfrreflection\CfrGen\CallbackToConfigurator\CallbackToConfiguratorInterface $callbackToConfigurator
   *   Object to turn the callback into a configurator.
   */
  public function __construct(ValueToCallbackInterface $valueToCallback, $argsKey, CallbackToConfiguratorInterface $callbackToConfigurator) {
    $this->valueToCallback = $valueToCallback;
    $this->argsKey = $argsKey;
    $this->callbackToConfigurator = $callbackToConfigurator;
  }

  /**
   * {@inheritdoc}
   */
  public function argDefinitionGetConfigurator($arg, array $definition, CfrContextInterface $context = NULL) {

    $factory = $this->valueToCallback->valueGetCallback($arg);

    if (NULL === $factory) {
      throw new ConfiguratorFactoryException('Definition refers to a non-existing callback.');
    }

    if (!empty($definition[$this->argsKey])) {
      $namedArgs = $definition[$this->argsKey];
      $factory = new CallbackReflection_BoundParameters($factory, $namedArgs);
    }

    return $this->callbackToConfigurator->callbackGetConfigurator($factory, $context);
  }

}
