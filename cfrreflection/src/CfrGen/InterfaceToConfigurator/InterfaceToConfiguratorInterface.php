<?php

namespace Drupal\cfrreflection\CfrGen\InterfaceToConfigurator;

use Drupal\cfrapi\Context\CfrContextInterface;

/**
 * Object to get a configurator from a given interface.
 */
interface InterfaceToConfiguratorInterface {

  /**
   * Gets a configurator for a given interface.
   *
   * @param string $interface
   *   Qualified class name of an interface.
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   Context to constrain the available options.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator.
   */
  public function interfaceGetConfigurator($interface, CfrContextInterface $context = NULL);

  /**
   * Gets an optional configurator for a given interface.
   *
   * @param string $interface
   *   Qualified class name of an interface.
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   Context to constrain the available options.
   * @param mixed $defaultValue
   *   Default value to use if the "empty" configuration is provided.
   *
   * @return \Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface
   *   Optional configurator.
   *
   * @todo The OptionalConfiguratorInterface will be removed, see #3165150.
   *   When this happens, the return type will need to be adjusted.
   */
  public function interfaceGetOptionalConfigurator($interface, CfrContextInterface $context = NULL, $defaultValue = NULL);

}
