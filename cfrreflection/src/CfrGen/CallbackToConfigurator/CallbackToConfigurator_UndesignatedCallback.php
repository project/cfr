<?php

namespace Drupal\cfrreflection\CfrGen\CallbackToConfigurator;

use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\Configurator\Unconfigurable\Configurator_FixedValue;
use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrapi\Exception\ConfiguratorFactoryException;

/**
 * Creates a configurator for a callback with an unknown return value.
 */
class CallbackToConfigurator_UndesignatedCallback implements CallbackToConfiguratorInterface {

  /**
   * {@inheritdoc}
   */
  public function callbackGetConfigurator(CallbackReflectionInterface $callback, CfrContextInterface $context = NULL) {

    // @todo Test if can be called without arguments.
    try {
      $configuratorOrValue = $callback->invokeArgs([]);
    }
    catch (\Exception $e) {
      throw new ConfiguratorFactoryException(
        'Exception in callback: ' . $e->getMessage(),
        0,
        $e);
    }

    if ($configuratorOrValue instanceof ConfiguratorInterface) {
      return $configuratorOrValue;
    }

    // @todo Check if the value implements a specific interface.
    return new Configurator_FixedValue($configuratorOrValue);
  }

}
