<?php

namespace Drupal\cfrreflection\CfrGen\CallbackToConfigurator;

use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Drupal\cfrapi\Context\CfrContextInterface;

/**
 * Object to create a configurator from an callback specified for a plugin.
 *
 * Clues in the plugin definition determine which implementation of this
 * interface should be used.
 */
interface CallbackToConfiguratorInterface {

  /**
   * Gets a configurator for a callback.
   *
   * @param \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface $callback
   *   Callback that was specified in the plugin definition.
   *   Typically this is either a class construction or a static factory.
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *   Context to constrain the available options.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface|null
   *   Configurator to configure a plugin instance, or NULL if not available for
   *   the given context or for current site configuration.
   *
   * @throws \Drupal\cfrapi\Exception\ConfiguratorFactoryException
   *   If the callback itself throws an exception, or the callback does not have
   *   the expected signature, or if the callback return value is not as
   *   expected.
   */
  public function callbackGetConfigurator(CallbackReflectionInterface $callback, CfrContextInterface $context = NULL);

}
