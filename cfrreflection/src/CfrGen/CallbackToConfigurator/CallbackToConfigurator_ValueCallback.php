<?php

namespace Drupal\cfrreflection\CfrGen\CallbackToConfigurator;

use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Donquixote\CallbackReflection\CodegenHelper\CodegenHelper;
use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrapi\Exception\ConfiguratorFactoryException;
use Drupal\cfrfamily\Configurator\Inlineable\InlineableConfiguratorInterface;
use Drupal\cfrreflection\CfrGen\ParamToConfigurator\ParamToConfiguratorInterface;
use Drupal\cfrreflection\Configurator\Configurator_CallbackConfigurable;
use Drupal\cfrreflection\Configurator\Configurator_CallbackInlineable;
use Drupal\cfrreflection\Configurator\Configurator_CallbackSimple;
use Drupal\cfrreflection\ParamToLabel\ParamToLabelInterface;

/**
 * Implementation that uses the callback as a value callback.
 *
 * Parameter configurators are auto-generated based on the type hint.
 */
class CallbackToConfigurator_ValueCallback implements CallbackToConfiguratorInterface {

  /**
   * @var \Drupal\cfrreflection\CfrGen\ParamToConfigurator\ParamToConfiguratorInterface
   */
  private $paramToConfigurator;

  /**
   * @var \Drupal\cfrreflection\ParamToLabel\ParamToLabelInterface
   */
  private $paramToLabel;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrreflection\CfrGen\ParamToConfigurator\ParamToConfiguratorInterface $paramToConfigurator
   *   Object that can create configurators for parameters.
   * @param \Drupal\cfrreflection\ParamToLabel\ParamToLabelInterface $paramToLabel
   *   Object that can create labels for parameters.
   */
  public function __construct(ParamToConfiguratorInterface $paramToConfigurator, ParamToLabelInterface $paramToLabel) {
    $this->paramToConfigurator = $paramToConfigurator;
    $this->paramToLabel = $paramToLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function callbackGetConfigurator(CallbackReflectionInterface $valueCallback, CfrContextInterface $context = NULL) {

    $params = $valueCallback->getReflectionParameters();

    if (0 === $nParams = \count($params)) {
      // The callback has no parameters.
      // Return an optionless configurator.
      return new Configurator_CallbackSimple($valueCallback);
    }

    $paramConfigurators = [];
    $paramLabels = [];
    foreach ($params as $param) {
      $paramConfigurators[] = $paramConfigurator = $this->paramToConfigurator->paramGetConfigurator($param, $context);
      if (NULL === $paramConfigurator) {
        // No configurator can be created for the parameter.
        throw new ConfiguratorFactoryException(
          strtr(
            'No configurator can be created for parameter !param of !callable.',
            [
              '!param' => '$' . $param->getName(),
              '!callable' => $valueCallback->argsPhpGetPhp([], new CodegenHelper()),
            ]));
      }
      $paramLabels[] = $this->paramToLabel->paramGetLabel($param);
    }

    if (1 === $nParams) {
      // The callback has a single parameter only.
      if ($paramConfigurators[0] instanceof InlineableConfiguratorInterface) {
        // An "inlineable" configurator was created for the parameter.
        // This is the usual case: The default ParamToLabel implementation
        // always returns an inlineable configurator or NULL.
        return new Configurator_CallbackInlineable(
          $valueCallback,
          $paramConfigurators[0],
          $paramLabels[0]);
      }

      // The single parameter configurator is not "inlineable".
      // This is an unusual case.
      // Fall through to the "multiple parameters" case for BC.
    }

    // Return a group configurator, with one option per parameter.
    return new Configurator_CallbackConfigurable(
      $valueCallback,
      $paramConfigurators,
      $paramLabels);
  }

}
