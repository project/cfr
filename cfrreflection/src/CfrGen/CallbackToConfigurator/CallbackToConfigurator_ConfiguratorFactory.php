<?php

namespace Drupal\cfrreflection\CfrGen\CallbackToConfigurator;

use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Donquixote\CallbackReflection\CodegenHelper\CodegenHelper;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrapi\Exception\ConfiguratorFactoryException;

/**
 * Implementation where the callback is a configurator factory.
 *
 * Parameters of that factory are to be filled from the context.
 * All of the parameters must be optional, otherwise the plugin is rejected.
 */
class CallbackToConfigurator_ConfiguratorFactory implements CallbackToConfiguratorInterface {

  /**
   * {@inheritdoc}
   */
  public function callbackGetConfigurator(CallbackReflectionInterface $configuratorFactoryCallback, CfrContextInterface $context = NULL) {

    $serialArgs = [];
    foreach ($configuratorFactoryCallback->getReflectionParameters() as $param) {

      // @todo Only accept optional parameters.
      if ($context && $context->paramValueExists($param)) {
        $arg = $context->paramGetValue($param);
      }
      elseif ($param->isOptional()) {
        // The ReflectionException should not occur, because the parameter is
        // optional. The try/catch is to satisfy code inspection tools.
        try {
          $arg = $param->getDefaultValue();
        }
        catch (\ReflectionException $e) {
          throw new \RuntimeException('Unexpected exception.', 0, $e);
        }
      }
      else {
        throw new ConfiguratorFactoryException(
          'Only optional parameters are allowed in configurator factory methods.');
      }

      $serialArgs[] = $arg;
    }

    try {
      $configuratorCandidate = $configuratorFactoryCallback->invokeArgs($serialArgs);
    }
    catch (\Exception $e) {
      throw new ConfiguratorFactoryException(
        'Exception in callback: ' . $e->getMessage(),
        0,
        $e);
    }

    if ($configuratorCandidate instanceof ConfiguratorInterface) {
      return $configuratorCandidate;
    }

    if ($configuratorCandidate === NULL) {
      // The plugin is not available for the given context or current site
      // configuration.
      return NULL;
    }

    throw new ConfiguratorFactoryException(
      strtr(
        'Return value from !callback is expected to be an instance of ConfiguratorInterface or NULL, !value found instead.',
        [
          '!callback' => $configuratorFactoryCallback->argsPhpGetPhp([], new CodegenHelper()),
          '!value' => is_object($configuratorCandidate)
            ? get_class($configuratorCandidate) . ' object'
            : var_export($configuratorCandidate, TRUE),
        ]));
  }

}
