<?php

namespace Drupal\cfrreflection\CfrGen\ParamToConfigurator;

use Drupal\cfrapi\Context\CfrContextInterface;

/**
 * Object to magically get a configurator for a function parameter.
 *
 * The ->confGetValue() method of the configurator should return values that can
 * be passed to the given parameter.
 */
interface ParamToConfiguratorInterface {

  /**
   * Gets a configurator for a given function parameter.
   *
   * @param \ReflectionParameter $param
   *   The parameter.
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   Context object to constrain available options.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface|null
   *   Configurator to configure a value for the parameter, or NULL if the
   *   parameter type does not support this.
   */
  public function paramGetConfigurator(\ReflectionParameter $param, CfrContextInterface $context = NULL);

}
