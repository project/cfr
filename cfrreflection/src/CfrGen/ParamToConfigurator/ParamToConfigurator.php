<?php

namespace Drupal\cfrreflection\CfrGen\ParamToConfigurator;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface;

/**
 * Default implementation.
 *
 * Checks the parameter type hint for an interface, and creates a configurator
 * based on this interface.
 */
class ParamToConfigurator implements ParamToConfiguratorInterface {

  /**
   * @var \Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface
   */
  private $interfaceToConfigurator;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrrealm\TypeToConfigurator\TypeToConfiguratorInterface $interfaceToConfigurator
   *   Object to create a configurator for an interface.
   */
  public function __construct(TypeToConfiguratorInterface $interfaceToConfigurator) {
    $this->interfaceToConfigurator = $interfaceToConfigurator;
  }

  /**
   * {@inheritdoc}
   */
  public function paramGetConfigurator(\ReflectionParameter $param, CfrContextInterface $context = NULL) {
    $typeHintReflectionClassLike = $param->getClass();
    if (!$typeHintReflectionClassLike) {
      // The parameter does not have a type hint.
      // No configurator can be created.
      return NULL;
    }
    if (!$param->isOptional()) {
      return $this->interfaceToConfigurator->typeGetConfigurator(
        $typeHintReflectionClassLike->getName(),
        $context);
    }
    try {
      $default = $param->getDefaultValue();
    }
    catch (\ReflectionException $e) {
      throw new \RuntimeException('Unexpected exception.');
    }
    return $this->interfaceToConfigurator->typeGetOptionalConfigurator(
      $typeHintReflectionClassLike->getName(),
      $context,
      $default);
  }

}
