<?php

/** @noinspection PhpDeprecationInspection */

namespace Drupal\cfrreflection\ValueToValue;

use Donquixote\CallbackReflection\Callback\CallbackReflection_ClassConstruction;
use Donquixote\CallbackReflection\Callback\CallbackReflection_ObjectMethod;
use Donquixote\CallbackReflection\Callback\CallbackReflection_StaticMethod;
use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Donquixote\CallbackReflection\Util\CallbackUtil;
use Drupal\cfrapi\ValueToValue\ValueToValueInterface;
use Drupal\cfrreflection\Util\CfrReflectionUtil;

/**
 * Implementation using a CallbackReflecton* object.
 *
 * @deprecated
 *   This is not used, and will be removed in 7.x-3.x.
 *   See https://www.drupal.org/node/3168360.
 */
class ValueToValue_Callback implements ValueToValueInterface {

  /**
   * @var \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface
   */
  private $callback;

  /**
   * Static factory, with class construction as callback.
   *
   * @param string $className
   *   Name of a class, where the constructor should be used as the callback.
   *
   * @return \Drupal\cfrapi\ValueToValue\ValueToValueInterface
   *   Created instance.
   */
  public static function createFromClassName($className) {
    $callback = CallbackReflection_ClassConstruction::createFromClassName($className);
    return new self($callback);
  }

  /**
   * Static factory, with a static method as callback.
   *
   * @param string $className
   *   Name of the class where the static method is defined.
   * @param string $methodName
   *   Name of the static method.
   *
   * @return \Drupal\cfrapi\ValueToValue\ValueToValueInterface
   *   Created instance.
   */
  public static function createFromClassStaticMethod($className, $methodName) {
    $callback = CallbackReflection_StaticMethod::create($className, $methodName);
    return new self($callback);
  }

  /**
   * Static factory, with an object method as callback.
   *
   * @param object $object
   *   Object where the method should be called.
   * @param string $methodName
   *   Method name.
   *
   * @return \Drupal\cfrapi\ValueToValue\ValueToValueInterface
   *   Created instance.
   */
  public static function createFromObjectMethod($object, $methodName) {
    $callback = CallbackReflection_ObjectMethod::create($object, $methodName);
    return new self($callback);
  }

  /**
   * Static factory, with an arbitrary callable.
   *
   * @param string $callable
   *   The callable.
   *
   * @return \Drupal\cfrapi\ValueToValue\ValueToValueInterface
   *   Created instance.
   */
  public static function createFromCallable($callable) {
    $callback = CallbackUtil::callableGetCallback($callable);
    return new self($callback);
  }

  /**
   * Constructor.
   *
   * @param \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface $callback
   *   The callback, as CallbackReflection* object.
   */
  public function __construct(CallbackReflectionInterface $callback) {
    $this->callback = $callback;
  }

  /**
   * {@inheritdoc}
   */
  public function valueGetValue($args) {
    // @todo Parameter should be named $value, not $args.
    // @todo Value passed on should be [$value] or [$args], not $value or $args.
    return CfrReflectionUtil::callbackValidateAndInvoke($this->callback, $args);
  }

}
