<?php

namespace Drupal\cfrreflection\Configurator;

use Donquixote\CallbackReflection\Callback\CallbackReflection_ClassConstruction;
use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Donquixote\CallbackReflection\Util\CallbackUtil;
use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\Unconfigurable\Configurator_OptionlessBase;
use Drupal\cfrapi\Exception\ConfToValueException;

/**
 * Configurator which gets the value from a callback without parameters.
 */
class Configurator_CallbackSimple extends Configurator_OptionlessBase {

  /**
   * @var \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface
   */
  private $callback;

  /**
   * Static factory, using a class constructor as the callback.
   *
   * @param string $className
   *   Name of a class with no parameters in the constructor.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator where the value is an instance of the class provided.
   */
  public static function createFromClassName($className) {
    $callback = CallbackReflection_ClassConstruction::createFromClassName($className);
    return new self($callback);
  }

  /**
   * Static factory, using an arbitrary callable.
   *
   * @param mixed|callable $callable
   *   The callable.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator where the value is the return value of the callable.
   */
  public static function createFromCallable($callable) {
    $callback = CallbackUtil::callableGetCallback($callable);
    if (NULL === $callback) {
      return NULL;
    }
    return new self($callback);
  }

  /**
   * Constructor.
   *
   * @param \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface $callback
   *   The callback that returns the value.
   */
  public function __construct(CallbackReflectionInterface $callback) {
    $this->callback = $callback;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {

    try {
      return $this->callback->invokeArgs([]);
    }
    catch (\Exception $e) {
      throw new ConfToValueException("Exception during callback.", NULL, $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    return $this->callback->argsPhpGetPhp([], $helper);
  }

}
