<?php

namespace Drupal\cfrreflection\Configurator;

use Donquixote\CallbackReflection\Callback\CallbackReflection_ClassConstruction;
use Donquixote\CallbackReflection\Callback\CallbackReflection_ObjectMethod;
use Donquixote\CallbackReflection\Callback\CallbackReflection_StaticMethod;
use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Donquixote\CallbackReflection\Util\CallbackUtil;
use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\Group\Configurator_GroupBase;
use Drupal\cfrreflection\Util\CfrReflectionUtil;

/**
 * Group configurator that passes the group values to a callback as parameters.
 */
class Configurator_CallbackConfigurable extends Configurator_GroupBase {

  /**
   * @var \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface
   */
  private $callback;

  /**
   * Static factory, using a class constructor as the callback.
   *
   * @param string $className
   *   A known class. If the class does not exist, a ReflectionException will be
   *   thrown. This shall be treated as an "unchecked" exception here.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface[] $paramConfigurators
   *   One configurator for each parameter of the class constructor.
   *   Numeric/serial array keys shall be used.
   * @param string[] $labels
   *   One label for each parameter of the constructor.
   *   Numeric/serial array keys shall be used.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator, where ->confGetValue() returns an instance of the class
   *   provided.
   */
  public static function createFromClassName($className, array $paramConfigurators, array $labels) {
    $callback = CallbackReflection_ClassConstruction::createFromClassName($className);
    return new self($callback, $paramConfigurators, $labels);
  }

  /**
   * Static factory, using a static method as the callback.
   *
   * @param string $className
   *   A known class. If the class does not exist, a ReflectionException will be
   *   thrown. This shall be treated as an "unchecked" exception here.
   * @param string $methodName
   *   Name of a static method within the class. If the method does not exist, a
   *   ReflectionException will be thrown. This shall be treated as an
   *   "unchecked" exception here.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface[] $paramConfigurators
   *   One configurator for each parameter of the static method.
   *   Numeric/serial array keys shall be used.
   * @param string[] $labels
   *   One label for each parameter of the static method.
   *   Numeric/serial array keys shall be used.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator, where ->confGetValue() is a return value of the static
   *   method provided.
   */
  public static function createFromClassStaticMethod($className, $methodName, array $paramConfigurators, array $labels) {
    $callback = CallbackReflection_StaticMethod::create($className, $methodName);
    return new self($callback, $paramConfigurators, $labels);
  }

  /**
   * Static factory, using an object method as the callback.
   *
   * @param object $object
   *   The object that has the method.
   * @param string $methodName
   *   The method to call.
   * @param array $paramConfigurators
   *   One configurator for each parameter of the static method.
   *   Numeric/serial array keys shall be used.
   * @param array $labels
   *   One label for each parameter of the static method.
   *   Numeric/serial array keys shall be used.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator, where ->confGetValue() returns a return value of the object
   *   method provided.
   */
  public static function createFromObjectMethod($object, $methodName, array $paramConfigurators, array $labels) {
    $callback = CallbackReflection_ObjectMethod::create($object, $methodName);
    return new self($callback, $paramConfigurators, $labels);
  }

  /**
   * Static factory, using an arbitrary callable as the callback.
   *
   * @param callable $callable
   *   Callable that returns the value.
   * @param array $paramConfigurators
   *   One configurator for each parameter of the callable.
   *   Numeric/serial array keys shall be used.
   * @param array $labels
   *   One label for each parameter of the callable.
   *   Numeric/serial array keys shall be used.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator, where ->confGetValue() returns a return value of the
   *   callable provided.
   */
  public static function createFromCallable($callable, array $paramConfigurators, array $labels) {
    $callback = CallbackUtil::callableGetCallback($callable);
    return new self($callback, $paramConfigurators, $labels);
  }

  /**
   * Static factory, using a CallbackReflection* object.
   *
   * This is mostly just an alias of the constructor.
   *
   * @param \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface $callback
   *   The callback.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface[] $paramConfigurators
   *   One configurator for each parameter of the callback.
   *   Numeric/serial array keys shall be used.
   * @param string[] $labels
   *   One label for each parameter of the callback.
   *   Numeric/serial array keys shall be used.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator, where ->confGetValue() returns a return value of the
   *   callback provided.
   */
  public static function createFromCallback(CallbackReflectionInterface $callback, array $paramConfigurators, array $labels) {
    return new self($callback, $paramConfigurators, $labels);
  }

  /**
   * Constructor.
   *
   * @param \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface $callback
   *   The callback.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface[] $paramConfigurators
   *   One configurator for each parameter of the callback.
   *   Numeric/serial array keys shall be used.
   * @param string[] $labels
   *   One label for each parameter of the callback.
   *   Numeric/serial array keys shall be used.
   */
  public function __construct(CallbackReflectionInterface $callback, array $paramConfigurators = [], array $labels = []) {
    $this->callback = $callback;
    foreach ($paramConfigurators as $k => $paramConfigurator) {
      $paramLabel = isset($labels[$k]) ? $labels[$k] : $k;
      $this->keySetConfigurator($k, $paramConfigurator, $paramLabel);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    $args = parent::confGetValue($conf);
    return CfrReflectionUtil::callbackValidateAndInvoke($this->callback, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    $php_statements = $this->confGetPhpStatements($conf, $helper);
    return $this->callback->argsPhpGetPhp($php_statements, $helper);
  }

}
