<?php

namespace Drupal\cfrreflection\Configurator;

use Donquixote\CallbackReflection\Callback\CallbackReflection_ClassConstruction;
use Donquixote\CallbackReflection\Callback\CallbackReflection_StaticMethod;
use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\Configurator_DecoratorBase;
use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrreflection\Util\CfrReflectionUtil;

/**
 * Configurator which sends a value to a callback with single parameter.
 */
class Configurator_CallbackMono extends Configurator_DecoratorBase {

  /**
   * @var \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface
   */
  private $callback;

  /**
   * Static factory. Alias of the method below.
   *
   * @param string $className
   *   Class name, where the constructor has a single parameter.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $argConfigurator
   *   Configurator for the argument to pass to the constructor.
   *
   * @return self
   *   Created instance.
   */
  public static function fromClass($className, ConfiguratorInterface $argConfigurator) {
    $callback = CallbackReflection_ClassConstruction::createFromClassName($className);
    return new self($callback, $argConfigurator);
  }

  /**
   * Static factory.
   *
   * @param string $className
   *   Class name, where the constructor has a single parameter.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $argConfigurator
   *   Configurator for the argument to pass to the constructor.
   *
   * @return self
   *   Created instance.
   */
  public static function createFromClassName($className, ConfiguratorInterface $argConfigurator) {
    $callback = CallbackReflection_ClassConstruction::createFromClassName($className);
    return new self($callback, $argConfigurator);
  }

  /**
   * Static factory. Convenient alternative to the method below.
   *
   * @param callable|array $method
   *   Static method with a single parameter.
   *   Format: [$class_name, $method_name].
   *   This format allows an IDE to recognize the value as a callback.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $argConfigurator
   *   Configurator for the argument to pass to the constructor.
   *
   * @return self
   *   Created instance.
   */
  public static function fromStaticMethod(array $method, ConfiguratorInterface $argConfigurator) {
    $callback = CallbackReflection_StaticMethod::create($method[0], $method[1]);
    return new self($callback, $argConfigurator);
  }

  /**
   * Static factory.
   *
   * @param string $className
   *   Class name.
   * @param string $methodName
   *   Name of a static method in the class which has a single parameter.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $argConfigurator
   *   Configurator for the argument to pass to the method.
   *
   * @return self
   *   Created instance.
   */
  public static function createFromClassStaticMethod($className, $methodName, ConfiguratorInterface $argConfigurator) {
    $callback = CallbackReflection_StaticMethod::create($className, $methodName);
    return new self($callback, $argConfigurator);
  }

  /**
   * Constructor.
   *
   * @param \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface $monoParamCallback
   *   Callback with exactly one parameter.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $argConfigurator
   *   Configurator for the parameter.
   */
  public function __construct(CallbackReflectionInterface $monoParamCallback, ConfiguratorInterface $argConfigurator) {
    $this->callback = $monoParamCallback;
    parent::__construct($argConfigurator);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {
    $arg = parent::confGetValue($conf);
    return CfrReflectionUtil::callbackValidateAndInvoke($this->callback, [$arg]);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {
    $arg = parent::confGetPhp($conf, $helper);
    return $this->callback->argsPhpGetPhp([$arg], $helper);
  }

}
