<?php

namespace Drupal\cfrreflection\Configurator;

use Donquixote\CallbackReflection\Callback\CallbackReflection_ClassConstruction;
use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;
use Drupal\cfrfamily\CfrLegendProvider\CfrLegendProviderInterface;
use Drupal\cfrfamily\Configurator\Inlineable\InlineableConfiguratorBase;
use Drupal\cfrfamily\Configurator\Inlineable\InlineableConfiguratorInterface;
use Drupal\cfrreflection\Util\CfrReflectionUtil;

/**
 * Inlineable configurator with a single-parameter value callback.
 */
class Configurator_CallbackInlineable extends InlineableConfiguratorBase {

  /**
   * @var \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface
   */
  private $callback;

  /**
   * @var \Drupal\cfrfamily\Configurator\Inlineable\InlineableConfiguratorInterface
   */
  private $argConfigurator;

  /**
   * @var null|string
   */
  private $paramLabel;

  /**
   * Static factory, where a class constructor is used as the callback.
   *
   * @param string $className
   *   Name of a class where the constructor has a single parameter.
   * @param \Drupal\cfrfamily\Configurator\Inlineable\InlineableConfiguratorInterface $argConfigurator
   *   Configurator for the value to pass to the single parameter.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Configurator where the value is an instance of the class provided.
   */
  public static function createFromClassName($className, InlineableConfiguratorInterface $argConfigurator) {
    $callback = CallbackReflection_ClassConstruction::createFromClassName($className);
    return new self($callback, $argConfigurator);
  }

  /**
   * Constructor.
   *
   * @param \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface $monoParamCallback
   *   Callback with exactly one parameter.
   * @param \Drupal\cfrfamily\Configurator\Inlineable\InlineableConfiguratorInterface $argConfigurator
   *   Configurator for the value to pass to the single parameter.
   * @param string|null $paramLabel
   *   Label for the single parameter, or NULL if no label should be used.
   */
  public function __construct(CallbackReflectionInterface $monoParamCallback, InlineableConfiguratorInterface $argConfigurator, $paramLabel = NULL) {
    $this->callback = $monoParamCallback;
    $this->argConfigurator = $argConfigurator;
    $this->paramLabel = $paramLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getCfrLegend() {
    if (!$this->argConfigurator instanceof CfrLegendProviderInterface) {
      return NULL;
    }
    return $this->argConfigurator->getCfrLegend();
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {

    if (NULL === $label) {
      $label = $this->paramLabel;
    }
    elseif (NULL !== $this->paramLabel) {
      $label .= ' | ' . $this->paramLabel;
    }

    return $this->argConfigurator->confGetForm($conf, $label);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return $this->argConfigurator->confGetSummary($conf, $summaryBuilder);
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetValue($id, $optionsConf) {

    $arg = $this->argConfigurator->idConfGetValue($id, $optionsConf);

    return CfrReflectionUtil::callbackValidateAndInvoke($this->callback, [$arg]);
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetPhp($id, $conf, CfrCodegenHelperInterface $helper) {
    $php = $this->argConfigurator->idConfGetPhp($id, $conf, $helper);
    return $this->callback->argsPhpGetPhp([$php], $helper);
  }

}
