<?php

namespace Drupal\cfrreflection\ParamToLabel;

use Drupal\cfrreflection\Util\StringUtil;

/**
 * Implementation which generates a human name from the machine name.
 */
class ParamToLabel implements ParamToLabelInterface {

  /**
   * {@inheritdoc}
   */
  public function paramGetLabel(\ReflectionParameter $param) {
    return StringUtil::methodNameGenerateLabel($param->getName());
  }

}
