<?php

namespace Drupal\cfrreflection\ParamToLabel;

/**
 * A service to auto-generate a label from a reflection parameter.
 */
interface ParamToLabelInterface {

  /**
   * Gets a label for a given parameter.
   *
   * In the default implementation, the label is generated from the parameter
   * name, by converting camel humps to spaces.
   *
   * @param \ReflectionParameter $param
   *   The parameter.
   *
   * @return string|null
   *   Label generated for the parameter, or NULL if not found.
   */
  public function paramGetLabel(\ReflectionParameter $param);

}
