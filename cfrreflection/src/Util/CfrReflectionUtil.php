<?php

namespace Drupal\cfrreflection\Util;

use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\Util\UtilBase;

/**
 * Utility class to deal with conf-to-value callbacks.
 *
 * All methods throw ConfToValueException on failure, assuming that they are
 * only called in a conf-to-value context.
 */
final class CfrReflectionUtil extends UtilBase {

  /**
   * Validates an invokes a callback with arguments.
   *
   * @param \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface $callback
   *   The callback.
   * @param array|mixed $args
   *   Arguments to pass to the callback.
   *
   * @return mixed
   *   Return value of the callback.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   *   If the arguments don't match the callback signature, or the callback
   *   itself throws an exception.
   */
  public static function callbackValidateAndInvoke(CallbackReflectionInterface $callback, $args) {

    if (!\is_array($args)) {
      throw new ConfToValueException("Non-array callback arguments");
    }

    self::callbackAssertValidArgs($callback, $args);

    try {
      return $callback->invokeArgs($args);
    }
    catch (\Exception $e) {
      throw new ConfToValueException('Exception during callback', NULL, $e);
    }
  }

  /**
   * Asserts that given arguments are suitable for a given callback.
   *
   * @param \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface $callback
   *   The callback.
   * @param array $args
   *   The arguments that should be passed to the callback.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   *   If the arguments don't match the callback signature.
   */
  public static function callbackAssertValidArgs(CallbackReflectionInterface $callback, array $args) {

    $params = $callback->getReflectionParameters();

    if (array_keys($params) !== array_keys($args)) {
      throw new ConfToValueException('Wrong argument count.');
    }

    foreach ($callback->getReflectionParameters() as $i => $param) {

      if (!array_key_exists($i, $args)) {
        if ($param->isOptional()) {
          // All following parameters are optional.
          return;
        }

        throw new ConfToValueException("Required argument $i missing.");
      }

      $arg = $args[$i];

      if ($param->isOptional()) {
        try {
          $default = $param->getDefaultValue();
        }
        catch (\ReflectionException $e) {
          throw new \RuntimeException('Unexpected exception', 0, $e);
        }
        if ($args[$i] === $default) {
          continue;
        }
      }

      if ($param->isArray()) {
        if (!\is_array($arg)) {
          throw new ConfToValueException("Argument $i must be an array.");
        }
      }

      if ($paramClass = $param->getClass()) {
        if (!\is_object($arg)) {
          throw new ConfToValueException("Argument $i must be an object.");
        }
        if (!$paramClass->isInstance($arg)) {
          $paramClassExport = $paramClass->getName();
          throw new ConfToValueException("Argument $i must be an instance of $paramClassExport.");
        }
      }
    }
  }

}
