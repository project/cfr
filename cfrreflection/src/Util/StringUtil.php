<?php

namespace Drupal\cfrreflection\Util;

use Donquixote\CallbackReflection\Util\UtilBase;

/**
 * Utility class for string functionality.
 */
final class StringUtil extends UtilBase {

  /**
   * Splits a camel-case string into fragments.
   *
   * Credits:
   *   - Charl van Niekerk,
   *     http://blog.charlvn.za.net/2007/11/php-camelcase-explode-20.html.
   *   - Andreas Hennings / dqxtech
   *     http://dqxtech.net/blog/2011-03-04/php-camelcaseexplode-xl-version.
   *
   * @param string $string
   *   The original string, that we want to explode.
   * @param bool $lowercase
   *   TRUE, to convert all characters to lowercase.
   * @param string $example_string
   *   Example to specify how to deal with multiple uppercase characters.
   *   Can be something like "AA Bc" or "A A Bc" or "AABc".
   * @param string|bool $glue
   *   Allows to implode the fragments with sth like "_" or "." or " ".
   *   If $glue is FALSE, it will just return an array.
   *
   * @return string[]|string
   *   Array of fragments, or fragments glued together to a new string.
   *
   * @throws \InvalidArgumentException
   *   If $example_string is invalid.
   */
  public static function camelCaseExplode($string, $lowercase = TRUE, $example_string = 'AA Bc', $glue = FALSE) {
    static $regexp_by_example = [];
    if (!isset($regexp_by_example[$example_string])) {
      $regexp_by_example[$example_string] = self::camelCaseExplodeExampleToRegex($example_string);
    }
    $array = self::camelCaseExplodeWithRegex($regexp_by_example[$example_string], $string);
    if ($lowercase) {
      $array = array_map('strtolower', $array);
    }
    return \is_string($glue) ? implode($glue, $array) : $array;
  }

  /**
   * Splits a camel-case string into fragments, using a regular expression.
   *
   * @param string $regexp
   *   Regular expression to use in preg_split().
   * @param string $string
   *   The string to explode.
   *
   * @return string[]
   *   The fragments.
   */
  public static function camelCaseExplodeWithRegex($regexp, $string) {
    return preg_split($regexp, $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
  }

  /**
   * Gets a regular expression for camel-case-splitting from example string.
   *
   * @param string $example_string
   *   Example to specify how to deal with multiple uppercase characters.
   *   Can be something like "AA Bc" or "A A Bc" or "AABc".
   *
   * @return string
   *   Regular expression to use.
   *
   * @throws \InvalidArgumentException
   *   If an invalid example string was provided.
   */
  public static function camelCaseExplodeExampleToRegex($example_string) {
    static $regexp_available = [
      '/([A-Z][^A-Z]*)/',
      '/([A-Z][^A-Z]+)/',
      '/([A-Z]+[^A-Z]*)/',
    ];
    $orig = str_replace(' ', '', $example_string);
    $expected_exploded = explode(' ', $example_string);
    foreach ($regexp_available as $regexp) {
      $actual_exploded = self::camelCaseExplodeWithRegex($regexp, $orig);
      if ($expected_exploded === $actual_exploded) {
        return $regexp;
      }
    }
    throw new \InvalidArgumentException('Invalid example string.');
  }

  /**
   * Generates a human-readable label from an interface name.
   *
   * @param string $interface
   *   Interface name in UpperCamelCase, e.g. 'EntityDisplayInterface'.
   *
   * @return string
   *   Human name generated from the interface name, e.g. 'Entity display'.
   */
  public static function interfaceGenerateLabel($interface) {
    $title = $interface;
    if (FALSE !== $pos = strrpos($title, '\\')) {
      $title = substr($title, $pos + 1);
    }
    if ('Interface' === substr($title, -9) && 'Interface' !== $title) {
      $title = substr($title, 0, -9);
    }
    return self::methodNameGenerateLabel($title);
  }

  /**
   * Gets the part of a class name after the last namespace separator.
   *
   * @param string $class
   *   Complete class name, e.g. 'Acme\\Animal\\Giraffe'.
   *
   * @return string
   *   Class name without namespace, e.g. 'Giraffe'.
   */
  public static function classGetShortname($class) {
    return (FALSE !== $pos = strrpos($class, '\\'))
      ? substr($class, $pos + 1)
      : $class;
  }

  /**
   * Generates a human-readable label from a method name.
   *
   * In fact this is used for any kind of identifier, not just method names.
   *
   * @param string $methodName
   *   Identifier in lowerCamelCase or UpperCamelCase.
   *   Typically, namespace separators should be removed before calling this.
   *
   * @return string
   *   Human name generated from the identifier.
   */
  public static function methodNameGenerateLabel($methodName) {
    return ucfirst(self::camelCaseExplode($methodName, TRUE, 'AA Bc', ' '));
  }

}
