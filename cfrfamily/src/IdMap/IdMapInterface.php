<?php

namespace Drupal\cfrfamily\IdMap;

/**
 * Object with a list/map of ids.
 */
interface IdMapInterface {

  /**
   * Gets a list of available ids.
   *
   * @return string[]
   *   List of available ids.
   */
  public function getIds();

  /**
   * Checks if an id is part of the list.
   *
   * @param string $id
   *   The id to check.
   *
   * @return bool
   *   TRUE, if the id is part of the list.
   */
  public function idIsKnown($id);

}
