<?php

namespace Drupal\cfrfamily\IdMap;

use Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface;

/**
 * Decorator where ids are filtered by an IdToConfigurator* object.
 *
 * If no configurator is available for the given id, the id is removed.
 */
class IdMap_CfrMapFilter implements IdMapInterface {

  /**
   * @var \Drupal\cfrfamily\IdMap\IdMapInterface
   */
  private $decorated;

  /**
   * @var \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface
   */
  private $idToConfigurator;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\IdMap\IdMapInterface $decorated
   *   Decorated id map.
   * @param \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface $idToConfigurator
   *   Object to get a configurator for a given id.
   *   This is used to filter out ids where no configurator is available.
   */
  public function __construct(IdMapInterface $decorated, IdToConfiguratorInterface $idToConfigurator) {
    $this->decorated = $decorated;
    $this->idToConfigurator = $idToConfigurator;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {

    $ids = [];
    foreach ($this->decorated->getIds() as $id) {
      if (NULL !== $this->idToConfigurator->idGetConfigurator($id)) {
        $ids[] = $id;
      }
    }

    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function idIsKnown($id) {
    return $this->decorated->idIsKnown($id) && NULL !== $this->idToConfigurator->idGetConfigurator($id);
  }

}
