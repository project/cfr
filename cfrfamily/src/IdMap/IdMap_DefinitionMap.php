<?php

namespace Drupal\cfrfamily\IdMap;

use Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface;

/**
 * Implementation using the keys from a definition map.
 */
class IdMap_DefinitionMap implements IdMapInterface {

  /**
   * @var \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface
   */
  private $definitionMap;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface $definitionMap
   *   Map of plugin definitions.
   */
  public function __construct(DefinitionMapInterface $definitionMap) {
    $this->definitionMap = $definitionMap;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return array_keys($this->definitionMap->getDefinitionsById());
  }

  /**
   * {@inheritdoc}
   */
  public function idIsKnown($id) {
    return NULL !== $this->definitionMap->idGetDefinition($id);
  }

}
