<?php

namespace Drupal\cfrfamily\CfrFamily;

/**
 * Represents a "family" of configurators for a drilldown.
 *
 * Note that an "empty" family with zero definitions is still valid.
 *
 * NOTE: This may seem overengineered, but it has to stay like this until a
 * major rewrite.
 */
interface CfrFamilyInterface {

  /**
   * Gets the drilldown legend that powers the configurator.
   *
   * @return \Drupal\cfrfamily\CfrLegend\CfrLegendInterface
   *   The drilldown legend.
   */
  public function getCfrLegend();

  /**
   * Gets a configurator based on the drilldown legend.
   *
   * @return \Drupal\cfrfamily\Configurator\Inlineable\InlineableConfiguratorInterface
   *   The drilldown configurator.
   */
  public function getFamilyConfigurator();

  /**
   * Gets an optional configurator based on the drilldown legend.
   *
   * @param mixed $defaultValue
   *   Default value the configurator should return if the configuration is
   *   considered 'empty'.
   *
   * @return \Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface
   *   The optional drilldown configurator.
   */
  public function getOptionalFamilyConfigurator($defaultValue = NULL);

}
