<?php

namespace Drupal\cfrfamily\CfrFamily;

use Drupal\cfrfamily\CfrLegend\CfrLegend_InlineExpanded;
use Drupal\cfrfamily\CfrLegend\CfrLegendInterface;
use Drupal\cfrfamily\Configurator\Composite\Configurator_CfrLegend;
use Drupal\cfrfamily\IdConfToValue\IdConfToValue_IdToCfrExpanded;
use Drupal\cfrfamily\IdConfToValue\IdConfToValue_IdToConfigurator;
use Drupal\cfrfamily\IdConfToValue\IdConfToValueInterface;
use Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface;

/**
 * Default implementation.
 */
class CfrFamily implements CfrFamilyInterface {

  /**
   * @var \Drupal\cfrfamily\CfrLegend\CfrLegendInterface
   */
  private $legend;

  /**
   * @var \Drupal\cfrfamily\IdConfToValue\IdConfToValue_IdToConfigurator
   */
  private $idConfToValue;

  /**
   * Static factory. Instance with "inline" items expanded.
   *
   * @param \Drupal\cfrfamily\CfrLegend\CfrLegendInterface $legend
   *   Drilldown legend, where some items might be marked as "inline".
   * @param \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface $idToConfigurator
   *   Object to get a sub-configurator for a chosen id.
   *
   * @return \Drupal\cfrfamily\CfrFamily\CfrFamilyInterface
   *   Created instance.
   */
  public static function createExpanded(CfrLegendInterface $legend, IdToConfiguratorInterface $idToConfigurator) {
    $legend = new CfrLegend_InlineExpanded($legend);
    $idConfToValue = new IdConfToValue_IdToCfrExpanded($idToConfigurator);
    return new self($legend, $idConfToValue);
  }

  /**
   * Static factory. Instance with "inline" items NOT expanded.
   *
   * @param \Drupal\cfrfamily\CfrLegend\CfrLegendInterface $legend
   *   Drilldown legend.
   * @param \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface $idToConfigurator
   *   Object to get a sub-configurator for a chosen id.
   *
   * @return \Drupal\cfrfamily\CfrFamily\CfrFamilyInterface
   *   Created instance.
   */
  public static function create(CfrLegendInterface $legend, IdToConfiguratorInterface $idToConfigurator) {
    return new self($legend, new IdConfToValue_IdToConfigurator($idToConfigurator));
  }

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\CfrLegend\CfrLegendInterface $legend
   *   Drilldown legend.
   * @param \Drupal\cfrfamily\IdConfToValue\IdConfToValueInterface $idConfToValue
   *   Drilldown value processor.
   */
  public function __construct(CfrLegendInterface $legend, IdConfToValueInterface $idConfToValue) {
    $this->legend = $legend;
    $this->idConfToValue = $idConfToValue;
  }

  /**
   * {@inheritdoc}
   */
  public function getCfrLegend() {
    return $this->legend;
  }

  /**
   * {@inheritdoc}
   */
  public function getFamilyConfigurator() {
    return new Configurator_CfrLegend(TRUE, $this->legend, $this->idConfToValue);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionalFamilyConfigurator($defaultValue = NULL) {
    return new Configurator_CfrLegend(FALSE, $this->legend, $this->idConfToValue);
  }

}
