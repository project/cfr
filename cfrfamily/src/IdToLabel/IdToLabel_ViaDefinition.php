<?php

namespace Drupal\cfrfamily\IdToLabel;

use Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface;
use Drupal\cfrfamily\IdToDefinition\IdToDefinitionInterface;

/**
 * Implementation that finds the label in a plugin definition.
 */
class IdToLabel_ViaDefinition implements IdToLabelInterface {

  /**
   * @var \Drupal\cfrfamily\IdToDefinition\IdToDefinitionInterface
   */
  private $idToDefinition;

  /**
   * @var \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface
   */
  private $definitionToLabel;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\IdToDefinition\IdToDefinitionInterface $idToDefinition
   *   Object to get a plugin definition for an id.
   * @param \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface $definitionToLabel
   *   Object to get a label for a definition.
   */
  public function __construct(IdToDefinitionInterface $idToDefinition, DefinitionToLabelInterface $definitionToLabel) {
    $this->idToDefinition = $idToDefinition;
    $this->definitionToLabel = $definitionToLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetLabel($id, $else = NULL) {

    if (NULL === $definition = $this->idToDefinition->idGetDefinition($id)) {
      return NULL;
    }

    return $this->definitionToLabel->definitionGetLabel($definition, NULL);
  }

}
