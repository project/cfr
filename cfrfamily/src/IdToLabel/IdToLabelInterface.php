<?php

namespace Drupal\cfrfamily\IdToLabel;

/**
 * Object to gets a label for a given id.
 */
interface IdToLabelInterface {

  /**
   * Gets a label for a given id.
   *
   * @param string $id
   *   The id.
   * @param string|null $else
   *   Fallback label to return if no label was specified.
   *
   * @return string|null
   *   The label, or NULL if not found.
   */
  public function idGetLabel($id, $else = NULL);

}
