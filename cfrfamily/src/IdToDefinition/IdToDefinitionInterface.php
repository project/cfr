<?php

namespace Drupal\cfrfamily\IdToDefinition;

/**
 * Object to get a plugin definition array for a given id.
 *
 * In the known implementation, this is combined with another interface to form
 * a "definition map".
 */
interface IdToDefinitionInterface {

  /**
   * Gets a plugin definition array for a given id.
   *
   * @param string $id
   *   The id.
   *
   * @return array|null
   *   A plugin definition array.
   */
  public function idGetDefinition($id);

}
