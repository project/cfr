<?php

namespace Drupal\cfrfamily\LegendItem;

use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;
use Drupal\cfrfamily\CfrLegendItem\CfrLegendItemInterface;

/**
 * Decorator for "inline" drilldown legend item.
 *
 * If an item is "inline", it can be listed as an option in a parent drilldown.
 */
class LegendItem_Inline implements CfrLegendItemInterface {

  /**
   * @var \Drupal\cfrfamily\CfrLegendItem\CfrLegendItemInterface
   */
  private $decorated;

  /**
   * @var string
   */
  private $groupLabel;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\CfrLegendItem\CfrLegendItemInterface $decorated
   *   Decorated legend item.
   * @param string $groupLabel
   *   Group label.
   */
  public function __construct(CfrLegendItemInterface $decorated, $groupLabel) {
    $this->decorated = $decorated;
    $this->groupLabel = $groupLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return $this->decorated->confGetSummary($conf, $summaryBuilder);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    return $this->decorated->confGetForm($conf, $label);
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->decorated->getLabel();
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupLabel() {
    return $this->groupLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function withLabels($label, $groupLabel) {
    return new self($this->decorated->withLabels($label, '?'), $groupLabel);
  }

  /**
   * {@inheritdoc}
   */
  public function isOptionless() {
    return $this->decorated->isOptionless();
  }

}
