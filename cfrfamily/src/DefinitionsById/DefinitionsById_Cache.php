<?php

namespace Drupal\cfrfamily\DefinitionsById;

use Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface;

/**
 * Cache decorator for DefinitionsById* / DefinitionMap*.
 */
class DefinitionsById_Cache implements DefinitionMapInterface {

  const CACHE_BIN = 'cache';

  /**
   * @var \Drupal\cfrfamily\DefinitionsById\DefinitionsByIdInterface
   */
  private $decorated;

  /**
   * @var string
   */
  private $cid;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\DefinitionsById\DefinitionsByIdInterface $decorated
   *   Decorated DefinitionsById* object.
   * @param string $cid
   *   Cache id.
   */
  public function __construct(DefinitionsByIdInterface $decorated, $cid) {
    $this->decorated = $decorated;
    $this->cid = $cid;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetDefinition($id) {
    $definitions = $this->getDefinitionsById();
    return isset($definitions[$id])
      ? $definitions[$id]
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionsById() {
    if ($cache = cache_get($this->cid, self::CACHE_BIN)) {
      return $cache->data;
    }
    $definitions = $this->decorated->getDefinitionsById();
    cache_set($this->cid, $definitions, self::CACHE_BIN);
    return $definitions;
  }

}
