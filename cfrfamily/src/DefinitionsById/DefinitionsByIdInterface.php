<?php

namespace Drupal\cfrfamily\DefinitionsById;

/**
 * List of plugin definitions by id.
 *
 * A "definition" is an array with instructions to create the plugin
 * configurator, and to produce a label + group label for showing the plugin in
 * a drilldown select.
 *
 * This interface on its own does not have a direct lookup method for a specific
 * id, it can only retrieve the complete list.
 */
interface DefinitionsByIdInterface {

  /**
   * Gets the list of definitions.
   *
   * @return array[]
   *   Array of all configurator definitions for this plugin type.
   */
  public function getDefinitionsById();

}
