<?php

namespace Drupal\cfrfamily\CfrLegend;

use Drupal\cfrapi\PossiblyOptionless\PossiblyOptionlessInterface;
use Drupal\cfrfamily\CfrLegendItem\CfrLegendItem;
use Drupal\cfrfamily\CfrLegendItem\CfrLegendItem_Parent;
use Drupal\cfrfamily\CfrLegendProvider\CfrLegendProviderInterface;
use Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface;
use Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface;
use Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface;

/**
 * Drilldown legend based on a definition map.
 *
 * The definition map could be coming from hook_cfrplugin_info().
 */
class CfrLegend_FromDefmap implements CfrLegendInterface {

  /**
   * @var \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface
   */
  private $definitionMap;

  /**
   * @var \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface
   */
  private $idToConfigurator;

  /**
   * @var \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface
   */
  private $definitionToLabel;

  /**
   * @var \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface
   */
  private $definitionToGroupLabel;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface $definitionMap
   *   The definition map, with a definition per drilldown option.
   * @param \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface $idToConfigurator
   *   Object to get a sub-configurator for a drilldown option.
   * @param \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface $definitionToLabel
   *   Object to get the label for a drilldown option.
   * @param \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface $definitionToGroupLabel
   *   Object to get the group label for a drilldown option.
   */
  public function __construct(
    DefinitionMapInterface $definitionMap,
    IdToConfiguratorInterface $idToConfigurator,
    DefinitionToLabelInterface $definitionToLabel,
    DefinitionToLabelInterface $definitionToGroupLabel
  ) {
    $this->definitionMap = $definitionMap;
    $this->idToConfigurator = $idToConfigurator;
    $this->definitionToLabel = $definitionToLabel;
    $this->definitionToGroupLabel = $definitionToGroupLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function getLegendItems($depth = 0) {
    static $rec = 0;
    if ($rec > 4) {
      throw new \RuntimeException('Possibly unlimited recursion detected.');
    }
    ++$rec;
    $items = [];
    foreach ($this->definitionMap->getDefinitionsById() as $id => $definition) {
      $legendItem = $this->idDefinitionGetLegendItem($id, $definition);
      if (NULL !== $legendItem) {
        $items[$id] = $legendItem;
      }
    }
    --$rec;
    return $items;
  }

  /**
   * Reads the group label from a plugin definition.
   *
   * @param array $definition
   *   Plugin definition.
   *
   * @return null|string
   *   The group label, or NULL to put it into the top-level group.
   */
  private function definitionGetGroupLabel(array $definition) {
    if (NULL === $this->definitionToGroupLabel) {
      return NULL;
    }
    $groupLabel = $this->definitionToGroupLabel->definitionGetLabel($definition, NULL);
    if ('' === $groupLabel) {
      return NULL;
    }
    return $groupLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetLegendItem($id) {
    $definition = $this->definitionMap->idGetDefinition($id);
    if (NULL === $definition) {
      return NULL;
    }
    return $this->idDefinitionGetLegendItem($id, $definition);
  }

  /**
   * {@inheritdoc}
   */
  public function idIsKnown($id) {
    return 1
      && NULL !== $this->definitionMap->idGetDefinition($id)
      && NULL !== $this->idToConfigurator->idGetConfigurator($id);
  }

  /**
   * Gets a legend item for a given id + definition.
   *
   * @param string $id
   *   Id of a drilldown option.
   * @param array $definition
   *   Plugin definition for the drilldown option.
   *
   * @return \Drupal\cfrfamily\CfrLegendItem\CfrLegendItemInterface|null
   *   The legend item, or NULL if not found or not available.
   */
  private function idDefinitionGetLegendItem($id, array $definition) {

    if (NULL === $configurator = $this->idToConfigurator->idGetConfigurator($id)) {
      return NULL;
    }

    $label = $this->definitionToLabel->definitionGetLabel($definition, $id);
    $groupLabel = $this->definitionGetGroupLabel($definition);

    if ($configurator instanceof PossiblyOptionlessInterface && $configurator->isOptionless()) {
      return new CfrLegendItem($label, $groupLabel, $configurator);
    }

    if (1
      && array_key_exists('inline', $definition)
      && TRUE === $definition['inline']
      && $configurator instanceof CfrLegendProviderInterface
      && NULL !== $innerCfrLegend = $configurator->getCfrLegend()
    ) {
      return new CfrLegendItem_Parent($label, $groupLabel, $configurator, $innerCfrLegend);
    }

    return new CfrLegendItem($label, $groupLabel, $configurator);
  }

}
