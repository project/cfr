<?php

namespace Drupal\cfrfamily\CfrLegend;

/**
 * Map of drilldown options for a drilldown configurator.
 *
 * Each "legend item" can produce a label, group label, and nested configurator.
 */
interface CfrLegendInterface {

  /**
   * Gets all legend items.
   *
   * @param int $depth
   *   If a nested configurator is "inlineable", that is, it is another
   *   drilldown or select element, then the nested options may be "inlined"
   *   into the parent drilldown.
   *   The $depth parameter sets a recursion limit for this inlining process.
   *   Note that the default value can be different per implementation.
   *
   * @return \Drupal\cfrfamily\CfrLegendItem\CfrLegendItemInterface[]
   *   Legend items representing the drilldown options.
   *   Format: $[$pluginId] = $cfrLegendItem.
   */
  public function getLegendItems($depth = 0);

  /**
   * Gets the legend item for a specific id.
   *
   * @param string $id
   *   The id to look up.
   *   In case of "inlined" drilldown options from a nested configurator, this
   *   will be a combined id like "$id/$child_id".
   *
   * @return \Drupal\cfrfamily\CfrLegendItem\CfrLegendItemInterface|null
   *   The legend item, or NULL if not found.
   */
  public function idGetLegendItem($id);

  /**
   * Checks if an id is part of the available options.
   *
   * @param string $id
   *   The id to check.
   *
   * @return bool
   *   TRUE, if the id is part of the available options.
   */
  public function idIsKnown($id);

}
