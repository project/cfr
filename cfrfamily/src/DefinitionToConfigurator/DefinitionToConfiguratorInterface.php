<?php

namespace Drupal\cfrfamily\DefinitionToConfigurator;

use Drupal\cfrapi\Context\CfrContextInterface;

/**
 * Object which can produce a configurator from a plugin definition array.
 */
interface DefinitionToConfiguratorInterface {

  /**
   * Creates a configurator from a definition array.
   *
   * @param array $definition
   *   The definition array, e.g. from hook_cfrplugin_info().
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   Context to constrain the available options.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface|null
   *   Configurator created from the definition, or NULL if the plugin is not
   *   available for the given context.
   *
   * @throws \Drupal\cfrapi\Exception\ConfiguratorFactoryException
   *   If the definition is malformed, or the callback it refers to does not
   *   have the expected signature or behavior.
   */
  public function definitionGetConfigurator(array $definition, CfrContextInterface $context = NULL);

}
