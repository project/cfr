<?php

namespace Drupal\cfrfamily\DefinitionToConfigurator;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\ArgDefToConfigurator\ArgDefToConfiguratorInterface;

/**
 * Implementation using mapper objects for specific keys in the definition.
 */
class DefinitionToConfigurator_Mappers implements DefinitionToConfiguratorInterface {

  /**
   * @var \Drupal\cfrfamily\ArgDefToConfigurator\ArgDefToConfiguratorInterface[]
   */
  private $mappers;

  /**
   * Sets / adds a mapper object for a specific key.
   *
   * @param string $key
   *   Key where to find a value in the definition.
   * @param \Drupal\cfrfamily\ArgDefToConfigurator\ArgDefToConfiguratorInterface $mapper
   *   Mapper object that should be called if the definition contains a value
   *   for the given key.
   */
  public function keySetMapper($key, ArgDefToConfiguratorInterface $mapper) {
    $this->mappers[$key] = $mapper;
  }

  /**
   * {@inheritdoc}
   */
  public function definitionGetConfigurator(array $definition, CfrContextInterface $context = NULL) {

    foreach ($this->mappers as $key => $mapper) {
      if (isset($definition[$key])) {
        return $mapper->argDefinitionGetConfigurator($definition[$key], $definition, $context);
      }
    }

    return NULL;
  }

}
