<?php

namespace Drupal\cfrfamily\CfrLegendItem;

use Drupal\cfrapi\LegendItem\LegendItem;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Represents a legend item with no further configuration options.
 */
class CfrLegendItem_NoConf extends LegendItem implements CfrLegendItemInterface {

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isOptionless() {
    return TRUE;
  }

}
