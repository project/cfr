<?php

namespace Drupal\cfrfamily\CfrLegendItem;

use Drupal\cfrfamily\CfrLegendProvider\CfrLegendProviderInterface;

/**
 * Recursive drilldown option.
 *
 * This is a drilldown legend item where the nested configurator is another
 * drilldown or select, with nested options that can be inlined into the parent
 * drilldown.
 *
 * Inherited methods:
 * - From CfrLegendItemInterface: Default methods for a drilldown legend item.
 * - From CfrLegendProviderInterface: Provides the nested drilldown legend.
 */
interface ParentLegendItemInterface extends CfrLegendItemInterface, CfrLegendProviderInterface {

  /**
   * Extracts id and options from the configuration for a drilldown.
   *
   * @param mixed $conf
   *   Configuration from which to extract id and child options.
   *   For a typical drilldown, this will be an array with keys for id and
   *   child options.
   *
   * @return array
   *   Id and child options extracted from the configuration.
   *   Format: One of..
   *   - array($id, $optionsConf)
   *   - array($id, null),
   *     if there are no child options.
   *   - array(null, null),
   *     if the configuration did not have the expected format.
   *
   * @see \Drupal\cfrapi\Util\ConfUtil::confGetIdOptions()
   */
  public function confGetIdOptions($conf);

}
