<?php

namespace Drupal\cfrfamily\CfrLegendItem;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\LegendItem\LegendItem;
use Drupal\cfrapi\PossiblyOptionless\PossiblyOptionlessInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Default implementation.
 *
 * Uses functionality from a regular select legend item, but with an additional
 * configurator object.
 */
class CfrLegendItem extends LegendItem implements CfrLegendItemInterface {

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private $configurator;

  /**
   * Constructor.
   *
   * @param string $label
   *   Label for the legend item.
   * @param string $groupLabel
   *   Optgroup label for the legend item.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator for this legend item.
   */
  public function __construct($label, $groupLabel, ConfiguratorInterface $configurator) {
    parent::__construct($label, $groupLabel);
    $this->configurator = $configurator;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {
    return $this->configurator->confGetForm($conf, $label);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {
    return $this->configurator->confGetSummary($conf, $summaryBuilder);
  }

  /**
   * {@inheritdoc}
   */
  public function withLabels($label, $groupLabel) {
    return new self($label, $groupLabel, $this->configurator);
  }

  /**
   * {@inheritdoc}
   */
  public function isOptionless() {
    return $this->configurator instanceof PossiblyOptionlessInterface
      ? $this->configurator->isOptionless()
      : FALSE;
  }

}
