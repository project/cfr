<?php

namespace Drupal\cfrfamily\CfrLegendItem;

use Drupal\cfrapi\LegendItem\LegendItemInterface;
use Drupal\cfrapi\PossiblyOptionless\PossiblyOptionlessInterface;
use Drupal\cfrapi\RawConfigurator\RawConfiguratorInterface;

/**
 * Represents an option in a drilldown element.
 *
 * Inherited methods:
 * - Methods from LegendItemInterface produce the select option at the top.
 * - Methods from RawConfiguratorInterface produce the nested form element and
 *   summary, if this option is chosen in the drilldown.
 * - The method from PossiblyOptionlessInterface determines whether or not '..'
 *   should be appended to the option label.
 */
interface CfrLegendItemInterface extends LegendItemInterface, RawConfiguratorInterface, PossiblyOptionlessInterface {

}
