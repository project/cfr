<?php

namespace Drupal\cfrfamily\CfrLegendItem;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\Util\ConfUtil;
use Drupal\cfrfamily\CfrLegend\CfrLegendInterface;

/**
 * Drilldown legend item with child options that can be inlined.
 */
class CfrLegendItem_Parent extends CfrLegendItem implements ParentLegendItemInterface {

  /**
   * @var \Drupal\cfrfamily\CfrLegend\CfrLegendInterface
   */
  private $legend;

  /**
   * Constructor.
   *
   * @param string $label
   *   Label for this legend item.
   * @param string $groupLabel
   *   Optgroup label for this legend item.
   * @param \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
   *   Configurator for this legend item.
   * @param \Drupal\cfrfamily\CfrLegend\CfrLegendInterface $legend
   *   Inner legend, for child options to be inlined.
   */
  public function __construct($label, $groupLabel, ConfiguratorInterface $configurator, CfrLegendInterface $legend) {
    parent::__construct($label, $groupLabel, $configurator);
    $this->legend = $legend;
  }

  /**
   * {@inheritdoc}
   */
  public function getCfrLegend() {
    return $this->legend;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetIdOptions($conf) {
    return ConfUtil::confGetIdOptions($conf);
  }

}
