<?php

namespace Drupal\cfrfamily\IdConfLegend;

use Drupal\cfrapi\Legend\LegendInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Legend for drilldown configurators.
 *
 * @deprecated
 *   This is not used and will be removed asap.
 *   See https://www.drupal.org/project/cfr/issues/3165296.
 *
 * @todo idConfGetPhp() is missing!
 */
interface IdConfLegendInterface extends LegendInterface {

  /**
   * Gets a nested form for a chosen drilldown option.
   *
   * @param string $id
   *   Drilldown option id.
   * @param mixed $optionsConf
   *   Configuration to send to the inner configurator.
   *
   * @return array|null
   *   Form element.
   */
  public function idConfGetOptionsForm($id, $optionsConf);

  /**
   * Gets a summary for a given id + options conf.
   *
   * @param string $id
   *   Drilldown option id.
   * @param mixed $optionsConf
   *   Configuration to send to the inner configurator.
   * @param \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface $summaryBuilder
   *   Helper object for summary-building.
   *
   * @return string|null
   *   Summary.
   */
  public function idConfGetSummary($id, $optionsConf, SummaryBuilderInterface $summaryBuilder);

  /**
   * Gets a value for a given id + options conf.
   *
   * @param string $id
   *   Drilldown option id.
   * @param mixed $optionsConf
   *   Configuration to send to the inner configurator.
   *
   * @return mixed
   *   Value to use in the application.
   */
  public function idConfGetValue($id, $optionsConf);

}
