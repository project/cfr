<?php

namespace Drupal\cfrfamily\Configurator\Inlineable;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\Util\ConfUtil;

/**
 * Base class for 'inlineable' configurators.
 */
abstract class InlineableConfiguratorBase implements InlineableConfiguratorInterface {

  /**
   * @var string
   */
  private $idKey = 'id';

  /**
   * @var string
   */
  private $optionsKey = 'options';

  /**
   * {@inheritdoc}
   */
  final public function confGetValue($conf) {
    list($id, $conf) = $this->confGetIdOptions($conf);

    if (NULL === $id) {
      throw new ConfToValueException("Id is required.");
    }

    return $this->idConfGetValue($id, $conf);
  }

  /**
   * {@inheritdoc}
   */
  final public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    list($id, $optionsConf) = $this->confGetIdOptions($conf);

    if (NULL === $id) {
      return $helper->incompatibleConfiguration($conf, "Required id missing.");
    }

    return $this->idConfGetPhp($id, $optionsConf, $helper);
  }

  /**
   * Splits drilldown configuration into id and options.
   *
   * @param mixed $conf
   *   Original configuration.
   *   If valid, the format is [$idKey => $id, $optionsKey => $optionsConf].
   *
   * @return array
   *   Configuration split in id vs options.
   *   Format: [$id, $optionsConf], OR [$id, NULL], OR [NULL, NULL].
   */
  protected function confGetIdOptions($conf) {
    return ConfUtil::confGetIdOptions($conf, $this->idKey, $this->optionsKey);
  }

}
