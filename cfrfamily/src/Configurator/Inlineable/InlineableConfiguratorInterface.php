<?php

namespace Drupal\cfrfamily\Configurator\Inlineable;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrfamily\CfrLegendProvider\CfrLegendProviderInterface;
use Drupal\cfrfamily\IdConfToValue\IdConfToValueInterface;

/**
 * Configurator with options that can be "inlined" into a parent drilldown.
 *
 * Inherited methods:
 * - ConfiguratorInterface::*()
 *   Regular methods for a configurator.
 * - CfrLegendProviderInterface::getCfrLegend()
 *   Provides the drilldown legend that powers the configurator.
 * - IdConfToValueInterface::*()
 *   Produces a combined value from the drilldown configuration.
 */
interface InlineableConfiguratorInterface extends ConfiguratorInterface, CfrLegendProviderInterface, IdConfToValueInterface {

}
