<?php

/** @noinspection PhpDeprecationInspection */

namespace Drupal\cfrfamily\Configurator\Composite;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\ConfEmptyness\ConfEmptyness_Key;
use Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;
use Drupal\cfrapi\Util\FormUtil;
use Drupal\cfrfamily\IdValueToValue\IdValueToValue;
use Drupal\cfrfamily\IdValueToValue\IdValueToValueInterface;

/**
 * Base class for drilldown configurators.
 */
abstract class Configurator_IdConfGrandBase implements OptionalConfiguratorInterface, IdValueToValueInterface {

  /**
   * @var bool
   */
  private $required;

  /**
   * @var mixed|null
   */
  private $defaultValue;

  /**
   * @var string|null
   */
  private $idLabel;

  /**
   * @var string
   */
  private $idKey;

  /**
   * @var string
   */
  private $optionsKey;

  /**
   * @var \Drupal\cfrfamily\IdValueToValue\IdValueToValueInterface|null
   */
  private $idValueToValue;

  /**
   * @var callable|null
   */
  private $formProcessCallback;

  /**
   * Constructor.
   *
   * @param bool $required
   *   TRUE if this should be required.
   * @param \Drupal\cfrfamily\IdValueToValue\IdValueToValueInterface|null $idValueToValue
   *   (optional) Object to combine the value from ->confGetValue() from the
   *   nested configurator with the id that was chosen.
   *   If NULL, the value from the nested configurator will be returned as-is.
   * @param string $idKey
   *   Array key for the id.
   * @param string $optionsKey
   *   Array key for the nested configuration.
   */
  public function __construct($required, IdValueToValueInterface $idValueToValue = NULL, $idKey = 'id', $optionsKey = 'options') {
    $this->required = $required;
    $this->idValueToValue = $idValueToValue;
    $this->idKey = $idKey;
    $this->optionsKey = $optionsKey;
  }

  /**
   * Immutable setter. Sets the array keys for id and option.
   *
   * @param string $idKey
   *   Array key for the id.
   * @param string $optionsKey
   *   Array key for the nested configuration.
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withKeys($idKey, $optionsKey) {
    $clone = clone $this;
    $clone->idKey = $idKey;
    $clone->optionsKey = $optionsKey;
    return $clone;
  }

  /**
   * Immutable setter. Sets a fallback label for the select element.
   *
   * @param string $idLabel
   *   Fallback label for the select element.
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withIdLabel($idLabel) {
    $clone = clone $this;
    $clone->idLabel = $idLabel;
    return $clone;
  }

  /**
   * Immutable setter. Makes this optional, and sets a default value.
   *
   * @param mixed $defaultValue
   *   Default value.
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withDefaultValue($defaultValue = NULL) {
    $clone = clone $this;
    $clone->required = FALSE;
    $clone->defaultValue = $defaultValue;
    return $clone;
  }

  /**
   * Immutable setter. Sets a processor for the returned value.
   *
   * @param \Drupal\cfrfamily\IdValueToValue\IdValueToValueInterface $idValueToValue
   *   Processor to combine the id and the nested value.
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withIdValueToValue(IdValueToValueInterface $idValueToValue) {
    $clone = clone $this;
    $clone->idValueToValue = $idValueToValue;
    return $clone;
  }

  /**
   * Immutable setter. Sets $this as a processor for the returned value.
   *
   * The resulting value will be like [$idKey => $id, $optionsKey => $value].
   *
   * @return static
   *   Cloned, modified instance.
   *
   * @see idValueGetValue()
   */
  public function withIdValueRepackaging() {
    return $this->withIdValueToValue($this);
  }

  /**
   * Immutable setter. Passes the id + value to a class constructor.
   *
   * @param string $class
   *   Class name. Constructor signature: ($id, $value).
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withValueConstructor($class) {
    return $this->withIdValueToValue(
      IdValueToValue::fromClassConstructor($class));
  }

  /**
   * Immutable setter. Passes the id + value to a factory callback.
   *
   * @param callable $factory
   *   Factory callback. Signature: ($id, $value) -> $value.
   *   Cannot be a closure.
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withValueFactory($factory) {
    return $this->withIdValueToValue(
      IdValueToValue::fromCallable($factory));
  }

  /**
   * Immutable setter. Sets a processor for the returned value.
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withoutIdValueToValue() {
    $clone = clone $this;
    $clone->idValueToValue = NULL;
    return $clone;
  }

  /**
   * Immutable setter. Adds a callback to process the form element.
   *
   * @param callable $formProcessCallback
   *   Callback to call at the end of ->confGetForm().
   *
   * @return static
   *   Cloned, modified instance.
   */
  public function withFormProcessCallback($formProcessCallback) {
    $clone = clone $this;
    $clone->formProcessCallback = $formProcessCallback;
    return $clone;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetForm($conf, $label) {

    list($id, $optionsConf) = $this->confGetIdOptions($conf);

    $form = [
      '#type' => 'themekit_container',
      '#tree' => TRUE,
      $this->idKey => $this->idBuildSelectElement($id, $label),
      '#input' => FALSE,
      '#process' => [function (array $element, array &$form_state) use ($id, $optionsConf) {
        $element = $this->processElement($element, $form_state, $id, $optionsConf);
        return $element;
      }],
      '#after_build' => [function (array $element, array &$form_state) {
        return $this->elementAfterBuild($element, $form_state);
      }],
      '#attributes' => ['class' => ['cfr-drilldown']],
    ];

    if (NULL !== $this->formProcessCallback) {
      $form = \call_user_func($this->formProcessCallback, $form);
    }

    return $form;
  }

  /**
   * Called from a form element '#process' callback.
   *
   * @param array $element
   *   Original form element.
   * @param array $form_state
   *   Form state.
   * @param string $defaultId
   *   Default value for the id.
   * @param mixed $defaultOptionsConf
   *   Default value for the options.
   *
   * @return array
   *   Processed form element.
   */
  private function processElement(array $element, array &$form_state, $defaultId, $defaultOptionsConf) {
    $input_ref =& drupal_array_get_nested_value($form_state['input'], $element['#parents'], $input_exists);
    $id = '';
    $prev_id = '';
    if (empty($form_state['input'])) {
      // Initial form build.
      $id = $defaultId;
      $prev_id = $defaultId;
    }
    elseif (!$input_exists) {
      // Form submission or AJAX request, but this element was newly added.
      drupal_array_set_nested_value($form_state['input'], $element['#parents'], [$this->idKey => NULL]);
    }
    else {
      // Form submission or AJAX request.
      if (isset($input_ref[$this->idKey])) {
        $id_from_input = $input_ref[$this->idKey];
        if (is_string($id_from_input) || is_int($id_from_input)) {
          $id = $id_from_input;
        }
      }
      else {
        $input_ref[$this->idKey] = '';
      }
      if (isset($input_ref['_previous_id'])) {
        $prev_id_from_input = $input_ref['_previous_id'];
        if (is_string($prev_id_from_input) || is_int($prev_id_from_input)) {
          $prev_id = $prev_id_from_input;
        }
      }
    }

    $element['_previous_id'] = [
      '#type' => 'hidden',
      '#value' => $id,
    ];

    if ('' !== $prev_id && (string) $id !== (string) $prev_id) {
      // Don't let values leak from one plugin to the other.
      unset($input_ref[$this->optionsKey]);
    }

    if ((string) $id !== (string) $defaultId) {
      $defaultOptionsConf = NULL;
    }

    $element[$this->optionsKey] = $this->idConfGetOptionsForm($id, $defaultOptionsConf);

    $form_build_id = $form_state['complete form']['#build_id'];
    $uniqid = sha1($form_build_id . serialize($element['#parents']));

    $element[$this->idKey]['#ajax'] = [
      'method' => 'replace',
      'callback' => FormUtil::f_findNestedElement('#cfr_ajax_id', $uniqid),
      'wrapper' => $uniqid,
    ];

    // Special handling of ajax for views.
    /* @see views_ui_edit_form() */
    // See https://www.drupal.org/node/1183418
    // This has to be added here, because it depends on the form state.
    if (1
      && isset($form_state['view'])
      && module_exists('views_ui')
      && $form_state['view'] instanceof \view
    ) {
      // @todo Does this always work?
      $element[$this->idKey]['#ajax']['path'] = empty($form_state['url'])
        ? url($_GET['q'], ['absolute' => TRUE])
        : $form_state['url'];
    }

    return $element;
  }

  /**
   * Called from a form element '#after_build' callback.
   *
   * @param array $element
   *   Original form element.
   * @param array $form_state
   *   Form state.
   *
   * @return array
   *   Processed form element.
   */
  private function elementAfterBuild(array $element, array &$form_state) {

    // Remove possible junk from the value.
    $value = drupal_array_get_nested_value($form_state['values'], $element['#parents']);
    $reduced_value = [];
    if (isset($value[$this->idKey])) {
      $reduced_value[$this->idKey] = $value[$this->idKey];
    }
    if (isset($value[$this->optionsKey])) {
      $reduced_value[$this->optionsKey] = $value[$this->optionsKey];
    }
    drupal_array_set_nested_value($form_state['values'], $element['#parents'], $reduced_value);

    // Add wrappers around the options element.
    $options_element = $element[$this->optionsKey];

    $element[$this->optionsKey] = [
      '#type' => 'themekit_container',
      '#attributes' => ['class' => ['cfrapi-child-options']],
    ];

    if (isset($options_element['#type'])
      || isset($options_element['#markup'])
      || isset($options_element['#theme'])
    ) {
      $child_elements = [$options_element];
    }
    elseif ($options_child_keys = element_children($options_element)) {
      $child_elements = [];
      foreach ($options_child_keys as $k) {
        $child_elements[$k] = $options_element[$k];
      }
    }
    else {
      $child_elements = [];
    }

    if (!$child_elements) {
      $element[$this->optionsKey]['#attributes'] = [];
    }
    else {
      foreach ($child_elements as $k => $child_element) {
        $element[$this->optionsKey][$k] = [
          '#type' => 'themekit_container',
          'content' => $child_element,
          '#attributes' => [
            'class' => ['cfrapi-child-option'],
            'data:child-option-name' => $k,
          ],
          'decoration' => [
            '#weight' => 100,
            '#markup' => '<div class="__decoration"></div>',
          ],
        ];
      }
    }

    // Add the ajax wrapper.
    $uniqid = $element[$this->idKey]['#ajax']['wrapper'];

    $element[$this->optionsKey] = [
      '#type' => 'themekit_container',
      '#attributes' => [
        'class' => ['cfr-drilldown-ajax-wrapper'],
        'id' => $uniqid,
      ],
      // Use a key that won't clash with existing keys.
      '#cfr_ajax_id' => $uniqid,
      'options' => $element[$this->optionsKey],
      '_previous_id' => $element['_previous_id'],
    ];

    unset($element['_previous_id']);

    // Set an element to return with ajax.
    if (!empty($form_state['input']['_triggering_element_name'])
      && $element[$this->idKey]['#name'] === $form_state['input']['_triggering_element_name']
    ) {
      // Skip the form rebuild, because we don't need it.
      $form_state['executed'] = TRUE;
    }

    // Add wrapper around the select element.
    // This must happen after the ajax binding.
    $element[$this->idKey] = [
      '#type' => 'themekit_container',
      '#attributes' => ['class' => ['cfr-drilldown-id']],
      'content' => $element[$this->idKey],
    ];

    return $element;
  }

  /**
   * Builds the select element to choose the id.
   *
   * @param string|int $id
   *   Default value for the id.
   * @param string|null $label
   *   Label for the select element.
   *
   * @return array
   *   Form element.
   */
  private function idBuildSelectElement($id, $label) {

    $element = [
      '#title' => ($label !== NULL) ? $label : $this->idLabel,
      '#type' => 'select',
      '#options' => $this->getSelectOptions(),
      '#default_value' => $id,
      '#attributes' => ['class' => ['cfr-drilldown-select']],
    ];

    if (NULL !== $id && !self::idExistsInSelectOptions($id, $element['#options'])) {
      $element['#options'][$id] = t("Unknown id '@id'", ['@id' => $id]);
      $element['#element_validate'][] = static function (array $element) use ($id) {
        if ((string) $id === (string) $element['#value']) {
          form_error($element, t("Unknown id %id. Maybe the id did exist in the past, but it currently does not.", ['%id' => $id]));
        }
      };
    }

    if ($this->required) {
      $element['#required'] = TRUE;
    }

    $element['#empty_value'] = '';

    return $element;
  }

  /**
   * Checks if a key exists in the options array.
   *
   * This is static, because it does not use any object properties.
   *
   * @param string $id
   *   Key to find.
   * @param array $options
   *   Options array in the format as for a 'select' element.
   *   Format: A mix of:
   *   - $[$optgroup_label][$id] = $option_label.
   *   - $[$id] = $option_label.
   *
   * @return bool
   *   TRUE, if the id exists in the options.
   */
  private static function idExistsInSelectOptions($id, $options) {

    if (isset($options[$id]) && !\is_array($options[$id])) {
      return TRUE;
    }

    foreach ($options as $optgroup) {
      if (\is_array($optgroup) && isset($optgroup[$id])) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function confGetSummary($conf, SummaryBuilderInterface $summaryBuilder) {

    list($id, $optionsConf) = $this->confGetIdOptions($conf);

    return $this->idConfGetSummary($id, $optionsConf, $summaryBuilder);
  }

  /**
   * Gets a summary to show if the configuration is empty.
   *
   * This seems to be not really used.
   *
   * @return string
   *   Summary to show if the configuration is empty.
   *
   * @deprecated
   *   This is a leftover from the time when OptionableInterface existed.
   */
  public function getEmptySummary() {
    return t('None');
  }

  /**
   * {@inheritdoc}
   */
  public function confGetValue($conf) {

    list($id, $optionsConf) = $this->confGetIdOptions($conf);

    if (NULL === $id) {
      if ($this->required) {
        throw new ConfToValueException("Required id missing.");
      }

      return $this->defaultValue;
    }

    $value = $this->idConfGetValue($id, $optionsConf);

    if (NULL === $this->idValueToValue) {
      return $value;
    }

    return $this->idValueToValue->idValueGetValue($id, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    list($id, $optionsConf) = $this->confGetIdOptions($conf);

    if (NULL === $id) {
      if ($this->required) {
        return $helper->incompatibleConfiguration($conf, "Required id missing.");
      }

      return $helper->export($this->defaultValue);
    }

    $php = $this->idConfGetPhp($id, $optionsConf, $helper);

    if (NULL === $this->idValueToValue) {
      return $php;
    }

    return $this->idValueToValue->idPhpGetPhp($id, $php, $helper);
  }

  /**
   * Gets the default value.
   *
   * @return mixed
   *   The default value.
   *
   * @deprecated
   *   This is a leftover from the time when OptionableInterface existed.
   */
  public function getEmptyValue() {
    return $this->defaultValue;
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated
   *   The method will be removed in 7.x-3.x.
   *   See https://www.drupal.org/project/cfr/issues/3165150.
   */
  public function getEmptyness() {
    return $this->required
      ? NULL
      : new ConfEmptyness_Key($this->idKey);
  }

  /**
   * {@inheritdoc}
   */
  public function idValueGetValue($id, $value) {
    return [$this->idKey => $id, $this->optionsKey => $value];
  }

  /**
   * {@inheritdoc}
   */
  public function idPhpGetPhp($id, $php, CfrCodegenHelperInterface $helper) {

    return '['
    . "\n  " . var_export($this->idKey, TRUE) . ' => ' . var_export($id, TRUE) . ','
    . "\n  " . var_export($this->optionsKey, TRUE) . ' => ' . $php . ','
    . "\n]";
  }

  /**
   * Splits configuration data into $id and $options.
   *
   * @param mixed $conf
   *   Configuration data.
   *   If valid, this has the following structure:
   *   - [$idKey => $id, $optionsKey => $options], OR
   *   - [$idKey => $id], if the inner configurator has no options.
   *
   * @return array
   *   Array with id and options.
   *   Format:
   *   - [$id, $options], OR
   *   - [$id, NULL], if the options don't exist or cannot be determined, OR
   *   - [NULL, NULL], if the id cannot be determined.
   */
  private function confGetIdOptions($conf) {

    if (!\is_array($conf)) {
      return [NULL, NULL];
    }

    if (!isset($conf[$this->idKey])) {
      return [NULL, NULL];
    }

    if ('' === $id = $conf[$this->idKey]) {
      return [NULL, NULL];
    }

    if (!\is_string($id) && !\is_int($id)) {
      return [NULL, NULL];
    }

    if (!isset($conf[$this->optionsKey])) {
      return [$id, NULL];
    }

    return [$id, $conf[$this->optionsKey]];
  }

  /**
   * Gets options for the drilldown select.
   *
   * @return string[]|string[][]|mixed[]
   *   Options suitable for '#options' in a '#type' => 'select'.
   *   Format (mixed):
   *   - $[$optgroup_label][$id] = $label
   *   - $[$id] = $label
   */
  abstract protected function getSelectOptions();

  /**
   * Gets a label for a given id.
   *
   * @param string $id
   *   The id.
   *
   * @return string|null
   *   The label, or the id itself or NULL if no label exists.
   */
  abstract protected function idGetLabel($id);

  /**
   * Gets the nested form element for a given drilldown id + child options.
   *
   * @param string $id
   *   Drilldown id that was chosen.
   * @param mixed $optionsConf
   *   Configuration for the nested configurator.
   *
   * @return array|null
   *   Form element(s) for the option sub-form.
   */
  abstract protected function idConfGetOptionsForm($id, $optionsConf);

  /**
   * Gets a summary for the given id + options configuration.
   *
   * @param string $id
   *   The id.
   * @param mixed $optionsConf
   *   The options configuration.
   * @param \Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface $summaryBuilder
   *   Helper object for summary-building.
   *
   * @return string|null
   *   The summary, typically a string of html.
   */
  abstract protected function idConfGetSummary($id, $optionsConf, SummaryBuilderInterface $summaryBuilder);

  /**
   * Gets a value for a given id + options configuration.
   *
   * @param string $id
   *   The id.
   * @param mixed $optionsConf
   *   The options configuration.
   *
   * @return mixed
   *   The value or object instance.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   *   If no value can be produced for the given configuration.
   */
  abstract protected function idConfGetValue($id, $optionsConf);

  /**
   * Gets PHP code to generate the value.
   *
   * @param string $id
   *   The id.
   * @param mixed $optionsConf
   *   The options configuration.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *   Helper for code generation.
   *
   * @return string
   *   Generated PHP expression.
   */
  abstract protected function idConfGetPhp($id, $optionsConf, CfrCodegenHelperInterface $helper);

}
