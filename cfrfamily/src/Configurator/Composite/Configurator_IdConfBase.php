<?php

namespace Drupal\cfrfamily\Configurator\Composite;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;

/**
 * Base class for drilldown configurators with ->idGetConfigurator() method.
 */
abstract class Configurator_IdConfBase extends Configurator_IdConfGrandBase {

  /**
   * {@inheritdoc}
   */
  protected function idConfGetOptionsForm($id, $optionsConf) {

    if (NULL === $configurator = $this->idGetConfigurator($id)) {
      return NULL;
    }

    return $configurator->confGetForm($optionsConf, $this->idGetOptionsFormLabel($id));
  }

  /**
   * Gets a label for the options form, depending on the chosen id.
   *
   * @param string $id
   *   Id of the chosen drilldown option.
   *
   * @return string|null
   *   Label for the options form, or NULL to show no label.
   */
  protected function idGetOptionsFormLabel($id) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function idConfGetSummary($id, $optionsConf, SummaryBuilderInterface $summaryBuilder) {

    $idLabel = $this->idGetLabel($id);

    if (NULL === $id or NULL === $configurator = $this->idGetConfigurator($id)) {
      return $idLabel;
    }

    return $summaryBuilder->idConf($idLabel, $configurator, $optionsConf);
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetValue($id, $optionsConf) {

    if (!$configurator = $this->idGetConfigurator($id)) {
      throw new ConfToValueException("Unknown id '$id'.");
    }

    return $configurator->confGetValue($optionsConf);
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetPhp($id, $conf, CfrCodegenHelperInterface $helper) {

    if (!$configurator = $this->idGetConfigurator($id)) {
      return $helper->incompatibleConfiguration($id, "Unknown id.");
    }

    return $configurator->confGetPhp($conf, $helper);
  }

  /**
   * Gets a configurator for a given id.
   *
   * @param string $id
   *   The id.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface|null
   *   Configurator for the id, or NULL if not found.
   */
  abstract protected function idGetConfigurator($id);

}
