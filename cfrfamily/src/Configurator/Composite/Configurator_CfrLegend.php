<?php

namespace Drupal\cfrfamily\Configurator\Composite;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\SummaryBuilder\SummaryBuilderInterface;
use Drupal\cfrfamily\CfrLegend\CfrLegendInterface;
use Drupal\cfrfamily\Configurator\Inlineable\InlineableConfiguratorInterface;
use Drupal\cfrfamily\IdConfToValue\IdConfToValueInterface;

/**
 * Drilldown configurator using a drilldown legend.
 */
class Configurator_CfrLegend extends Configurator_IdConfGrandBase implements InlineableConfiguratorInterface {

  /**
   * @var \Drupal\cfrfamily\CfrLegend\CfrLegendInterface
   */
  private $legend;

  /**
   * @var \Drupal\cfrfamily\IdConfToValue\IdConfToValueInterface
   */
  private $idConfToValue;

  /**
   * Constructor.
   *
   * @param bool $required
   *   TRUE, to make this required.
   * @param \Drupal\cfrfamily\CfrLegend\CfrLegendInterface $legend
   *   Drilldown legend.
   * @param \Drupal\cfrfamily\IdConfToValue\IdConfToValueInterface $idConfToValue
   *   Object to generate a value from id + options.
   */
  public function __construct(
    $required,
    CfrLegendInterface $legend,
    IdConfToValueInterface $idConfToValue
  ) {
    parent::__construct($required);
    $this->legend = $legend;
    $this->idConfToValue = $idConfToValue;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSelectOptions() {

    $options = [];
    $groups = [];
    foreach ($this->legend->getLegendItems() as $id => $item) {
      $itemLabel = $item->getLabel();
      if (!$item->isOptionless()) {
        $itemLabel .= '…';
      }
      if (NULL === $groupLabel = $item->getGroupLabel()) {
        $options[$id] = $itemLabel;
      }
      else {
        $groups[$groupLabel][$id] = $itemLabel;
      }
    }

    asort($options);
    ksort($groups);
    foreach ($groups as &$group) {
      asort($group);
    }

    return $options + $groups;
  }

  /**
   * {@inheritdoc}
   */
  protected function idGetLabel($id) {

    if (NULL === $item = $this->legend->idGetLegendItem($id)) {
      return $id;
    }

    return $item->getLabel();
  }

  /**
   * {@inheritdoc}
   */
  protected function idConfGetOptionsForm($id, $optionsConf) {

    if (NULL === $item = $this->legend->idGetLegendItem($id)) {
      return NULL;
    }

    return $item->confGetForm($optionsConf, NULL);
  }

  /**
   * {@inheritdoc}
   */
  protected function idConfGetSummary($id, $optionsConf, SummaryBuilderInterface $summaryBuilder) {

    if (NULL === $legendItem = $this->legend->idGetLegendItem($id)) {
      return '- ' . t('Unknown') . ' -';
    }

    $idLabel = $legendItem->getLabel();

    return $summaryBuilder->idConf($idLabel, $legendItem, $optionsConf);
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetValue($id, $optionsConf) {
    return $this->idConfToValue->idConfGetValue($id, $optionsConf);
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetPhp($id, $conf, CfrCodegenHelperInterface $helper) {
    return $this->idConfToValue->idConfGetPhp($id, $conf, $helper);
  }

  /**
   * {@inheritdoc}
   */
  public function getCfrLegend() {
    return $this->legend;
  }

}
