<?php

namespace Drupal\cfrfamily\Configurator\Composite;

use Drupal\cfrapi\Legend\LegendInterface;
use Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface;

/**
 * Drilldown configurator using a legend and an IdToConfigurator* object.
 */
class Configurator_IdConf extends Configurator_IdConfBase {

  /**
   * @var \Drupal\cfrapi\Legend\LegendInterface
   */
  private $legend;

  /**
   * @var \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface
   */
  private $idToConfigurator;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrapi\Legend\LegendInterface $legend
   *   Legend with options for the id.
   * @param \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface $idToConfigurator
   *   Object to get the inner configurator for a chosen drilldown id.
   */
  public function __construct(LegendInterface $legend, IdToConfiguratorInterface $idToConfigurator) {
    $this->legend = $legend;
    $this->idToConfigurator = $idToConfigurator;
    parent::__construct(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  protected function getSelectOptions() {
    return $this->legend->getSelectOptions();
  }

  /**
   * {@inheritdoc}
   */
  protected function idGetLabel($id) {
    return $this->legend->idGetLabel($id);
  }

  /**
   * {@inheritdoc}
   */
  protected function idGetConfigurator($id) {
    return $this->idToConfigurator->idGetConfigurator($id);
  }

}
