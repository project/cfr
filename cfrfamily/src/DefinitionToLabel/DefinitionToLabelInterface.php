<?php

namespace Drupal\cfrfamily\DefinitionToLabel;

/**
 * Object to get a label or group label from a plugin definition array.
 */
interface DefinitionToLabelInterface {

  /**
   * Gets a label from a definition array.
   *
   * This can also be used as an optgroup label.
   *
   * @param array $definition
   *   The definition array, e.g. from hook_cfrplugin_info().
   * @param string|null $else
   *   Fallback label. Typically the plugin id.
   *
   * @return string
   *   The raw label, not safe for html.
   */
  public function definitionGetLabel(array $definition, $else);

}
