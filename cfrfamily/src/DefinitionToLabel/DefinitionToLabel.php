<?php

namespace Drupal\cfrfamily\DefinitionToLabel;

/**
 * Implementation that gets the label from a key in the definition array.
 */
class DefinitionToLabel implements DefinitionToLabelInterface {

  /**
   * @var string
   */
  private $key;

  /**
   * Static factory. Finds the label in $definition['label'].
   *
   * @return \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabel
   *   Created instance.
   */
  public static function create() {
    return new self('label');
  }

  /**
   * Static factory. Finds the label in $definition['group_label'].
   *
   * @return \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabel
   *   Created instance.
   */
  public static function createGroupLabel() {
    return new self('group_label');
  }

  /**
   * Constructor.
   *
   * @param string $key
   *   Key in the definition array that should contain the label.
   *   E.g. 'label'.
   */
  public function __construct($key) {
    $this->key = $key;
  }

  /**
   * {@inheritdoc}
   */
  public function definitionGetLabel(array $definition, $else) {
    return isset($definition[$this->key])
      ? $definition[$this->key]
      : $else;
  }

}
