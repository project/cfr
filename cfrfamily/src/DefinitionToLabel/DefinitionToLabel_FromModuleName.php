<?php

namespace Drupal\cfrfamily\DefinitionToLabel;

/**
 * Implementation to get optgroup labels based on module names.
 */
class DefinitionToLabel_FromModuleName implements DefinitionToLabelInterface {

  /**
   * @var string[]
   */
  private $labelsByModule = [];

  /**
   * @var array[]|null
   */
  private $modulesInfo;

  /**
   * {@inheritdoc}
   */
  public function definitionGetLabel(array $definition, $else) {
    if (isset($definition['group_label'])) {
      return $definition['group_label'];
    }
    return isset($definition['module'])
      ? $this->moduleGetLabel($definition['module'])
      : $else;
  }

  /**
   * Gets the human name for a module.
   *
   * This uses a buffer variable, the actual lookup happens in another method.
   *
   * @param string $module
   *   Module machine name.
   *
   * @return string
   *   Module human name, or the machine name if not found.
   */
  private function moduleGetLabel($module) {
    return array_key_exists($module, $this->labelsByModule)
      ? $this->labelsByModule[$module]
      : $this->labelsByModule[$module] = $this->moduleFindLabel($module);
  }

  /**
   * Finds the human name for a module.
   *
   * This method does the actual lookup.
   *
   * @param string $module
   *   Module machine name.
   *
   * @return string
   *   Module human name, or the machine name if not found.
   */
  private function moduleFindLabel($module) {
    if (NULL === $this->modulesInfo) {
      $this->modulesInfo = system_get_info('module_enabled');
    }
    return isset($this->modulesInfo[$module]['name'])
      ? $this->modulesInfo[$module]['name']
      : $module;
  }

}
