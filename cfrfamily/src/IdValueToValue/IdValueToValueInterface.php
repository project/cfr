<?php

namespace Drupal\cfrfamily\IdValueToValue;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * Object to produce a combined value from a drilldown id + nested value.
 */
interface IdValueToValueInterface {

  /**
   * Gets a combined value from a drilldown id + nested value.
   *
   * @param string $id
   *   The id from the drilldown.
   * @param mixed $value
   *   Value from the nested configurator.
   *   E.g. $this->idGetConfigurator($id)->confGetValue($conf).
   *
   * @return mixed
   *   Transformed or combined value.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   */
  public function idValueGetValue($id, $value);

  /**
   * Gets PHP code to produce the combined value.
   *
   * @param string $id
   *   The id from the drilldown.
   * @param string $php
   *   PHP code to generate the value from the nested configurator.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *   Helper for code generation.
   *
   * @return string
   *   PHP code which, if executed, will either return the combined value, or
   *   throw an exception.
   */
  public function idPhpGetPhp($id, $php, CfrCodegenHelperInterface $helper);

}
