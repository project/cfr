<?php

namespace Drupal\cfrfamily\IdValueToValue;

use Donquixote\CallbackReflection\Callback\CallbackReflection_ClassConstruction;
use Donquixote\CallbackReflection\Util\CallbackUtil;
use Drupal\cfrapi\Util\UtilBase;

/**
 * Utility class to create IdValueToValue objects.
 */
final class IdValueToValue extends UtilBase {

  /**
   * Static factory.
   *
   * @param string $class
   *   Class name. Constructor signature: ($id, $value).
   *
   * @return \Drupal\cfrfamily\IdValueToValue\IdValueToValueInterface
   *   Object to produce a combined value from a drilldown id + nested value.
   */
  public static function fromClassConstructor($class) {
    return new IdValueToValue_Callback(
      CallbackReflection_ClassConstruction::createFromClassName($class));
  }

  /**
   * Static factory.
   *
   * @param callable $callback
   *   Callback. Signature: ($id, $value) -> $value.
   *
   * @return \Drupal\cfrfamily\IdValueToValue\IdValueToValueInterface
   *   Object to produce a combined value from a drilldown id + nested value.
   */
  public static function fromCallable($callback) {
    return new IdValueToValue_Callback(
      CallbackUtil::callableGetCallback($callback));
  }

}
