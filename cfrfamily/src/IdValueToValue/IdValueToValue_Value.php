<?php

namespace Drupal\cfrfamily\IdValueToValue;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * Implementation which uses the value from the nested configurator.
 */
class IdValueToValue_Value implements IdValueToValueInterface {

  /**
   * {@inheritdoc}
   */
  public function idValueGetValue($id, $value) {
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function idPhpGetPhp($id, $php, CfrCodegenHelperInterface $helper) {
    return $php;
  }

}
