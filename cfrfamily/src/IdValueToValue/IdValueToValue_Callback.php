<?php

namespace Drupal\cfrfamily\IdValueToValue;

use Donquixote\CallbackReflection\Callback\CallbackReflectionInterface;
use Donquixote\CallbackReflection\Util\CallbackUtil;
use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Exception\ConfToValueException;

/**
 * Implementation that uses a callback via callback-reflection.
 */
class IdValueToValue_Callback implements IdValueToValueInterface {

  /**
   * @var \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface
   */
  private $callback;

  /**
   * Static factory.
   *
   * @param callable $callback
   *   Callback. Signature: ($id, $value) -> $value.
   */
  public static function fromCallable($callback) {
    return new self(
      CallbackUtil::callableGetCallback($callback));
  }

  /**
   * Constructor.
   *
   * @param \Donquixote\CallbackReflection\Callback\CallbackReflectionInterface $callback
   *   Callback reflection object. Signature: ($id, $value) -> $value.
   */
  public function __construct(CallbackReflectionInterface $callback) {
    $this->callback = $callback;
  }

  /**
   * {@inheritdoc}
   */
  public function idValueGetValue($id, $value) {
    try {
      return $this->callback->invokeArgs([$id, $value]);
    }
    catch (\Exception $e) {
      throw new ConfToValueException('Exception in callback.', 0, $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function idPhpGetPhp($id, $php, CfrCodegenHelperInterface $helper) {
    return $this->callback->argsPhpGetPhp(
      [
        var_export($id, TRUE),
        $php,
      ],
      $helper);
  }

}
