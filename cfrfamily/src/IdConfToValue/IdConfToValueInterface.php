<?php

namespace Drupal\cfrfamily\IdConfToValue;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;

/**
 * Object to produce a combined value from a drilldown id + options.
 */
interface IdConfToValueInterface {

  /**
   * Gets a combined value from a drilldown id + options.
   *
   * @param string|null $id
   *   Id extracted from drilldown configuration.
   * @param mixed $conf
   *   Child options extracted from drilldown configuration.
   *
   * @return mixed
   *   Combined value.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   */
  public function idConfGetValue($id, $conf);

  /**
   * Gets PHP code to create the combined value from a drilldown id + options.
   *
   * @param string|int $id
   *   Id extracted from drilldown configuration.
   * @param mixed $conf
   *   Child options extracted from drilldown configuration.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *   Helper for code generation.
   *
   * @return string
   *   PHP expression which, if executed, returns the combined value OR throws
   *   an exception.
   */
  public function idConfGetPhp($id, $conf, CfrCodegenHelperInterface $helper);

}
