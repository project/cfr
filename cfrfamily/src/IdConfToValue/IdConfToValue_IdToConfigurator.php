<?php

namespace Drupal\cfrfamily\IdConfToValue;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface;

/**
 * Implementation using an IdToConfigurator* object.
 */
class IdConfToValue_IdToConfigurator implements IdConfToValueInterface {

  /**
   * @var \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface
   */
  private $idToConfigurator;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface $idToConfigurator
   *   Object to provide a configurator for a given id.
   */
  public function __construct(IdToConfiguratorInterface $idToConfigurator) {
    $this->idToConfigurator = $idToConfigurator;
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetValue($id, $conf) {

    if (NULL === $id) {
      throw new ConfToValueException("Required id is empty.");
    }

    if (NULL === $configurator = $this->idToConfigurator->idGetConfigurator($id)) {
      throw new ConfToValueException("Unknown id '$id'.");
    }

    return $configurator->confGetValue($conf);
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetPhp($id, $conf, CfrCodegenHelperInterface $helper) {

    if (NULL === $id) {
      return $helper->incompatibleConfiguration($id, "Required id missing.");
    }

    if (NULL === $configurator = $this->idToConfigurator->idGetConfigurator($id)) {
      return $helper->incompatibleConfiguration($id, "Unknown id.");
    }

    return $configurator->confGetPhp($conf, $helper);
  }

}
