<?php

namespace Drupal\cfrfamily\IdConfToValue;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrfamily\Configurator\Inlineable\InlineableConfiguratorInterface;
use Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface;

/**
 * Complements a drilldown legend where child drilldowns are expanded.
 *
 * @see \Drupal\cfrfamily\CfrLegend\CfrLegend_InlineExpanded
 */
class IdConfToValue_IdToCfrExpanded implements IdConfToValueInterface {

  /**
   * @var \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface
   */
  private $idToConfigurator;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface $idToConfigurator
   *   Object to provide a configurator for a given id.
   */
  public function __construct(IdToConfiguratorInterface $idToConfigurator) {
    $this->idToConfigurator = $idToConfigurator;
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetValue($id, $conf) {

    if (NULL === $id) {
      throw new ConfToValueException("Required id missing.");
    }

    if (NULL !== $configurator = $this->idToConfigurator->idGetConfigurator($id)) {
      return $configurator->confGetValue($conf);
    }

    $pos = 0;
    while (FALSE !== $pos = strpos($id, '/', $pos + 1)) {
      $k = substr($id, 0, $pos);
      if (NULL === $configurator = $this->idToConfigurator->idGetConfigurator($k)) {
        continue;
      }
      if (!$configurator instanceof InlineableConfiguratorInterface) {
        continue;
      }
      $subId = substr($id, $pos + 1);
      return $configurator->idConfGetValue($subId, $conf);
    }

    throw new ConfToValueException("Unknown id '$id'.");
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetPhp($id, $conf, CfrCodegenHelperInterface $helper) {

    if (NULL === $id) {
      return $helper->incompatibleConfiguration($id, "Required id missing.");
    }

    if (NULL !== $configurator = $this->idToConfigurator->idGetConfigurator($id)) {
      return $configurator->confGetPhp($conf, $helper);
    }

    $pos = 0;
    while (FALSE !== $pos = strpos($id, '/', $pos + 1)) {
      $k = substr($id, 0, $pos);
      if (NULL === $configurator = $this->idToConfigurator->idGetConfigurator($k)) {
        continue;
      }
      if (!$configurator instanceof InlineableConfiguratorInterface) {
        continue;
      }
      $subId = substr($id, $pos + 1);
      // @todo This is not 100% consistent with confGetValue().
      return $configurator->idConfGetPhp($subId, $conf, $helper);
    }

    return $helper->incompatibleConfiguration($id, "Unknown id.");
  }

}
