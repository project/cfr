<?php

namespace Drupal\cfrfamily\IdConfToValue;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Exception\ConfToValueException;
use Drupal\cfrfamily\CfrLegend\CfrLegendInterface;
use Drupal\cfrfamily\CfrLegendItem\ParentLegendItemInterface;

/**
 * Uses drilldown legend + IdConfToValue*, with inline drilldowns expanded.
 */
class IdConfToValue_LegendInlineExpanded implements IdConfToValueInterface {

  /**
   * @var \Drupal\cfrfamily\CfrLegend\CfrLegendInterface
   */
  private $legend;

  /**
   * @var \Drupal\cfrfamily\IdConfToValue\IdConfToValueInterface
   */
  private $idConfToValue;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\CfrLegend\CfrLegendInterface $decoratedLegend
   *   Drilldown legend.
   * @param \Drupal\cfrfamily\IdConfToValue\IdConfToValueInterface $idConfToValue
   *   Object to get a value from drilldown id + options.
   */
  public function __construct(CfrLegendInterface $decoratedLegend, IdConfToValueInterface $idConfToValue) {
    $this->legend = $decoratedLegend;
    $this->idConfToValue = $idConfToValue;
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetValue($id, $conf) {

    if ($this->legend->idIsKnown($id)) {
      return $this->idConfToValue->idConfGetValue($id, $conf);
    }

    $pos = 0;
    while (FALSE !== $pos = strpos($id, '/', $pos + 1)) {
      $k = substr($id, 0, $pos);
      if (!$this->legend->idIsKnown($k)) {
        continue;
      }
      $outerLegendItem = $this->legend->idGetLegendItem($k);
      if (!$outerLegendItem instanceof ParentLegendItemInterface) {
        continue;
      }
      if (NULL === $inlineLegend = $outerLegendItem->getCfrLegend()) {
        continue;
      }
      if (!$inlineLegend instanceof IdConfToValueInterface) {
        continue;
      }
      $subId = substr($id, $pos + 1);
      return $inlineLegend->idConfGetValue($subId, $conf);
    }

    throw new ConfToValueException("Unknown id $id.");
  }

  /**
   * {@inheritdoc}
   */
  public function idConfGetPhp($id, $conf, CfrCodegenHelperInterface $helper) {

    if ($this->legend->idIsKnown($id)) {
      return $this->idConfToValue->idConfGetPhp($id, $conf, $helper);
    }

    $pos = 0;
    while (FALSE !== $pos = strpos($id, '/', $pos + 1)) {
      $k = substr($id, 0, $pos);
      if (!$this->legend->idIsKnown($k)) {
        continue;
      }
      $outerLegendItem = $this->legend->idGetLegendItem($k);
      if (!$outerLegendItem instanceof ParentLegendItemInterface) {
        continue;
      }
      if (NULL === $inlineLegend = $outerLegendItem->getCfrLegend()) {
        continue;
      }
      if (!$inlineLegend instanceof IdConfToValueInterface) {
        continue;
      }
      $subId = substr($id, $pos + 1);
      return $inlineLegend->idConfGetPhp($subId, $conf, $helper);
    }

    return $helper->incompatibleConfiguration($id, "Unknown id.");
  }

}
