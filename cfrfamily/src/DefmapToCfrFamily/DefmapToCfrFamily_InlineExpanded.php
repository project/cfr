<?php

namespace Drupal\cfrfamily\DefmapToCfrFamily;

use Drupal\cfrfamily\CfrFamily\CfrFamily;
use Drupal\cfrfamily\CfrLegend\CfrLegendInterface;
use Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface;

/**
 * Implementation where inline items are included in the parent drilldown.
 */
class DefmapToCfrFamily_InlineExpanded extends DefmapToCfrFamily {

  /**
   * {@inheritdoc}
   */
  protected function create(CfrLegendInterface $legend, IdToConfiguratorInterface $idToConfigurator) {
    return CfrFamily::createExpanded($legend, $idToConfigurator);
  }

}
