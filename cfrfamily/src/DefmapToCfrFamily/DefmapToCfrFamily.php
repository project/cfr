<?php

namespace Drupal\cfrfamily\DefmapToCfrFamily;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\CfrFamily\CfrFamily;
use Drupal\cfrfamily\CfrLegend\CfrLegend_FromDefmap;
use Drupal\cfrfamily\CfrLegend\CfrLegendInterface;
use Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface;
use Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface;
use Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface;
use Drupal\cfrfamily\IdToConfigurator\IdToConfigurator_ViaDefinition;
use Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface;

/**
 * Default implementation.
 */
class DefmapToCfrFamily implements DefmapToCfrFamilyInterface {

  /**
   * @var \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface
   */
  private $definitionToConfigurator;

  /**
   * @var \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface
   */
  private $definitionToLabel;

  /**
   * @var \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface
   */
  private $definitionToGrouplabel;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface $definitionToConfigurator
   *   Object to create a configurator from a plugin definition.
   * @param \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface $definitionToLabel
   *   Object to get a label for a plugin definition.
   * @param \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface $definitionToGrouplabel
   *   Object to get a group label for a plugin definition.
   */
  public function __construct(
    DefinitionToConfiguratorInterface $definitionToConfigurator,
    DefinitionToLabelInterface $definitionToLabel,
    DefinitionToLabelInterface $definitionToGrouplabel
  ) {
    $this->definitionToConfigurator = $definitionToConfigurator;
    $this->definitionToLabel = $definitionToLabel;
    $this->definitionToGrouplabel = $definitionToGrouplabel;
  }

  /**
   * {@inheritdoc}
   */
  public function defmapGetCfrFamily(DefinitionMapInterface $definitionMap, CfrContextInterface $context = NULL) {
    $configuratorMap = new IdToConfigurator_ViaDefinition($definitionMap, $this->definitionToConfigurator, $context);
    $legend = new CfrLegend_FromDefmap($definitionMap, $configuratorMap, $this->definitionToLabel, $this->definitionToGrouplabel);
    return $this->create($legend, $configuratorMap);
  }

  /**
   * Creates the CfrFamily* object.
   *
   * This is a separate method, to allow easy override in a subclass.
   *
   * @param \Drupal\cfrfamily\CfrLegend\CfrLegendInterface $legend
   *   Drilldown legend.
   * @param \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface $idToConfigurator
   *   Object to get a configurator for a given id.
   *
   * @return \Drupal\cfrfamily\CfrFamily\CfrFamilyInterface
   *   The CfrFamily* object.
   */
  protected function create(CfrLegendInterface $legend, IdToConfiguratorInterface $idToConfigurator) {
    return CfrFamily::create($legend, $idToConfigurator);
  }

}
