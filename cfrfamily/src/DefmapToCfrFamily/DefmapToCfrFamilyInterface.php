<?php

namespace Drupal\cfrfamily\DefmapToCfrFamily;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface;

/**
 * Object to get a "CfrFamily" from a definition map.
 */
interface DefmapToCfrFamilyInterface {

  /**
   * Gets a "CfrFamily" from a definition map.
   *
   * @param \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface $definitionMap
   *   Map of plugin definition arrays.
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   Context to constrain the available options.
   *
   * @return \Drupal\cfrfamily\CfrFamily\CfrFamilyInterface
   *   The "CfrFamily" object.
   */
  public function defmapGetCfrFamily(DefinitionMapInterface $definitionMap, CfrContextInterface $context = NULL);

}
