<?php

namespace Drupal\cfrfamily\DefinitionMap;

use Drupal\cfrfamily\DefinitionsById\DefinitionsByIdInterface;
use Drupal\cfrfamily\IdToDefinition\IdToDefinitionInterface;

/**
 * Map of plugin definitions.
 *
 * Inherited methods:
 * - DefinitionsByIdInterface::getDefinitionsById()
 *   Gets the complete array of definitions by id.
 * - IdToDefinitionInterface::idGetDefinition()
 *   Gets a definition for a specific id.
 *
 * Note that an "empty" map with zero definitions is still a valid instance.
 */
interface DefinitionMapInterface extends DefinitionsByIdInterface, IdToDefinitionInterface {

}
