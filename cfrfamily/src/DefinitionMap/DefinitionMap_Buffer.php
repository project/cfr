<?php

namespace Drupal\cfrfamily\DefinitionMap;

use Drupal\cfrfamily\DefinitionsById\DefinitionsByIdInterface;

/**
 * Buffers the plugin definitions for a specific type.
 */
class DefinitionMap_Buffer implements DefinitionMapInterface {

  /**
   * @var array[]|null
   */
  private $definitions;

  /**
   * @var \Drupal\cfrfamily\DefinitionsById\DefinitionsByIdInterface
   */
  private $decorated;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\DefinitionsById\DefinitionsByIdInterface $decorated
   *   Decorated DefinitionsById object.
   */
  public function __construct(DefinitionsByIdInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetDefinition($id) {
    $definitions = $this->getDefinitionsById();
    return isset($definitions[$id])
      ? $definitions[$id]
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionsById() {
    return isset($this->definitions)
      ? $this->definitions
      : $this->definitions = $this->decorated->getDefinitionsById();
  }

}
