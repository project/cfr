<?php

namespace Drupal\cfrfamily\CfrFamilyContainer;

use Donquixote\Containerkit\Container\ContainerBase;

/**
 * Base class for family DI container.
 */
abstract class CfrFamilyContainerBase extends ContainerBase implements CfrFamilyContainerInterface {

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface
   *   Object to get a configurator for a given id.
   *
   * @see $idToConfigurator
   */
  abstract protected function get_idToConfigurator();

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *   Drilldown configurator for this "family" of plugins.
   *
   * @see $configurator
   */
  abstract protected function get_configurator();

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface
   *   Optional drilldown configurator for this "family" of plugins.
   *
   * @see $optionalConfigurator
   */
  abstract protected function get_optionalConfigurator();

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrfamily\CfrLegend\CfrLegendInterface
   *   Drilldown legend for this "family" of plugins.
   *
   * @see $cfrLegend
   */
  abstract protected function get_cfrLegend();

  /**
   * Factory called from __get().
   *
   * @return \Drupal\cfrapi\ConfEmptyness\ConfEmptynessInterface
   *   Object to detect and create 'empty' configuration values.
   *
   * @deprecated
   *   This property + method will be removed in 7.x-3.x.
   *   The entire ConfEmptyness* concept is obsolete.
   *   See https://www.drupal.org/project/cfr/issues/3165150.
   *
   * @see $confEmptyness
   */
  abstract protected function get_confEmptyness();

}
