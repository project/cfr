<?php

namespace Drupal\cfrfamily\CfrFamilyContainer;

/**
 * Contains objects that are specific to a configurator type.
 *
 * @property \Drupal\cfrapi\Legend\LegendInterface $legend
 *   (not implemented)
 * @property \Drupal\cfrapi\Configurator\ConfiguratorInterface $configurator
 *   Drilldown configurator for this "family" of plugins.
 * @property \Drupal\cfrapi\Configurator\Optional\OptionalConfiguratorInterface $optionalConfigurator
 *   Optional drilldown configurator for this "family" of plugins.
 * @property \Drupal\cfrfamily\CfrLegend\CfrLegendInterface $cfrLegend
 *   Drilldown legend for this "family" of plugins.
 * @property \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface $idToConfigurator
 *   Object to get a configurator for a given id.
 * @property \Drupal\cfrapi\ConfEmptyness\ConfEmptynessInterface $confEmptyness
 *   (deprecated) Object to detect and create 'empty' configuration values.
 *
 * @todo $legend is not implemented. Consider to remove.
 */
interface CfrFamilyContainerInterface {

}
