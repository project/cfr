<?php

/** @noinspection PhpDeprecationInspection */

namespace Drupal\cfrfamily\CfrFamilyContainer;

use Drupal\cfrapi\ConfEmptyness\ConfEmptyness_Key;
use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\CfrLegend\CfrLegend_FromDefmap;
use Drupal\cfrfamily\Configurator\Composite\Configurator_CfrLegend;
use Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface;
use Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface;
use Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface;
use Drupal\cfrfamily\IdConfToValue\IdConfToValue_IdToConfigurator;
use Drupal\cfrfamily\IdToConfigurator\IdToConfigurator_ViaDefinition;

/**
 * Implementation using a definition map.
 */
class CfrFamilyContainer_FromDefmap extends CfrFamilyContainerBase {

  /**
   * @var \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface
   */
  private $definitionToLabel;

  /**
   * @var \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface
   */
  private $definitionToGrouplabel;

  /**
   * @var \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface
   */
  private $definitionToConfigurator;

  /**
   * @var \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface
   */
  private $definitionMap;

  /**
   * @var \Drupal\cfrapi\Context\CfrContextInterface|null
   */
  private $context;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface $definitionToConfigurator
   *   Object to get a configurator from a plugin definition.
   * @param \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface $definitionToLabel
   *   Object to get a label from a plugin definition.
   * @param \Drupal\cfrfamily\DefinitionToLabel\DefinitionToLabelInterface $definitionToGrouplabel
   *   Object to get an optgroup label from a plugin definition.
   * @param \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface $definitionMap
   *   Map of plugin definitions.
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *   Context to restrict available options.
   */
  public function __construct(
    DefinitionToConfiguratorInterface $definitionToConfigurator,
    DefinitionToLabelInterface $definitionToLabel,
    DefinitionToLabelInterface $definitionToGrouplabel,
    DefinitionMapInterface $definitionMap,
    CfrContextInterface $context = NULL
  ) {
    $this->definitionMap = $definitionMap;
    $this->context = $context;
    $this->definitionToConfigurator = $definitionToConfigurator;
    $this->definitionToLabel = $definitionToLabel;
    $this->definitionToGrouplabel = $definitionToGrouplabel;
  }

  /**
   * {@inheritdoc}
   */
  protected function get_configurator() {
    $idConfToValue = new IdConfToValue_IdToConfigurator($this->idToConfigurator);
    return new Configurator_CfrLegend(TRUE, $this->cfrLegend, $idConfToValue);
  }

  /**
   * {@inheritdoc}
   */
  protected function get_optionalConfigurator() {
    $idConfToValue = new IdConfToValue_IdToConfigurator($this->idToConfigurator);
    return new Configurator_CfrLegend(FALSE, $this->cfrLegend, $idConfToValue);
  }

  /**
   * {@inheritdoc}
   */
  protected function get_cfrLegend() {
    return new CfrLegend_FromDefmap(
      $this->definitionMap,
      $this->idToConfigurator,
      $this->definitionToLabel,
      $this->definitionToGrouplabel);
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated
   *   This property + method will be removed in 7.x-3.x.
   *   The entire ConfEmptyness* concept is obsolete.
   *   See https://www.drupal.org/project/cfr/issues/3165150.
   */
  protected function get_confEmptyness() {
    return new ConfEmptyness_Key('id');
  }

  /**
   * {@inheritdoc}
   */
  protected function get_idToConfigurator() {
    return new IdToConfigurator_ViaDefinition(
      $this->definitionMap,
      $this->definitionToConfigurator,
      $this->context);
  }

}
