<?php

namespace Drupal\cfrfamily\EnumMap;

use Drupal\cfrapi\EnumMap\EnumMapInterface;
use Drupal\cfrfamily\IdMap\IdMapInterface;
use Drupal\cfrfamily\IdToLabel\IdToLabelInterface;

/**
 * Implementation using a composition of IdMap* + IdToLabel*.
 */
class EnumMap_IdToLabel implements EnumMapInterface {

  /**
   * @var \Drupal\cfrfamily\IdMap\IdMapInterface
   */
  private $idMap;

  /**
   * @var \Drupal\cfrfamily\IdToLabel\IdToLabelInterface
   */
  private $idToLabel;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\IdMap\IdMapInterface $idMap
   *   Object to provide the ids.
   * @param \Drupal\cfrfamily\IdToLabel\IdToLabelInterface $idToLabel
   *   Object to provide a label for a given id.
   */
  public function __construct(IdMapInterface $idMap, IdToLabelInterface $idToLabel) {
    $this->idMap = $idMap;
    $this->idToLabel = $idToLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function idIsKnown($id) {
    return $this->idMap->idIsKnown($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectOptions() {

    $options = [];
    foreach ($this->idMap->getIds() as $id) {
      $options[$id] = $this->idToLabel->idGetLabel($id, $id);
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetLabel($id) {
    return $this->idToLabel->idGetLabel($id);
  }

}
