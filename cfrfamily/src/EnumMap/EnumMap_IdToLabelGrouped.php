<?php

namespace Drupal\cfrfamily\EnumMap;

use Drupal\cfrapi\EnumMap\EnumMapInterface;
use Drupal\cfrfamily\IdMap\IdMapInterface;
use Drupal\cfrfamily\IdToLabel\IdToLabelInterface;

/**
 * Implementation with grouped options.
 */
class EnumMap_IdToLabelGrouped implements EnumMapInterface {

  /**
   * @var \Drupal\cfrfamily\IdMap\IdMapInterface
   */
  private $idMap;

  /**
   * @var \Drupal\cfrfamily\IdToLabel\IdToLabelInterface
   */
  private $idToLabel;

  /**
   * @var \Drupal\cfrfamily\IdToLabel\IdToLabelInterface
   */
  private $idToGroupLabel;

  /**
   * Static factory.
   *
   * @param \Drupal\cfrfamily\IdMap\IdMapInterface $idMap
   *   Object to check and provide known ids.
   * @param \Drupal\cfrfamily\IdToLabel\IdToLabelInterface $idToLabel
   *   Object to get a label for a given id.
   * @param \Drupal\cfrfamily\IdToLabel\IdToLabelInterface|null $idToGroupLabel
   *   (optional) Object to get a group label for a given id.
   *
   * @return \Drupal\cfrapi\EnumMap\EnumMapInterface
   *   Resulting EnumMap object which can provide options for a select element.
   */
  public static function create(IdMapInterface $idMap, IdToLabelInterface $idToLabel, IdToLabelInterface $idToGroupLabel = NULL) {
    return (NULL === $idToGroupLabel)
      ? new EnumMap_IdToLabel($idMap, $idToLabel)
      : new self($idMap, $idToLabel, $idToGroupLabel);
  }

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\IdMap\IdMapInterface $idMap
   *   Object to check and provide known ids.
   * @param \Drupal\cfrfamily\IdToLabel\IdToLabelInterface $idToLabel
   *   Object to get a label for a given id.
   * @param \Drupal\cfrfamily\IdToLabel\IdToLabelInterface $idToGroupLabel
   *   Object to get a group label for a given id.
   */
  public function __construct(IdMapInterface $idMap, IdToLabelInterface $idToLabel, IdToLabelInterface $idToGroupLabel) {
    $this->idMap = $idMap;
    $this->idToLabel = $idToLabel;
    $this->idToGroupLabel = $idToGroupLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function idIsKnown($id) {
    return $this->idMap->idIsKnown($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectOptions() {

    $options = [];
    foreach ($this->idMap->getIds() as $id) {
      if (NULL === $groupLabel = $this->idToGroupLabel->idGetLabel($id)) {
        $options[$id] = $this->idToLabel->idGetLabel($id, $id);
      }
      else {
        $options[$groupLabel][$id] = $this->idToLabel->idGetLabel($id, $id);
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetLabel($id) {
    return $this->idToLabel->idGetLabel($id);
  }

}
