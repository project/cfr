<?php

namespace Drupal\cfrfamily\ArgDefToConfigurator;

use Drupal\cfrapi\Configurator\Unconfigurable\Configurator_FixedValue;
use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrapi\Exception\ConfiguratorFactoryException;

/**
 * Implementation where $definition[$key] is a fixed value.
 */
class ArgDefToConfigurator_FixedValue implements ArgDefToConfiguratorInterface {

  /**
   * {@inheritdoc}
   */
  public function argDefinitionGetConfigurator($fixedValue, array $definition, CfrContextInterface $context = NULL) {
    if (!\is_object($fixedValue)) {
      throw new ConfiguratorFactoryException(
        strtr(
          'Value is expected to be an object, !value found instead.',
          ['!value' => var_export($fixedValue, TRUE)]));
    }
    return new Configurator_FixedValue($fixedValue);
  }

}
