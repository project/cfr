<?php

namespace Drupal\cfrfamily\ArgDefToConfigurator;

use Drupal\cfrapi\Configurator\ConfiguratorInterface;
use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrapi\Exception\ConfiguratorFactoryException;

/**
 * Implementation where $definition[$key] is a configurator object.
 */
class ArgDefToConfigurator_ConfiguratorObject implements ArgDefToConfiguratorInterface {

  /**
   * {@inheritdoc}
   */
  public function argDefinitionGetConfigurator($cfr, array $definition, CfrContextInterface $context = NULL) {
    if (!$cfr instanceof ConfiguratorInterface) {
      throw new ConfiguratorFactoryException(
        strtr(
          'Value is expected to be an instance of ConfiguratorInterface, !value found instead.',
          [
            '!value' => is_object($cfr)
              ? get_class($cfr) . ' object'
              : var_export($cfr, TRUE),
          ]));
    }
    return $cfr;
  }

}
