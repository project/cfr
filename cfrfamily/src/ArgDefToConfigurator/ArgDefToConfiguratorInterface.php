<?php

namespace Drupal\cfrfamily\ArgDefToConfigurator;

use Drupal\cfrapi\Context\CfrContextInterface;

/**
 * Sub-component for DefinitionToConfigurator*.
 *
 * Implementations represent specific ways that a definition can specify a
 * configurator, and are registered for specific keys within the definition.
 *
 * @see \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface
 * @see \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfigurator_Mappers
 * @see \Drupal\cfrplugin\Util\ServiceFactoryUtil::createDeftocfrMappers()
 *
 * @todo Simplify this.
 */
interface ArgDefToConfiguratorInterface {

  /**
   * Attempts to get a configurator as specified by the definition.
   *
   * @param mixed $arg
   *   Value from the plugin definition, for the key this object was registered
   *   for. E.g. the value of $definition['configurator_factory'].
   * @param array $definition
   *   The entire plugin definition, for additional options.
   *   E.g. this could contain $definition['configurator_arguments'].
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   The context object to be passed to configurator factories.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface|null
   *   A configurator, or NULL if not available for the given context or for
   *   current site configuration.
   *
   * @throws \Drupal\cfrapi\Exception\ConfiguratorFactoryException
   *   If the definition is malformed, or if the callback it refers to does not
   *   behave as expected.
   */
  public function argDefinitionGetConfigurator($arg, array $definition, CfrContextInterface $context = NULL);

}
