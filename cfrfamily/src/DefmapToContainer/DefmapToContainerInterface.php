<?php

namespace Drupal\cfrfamily\DefmapToContainer;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface;

/**
 * Object to create a local DI container from a definition map.
 *
 * The DI container can produce all the objects related to the drilldown family.
 */
interface DefmapToContainerInterface {

  /**
   * Gets a container from a definition map.
   *
   * @param \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface $definitionMap
   *   Map with plugin definitions.
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *   Context to constrain the available options.
   *
   * @return \Drupal\cfrfamily\CfrFamilyContainer\CfrFamilyContainerInterface
   *   DI container which can provide objects related to the family.
   */
  public function defmapGetContainer(DefinitionMapInterface $definitionMap, CfrContextInterface $context = NULL);

}
