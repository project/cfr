<?php

namespace Drupal\cfrfamily\CfrLegendProvider;

/**
 * Object that can provide a drilldown legend.
 *
 * In the known implementations this is used as an add-on to other interfaces.
 */
interface CfrLegendProviderInterface {

  /**
   * Gets a drilldown legend.
   *
   * @return \Drupal\cfrfamily\CfrLegend\CfrLegendInterface|null
   *   A drilldown legend, or NULL.
   */
  public function getCfrLegend();

}
