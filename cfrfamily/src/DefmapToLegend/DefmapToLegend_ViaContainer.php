<?php

namespace Drupal\cfrfamily\DefmapToLegend;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface;
use Drupal\cfrfamily\DefmapToContainer\DefmapToContainerInterface;

/**
 * Implementation using a DefmapToContainer* object.
 *
 * For a given definition map, it first creates the family DI container, then
 * it gets the drilldown legend from there.
 */
class DefmapToLegend_ViaContainer implements DefmapToLegendInterface {

  /**
   * @var \Drupal\cfrfamily\DefmapToContainer\DefmapToContainerInterface
   */
  private $defmapToContainer;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\DefmapToContainer\DefmapToContainerInterface $defmapToContainer
   *   Object to get a family DI container from a definition map.
   */
  public function __construct(DefmapToContainerInterface $defmapToContainer) {
    $this->defmapToContainer = $defmapToContainer;
  }

  /**
   * {@inheritdoc}
   */
  public function defmapGetLegend(DefinitionMapInterface $definitionMap, CfrContextInterface $context) {
    return $this->defmapToContainer->defmapGetContainer($definitionMap, $context)->legend;
  }

}
