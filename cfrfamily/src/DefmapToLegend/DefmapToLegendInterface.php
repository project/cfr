<?php

namespace Drupal\cfrfamily\DefmapToLegend;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface;

/**
 * Gets a legend from a definition map.
 *
 * @todo This is not used in a relevant way. Consider to remove, see #3165296.
 */
interface DefmapToLegendInterface {

  /**
   * Gets a legend from a definition map.
   *
   * @param \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface $definitionMap
   *   Map of plugin definitons.
   * @param \Drupal\cfrapi\Context\CfrContextInterface $context
   *   Context to constrain the available options.
   *
   * @return \Drupal\cfrapi\Legend\LegendInterface
   *   Legend for the definition map.
   */
  public function defmapGetLegend(DefinitionMapInterface $definitionMap, CfrContextInterface $context);

}
