<?php

namespace Drupal\cfrfamily\IdToConfigurator;

/**
 * Object to get a configurator for a given id, e.g. for a drilldown.
 */
interface IdToConfiguratorInterface {

  /**
   * Gets a configurator for a given id.
   *
   * @param string|int $id
   *   The id.
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface|null
   *   The configurator for the id, or NULL if it does not exist or cannot be
   *   created.
   */
  public function idGetConfigurator($id);

}
