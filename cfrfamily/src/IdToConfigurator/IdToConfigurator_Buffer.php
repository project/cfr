<?php

namespace Drupal\cfrfamily\IdToConfigurator;

/**
 * Buffer decorator.
 */
class IdToConfigurator_Buffer implements IdToConfiguratorInterface {

  /**
   * @var \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface
   */
  private $decorated;

  /**
   * @var \Drupal\cfrapi\Configurator\ConfiguratorInterface[]|null[]
   */
  private $buffer = [];

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\IdToConfigurator\IdToConfiguratorInterface $decorated
   *   Decorated IdToConfigurator* object.
   */
  public function __construct(IdToConfiguratorInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetConfigurator($id) {
    return array_key_exists($id, $this->buffer)
      ? $this->buffer[$id]
      : $this->buffer[$id] = $this->decorated->idGetConfigurator($id);
  }

}
