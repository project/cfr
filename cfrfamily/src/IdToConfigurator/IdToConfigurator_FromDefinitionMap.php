<?php

namespace Drupal\cfrfamily\IdToConfigurator;

use Drupal\cfrapi\Context\CfrContextInterface;
use Drupal\cfrapi\Exception\ConfiguratorFactoryException;
use Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface;
use Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface;

/**
 * Obsolete implementation.
 *
 * @deprecated
 *   Use IdToConfigurator_ViaDefinition instead.
 *
 * @see \Drupal\cfrfamily\IdToConfigurator\IdToConfigurator_ViaDefinition
 */
class IdToConfigurator_FromDefinitionMap implements IdToConfiguratorInterface {

  /**
   * @var \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface
   */
  private $definitionMap;

  /**
   * @var \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface
   */
  private $definitionToConfigurator;

  /**
   * @var \Drupal\cfrapi\Context\CfrContextInterface|null
   */
  private $context;

  /**
   * Constructor.
   *
   * @param \Drupal\cfrfamily\DefinitionMap\DefinitionMapInterface $definitionMap
   *   Map of plugin definitions.
   * @param \Drupal\cfrfamily\DefinitionToConfigurator\DefinitionToConfiguratorInterface $definitionToConfigurator
   *   Object to get a configurator for a plugin definition.
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *   Context to constrain available options.
   */
  public function __construct(
    DefinitionMapInterface $definitionMap,
    DefinitionToConfiguratorInterface $definitionToConfigurator,
    CfrContextInterface $context = NULL
  ) {
    $this->definitionMap = $definitionMap;
    $this->definitionToConfigurator = $definitionToConfigurator;
    $this->context = $context;
  }

  /**
   * {@inheritdoc}
   */
  public function idGetConfigurator($id) {

    $definition = $this->definitionMap->idGetDefinition($id);

    if (NULL === $definition) {
      return NULL;
    }

    try {
      return $this->definitionToConfigurator->definitionGetConfigurator($definition, $this->context);
    }
    catch (ConfiguratorFactoryException $e) {
      // Something is wrong with this plugin.
      // Report the problem.
      $this->reportFailingDefinition($id, $definition, $e, $this->context);

      // Return NULL, as if this plugin did not exist.
      return NULL;
    }
  }

  /**
   * Reports when ->definitionGetConfigurator() has failed.
   *
   * This could become protected in the future, so that it can be overridden.
   *
   * @param string|int $id
   *   Plugin id.
   * @param array $definition
   *   Plugin definition.
   * @param \Exception $e
   *   Exception from ->definitionGetConfigurator().
   * @param \Drupal\cfrapi\Context\CfrContextInterface|null $context
   *   Context that was sent to ->definitionGetConfigurator().
   */
  private function reportFailingDefinition($id, array $definition, \Exception $e, CfrContextInterface $context = NULL) {
    watchdog(
      'cfrplugin',
      'Broken plugin at @id: !message',
      [
        '@id' => var_export($id, TRUE),
        '!message' => '<div>' . $e->getMessage() . '</div>',
      ]);
  }

}
